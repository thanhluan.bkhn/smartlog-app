# smartlog-app

// create app
# appcenter apps create -d <appDisplayName> -o <operatingSystem>  -p <platform>
appcenter apps create -d sabeco-android -o Android -p React-Native
appcenter apps create -d sabeco-ios -o iOS -p React-Native

// code push add app
# appcenter codepush deployment add -a <ownerName>/<appName> <deploymentName>
appcenter codepush deployment add -a thanhluan.bkhn-gmail.com/sabeco-android dev
appcenter codepush deployment add -a thanhluan.bkhn-gmail.com/sabeco-android qa
appcenter codepush deployment add -a thanhluan.bkhn-gmail.com/sabeco-android prod

// deploy
# appcenter codepush deployment add -a <ownerName>/<appName> <deploymentName>
appcenter codepush release-react -a thanhluan.bkhn-gmail.com/sabeco-android -d dev
appcenter codepush release-react -a thanhluan.bkhn-gmail.com/sabeco-android -d qa
appcenter codepush release-react -a thanhluan.bkhn-gmail.com/sabeco-android -d prod

// show list
appcenter apps list
// delete app and update
# appcenter apps delete -a <ownerName>/<appName>
# appcenter apps update -n <newName> -a <ownerName>/<appName>
# appcenter codepush deployment remove -a <ownerName>/<appName> <deploymentName>
# appcenter codepush deployment rename -a <ownerName>/<appName> <deploymentName> <newDeploymentName>

appcenter apps delete -a thanhluan.bkhn-gmail.com/sabeco-android



