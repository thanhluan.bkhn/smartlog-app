import React, { Component, LegacyRef } from 'react';
import { View, Text, Image, KeyboardAvoidingView, Platform, TextInputSelectionChangeEventData, Keyboard } from 'react-native';
import { Input, Button } from 'react-native-elements';
import { STYLES, IMAGES } from '~/common/constants';
import { EColor, ESize } from '~/common/enums';
import AppLayout from '~/layouts/AppLayout';
import { AuthStore } from '~/stores/auth/auth.store';
import { BindStore } from '~/stores/base/decorator';
import { LanguageStore } from '~/stores/language/language.store';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { ScrollView } from 'react-native-gesture-handler';
import BuildConfig from '~/common/config/BuildConfig';


interface IProps {
    authStore?: AuthStore,
    languageStore?: LanguageStore
}
@BindStore('authStore', 'languageStore')
export default class SignInScreen extends Component<IProps> {

    state = {
        username: '',
        password: '',
        width: ESize.WIDTH_WINDOW,
        height: ESize.HEIGHT_WINDOW,
    }


    onLayout = ({ nativeEvent: { layout } }) => {
        const { width, height } = layout;
        this.setState({
            width, height
        });
    };
    private refInputPassword;
    private refBtn;
    login = async () => {
        const { username, password } = this.state
        const { signIn } = this.props.authStore
        await signIn(username, password)
    }

    render() {
        const { username, password, height, width } = this.state
        const { languageStore, authStore } = this.props
        const { isLoading } = authStore
        const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : - ESize.HEIGHT_SCREEN / 7;
        return (

            <AppLayout isLoading={isLoading}>

                <KeyboardAwareScrollView
                    style={{ flex: 1 }}
                    enableOnAndroid
                >
                    <ScrollView onLayout={this.onLayout}>
                        <View style={{
                            flex: 1, backgroundColor: EColor.primary,
                            flexDirection: 'column', height: (height > width) ? height : width
                        }}>
                            <View style={{ flex: 6 }}>
                                <View style={{ ...STYLES.center, flex: 7 }}>
                                    <Image
                                        resizeMode='contain'
                                        source={IMAGES.sabeco_logo}
                                    >
                                    </Image>
                                    <Text style={{ color: EColor.white, textAlign: 'center' }}>{BuildConfig.version}</Text>
                                </View>
                                <View style={{ flex: 1, marginLeft: 20, marginRight: 20 }}>
                                    <Input
                                        inputContainerStyle={{
                                            borderWidth: 1,
                                            borderRadius: 50,
                                        }}
                                        placeholderTextColor={EColor.disable}
                                        placeholder={languageStore.translate('Username')}
                                        inputStyle={{
                                            marginLeft: 10,
                                            color: EColor.white
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({
                                                username: text
                                            })
                                        }}
                                        returnKeyType={"next"}
                                        onSubmitEditing={() => this.refInputPassword.focus()}
                                        value={username}
                                        leftIcon={{ type: 'font-awesome', name: 'user', color: EColor.white }}
                                    />
                                </View>
                                <View style={{ height: 20 }}>

                                </View>
                                <View style={{ flex: 1, marginLeft: 20, marginRight: 20 }}>
                                    <Input
                                        inputContainerStyle={{
                                            borderWidth: 1,
                                            borderRadius: 50,
                                        }}
                                        blurOnSubmit={false}
                                        ref={(ip) => this.refInputPassword = ip}
                                        placeholderTextColor={EColor.disable}
                                        placeholder={languageStore.translate('Password')}
                                        inputStyle={{
                                            marginLeft: 10,
                                            color: EColor.white
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({
                                                password: text
                                            })
                                        }}
                                        onSubmitEditing={() => Keyboard.dismiss()}
                                        secureTextEntry={true}
                                        value={password}
                                        leftIcon={{ type: 'font-awesome', name: 'lock', color: EColor.white }}
                                    />
                                </View>
                            </View>
                            <View style={{ ...STYLES.center, flex: 1 }}>
                                <Button
                                    ref={(ref) => this.refBtn = ref}
                                    containerStyle={{
                                        width: '85%',
                                    }}
                                    titleStyle={{
                                        color: EColor.white
                                    }}
                                    buttonStyle={{
                                        borderColor: EColor.white,
                                        borderWidth: 1,
                                        borderRadius: 50,

                                    }}
                                    type='outline'
                                    onPress={this.login}
                                    title={languageStore.translate('Login')}
                                >

                                </Button>
                            </View>
                            <View style={{ flex: 2 }}>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAwareScrollView>
            </AppLayout>
        )
    }
}
