import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { STYLES } from '~/common/constants'
import FDateTimePicker from '~/components/inputs/FDateTimePicker'
import { BindStore } from '~/stores/base/decorator'
import { ReceiptListStore } from '~/stores/inbound/receipt-list.store'
import { ReceiptDetailStore } from '~/stores/inbound/receipt-detail.store'

interface IProps  {
    receiptDetailStore?: ReceiptDetailStore
}

@BindStore('receiptDetailStore')
export default class Example extends Component<IProps> {
    async componentDidMount(){
        const {load} = this.props.receiptDetailStore
        await load();
    }
    render() {
        const {data} = this.props.receiptDetailStore
        alert(JSON.stringify(data[0]))
        return (
            <View style = {{flex: 1, flexDirection: 'column'}}>
                <FDateTimePicker value = {new Date()} />
                <FDateTimePicker value = {undefined} />
            </View>
        )
    }
}
