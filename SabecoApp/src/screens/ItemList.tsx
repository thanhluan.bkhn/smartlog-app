import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { observer } from 'mobx-react'
import { BindStore } from '~/stores/base/decorator'
import { IBaseScreenProps } from './BaseScreen'
import { ReceiptListStore } from '~/stores/inbound/receipt-list.store'
interface IProps {
    receiptListStore?: ReceiptListStore,
    item: any
}
@BindStore('receiptListStore')
export  class ItemList extends Component<IProps> {
    render() {
        const {item} = this.props

        return (
            <View>
                <Text> {JSON.stringify(item)} </Text>
            </View>
        )
    }
}
