import React, { Component } from 'react'
import { Text, View, Image, Platform } from 'react-native'
import { STYLES, IMAGES } from '~/common/constants'
import { EColor } from '~/common/enums'
import { BindStore } from '~/stores/base/decorator'
import { LanguageStore } from '~/stores/language/language.store'
import { AuthStore } from '~/stores/auth/auth.store'
import { IScreenProps } from '~/stores/base/interfaces'
import { apiMediaService, apiAuthService } from '~/services/network/api.service'
import navigationService from '~/services/navigation/navigation.service'
import firebase from 'react-native-firebase'

interface IProps extends IScreenProps {
  authStore?: AuthStore,
  languageStore?: LanguageStore,
}

@BindStore('authStore', 'languageStore')
export default class SplashScreen extends Component<IProps> {
  private removeNotificationListener;
   setupFirebase =async () => {
    try {
      const enabled = await firebase.messaging().hasPermission();
      if (enabled) {

      }
      await firebase.messaging().requestPermission();
      const token = await firebase.messaging().getToken();
      
      const channel = new firebase.notifications.Android.Channel(
        'channelId',
        'Channel Name',
        firebase.notifications.Android.Importance.Max
      ).setDescription('A natural description of the channel');
      firebase.notifications().android.createChannel(channel);

      firebase.notifications().onNotificationOpened(notificationOpen => {
        console.log(notificationOpen);
        const { data } = notificationOpen.notification;
        if (data.screen) {
          navigationService.navigate(data.screen)
        }
      })
      this.removeNotificationListener = firebase.notifications().onNotification(async (notification) => {
        if (Platform.OS === 'android') {
          const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setBody(notification.body)
            .setData(notification.data)
            .setSound('default')
            .android.setCategory('msg')
            .android.setChannelId('channelId')
            .android.setBigPicture('https://salt.tikicdn.com/cache/w1200/ts/product/41/99/74/08cdc81701dae267df131f43ef0ae1d4.jpg')
            .android.setBigPicture('https://www.google.com/imgres?imgurl=https%3A%2F%2Fdbservices.com%2Fwp-content%2Fuploads%2F2016%2F08%2FFileMaker-Cross-Platform-Notifications-1024x1024.png&imgrefurl=https%3A%2F%2Fdbservices.com%2Farticles%2Ffilemaker-cross-platform-notifications%2F&docid=A58aXQnEG0YKQM&tbnid=I6UzamkYxKi8fM%3A&vet=10ahUKEwj-2KmDxcHiAhXZxYsBHdN-AW8QMwhXKAwwDA..i&w=1024&h=1024&bih=852&biw=1745&q=notification&ved=0ahUKEwj-2KmDxcHiAhXZxYsBHdN-AW8QMwhXKAwwDA&iact=mrc&uact=8')
            .android.setPriority(firebase.notifications.Android.Priority.Max)
            .android.setSmallIcon('ic_launcher'); // name icon path @drawable

          // const action = new firebase.notifications.Android.Action('test_action', 'ic_notifications', 'My Test Action');
          // Add the action to the notification
          // localNotification.android.addAction(action);

          firebase.notifications()
            .displayNotification(localNotification).then(_ => {
              console.log(_);
            })
            .catch(err => {
              console.log(err)
            });

        } else if (Platform.OS === 'ios') {
          const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setBody(notification.body)
            .setData(notification.data)
            .ios.setBadge(notification.ios.badge!);
          firebase.notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));
        }
      }
      );
    } catch (error) {
      console.log(error);

      // alert(JSON.stringify(error))
    }
  }
  componentWillUnmount() {
   
  }

  async componentDidMount() {
    const { navigation } = this.props
    try {
      const { navigation, languageStore, authStore } = this.props
      apiMediaService.setNavigation(navigation);
      apiAuthService.setNavigation(navigation);
      await languageStore.loadResource();
      await authStore.authenticate(navigation)
      await this.setupFirebase();
    } catch (error) {
      navigation.navigate('AuthNavigation');
    }
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: EColor.primary, flexDirection: 'column' }}>
        <View style={{ flex: 6 }}>
          <View style={{ ...STYLES.center, flex: 7 }}>
            <Image
              resizeMode='contain'
              source={IMAGES.sabeco_logo}
            >

            </Image>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              style={{ color: 'white', textAlign: 'center' }}
            >Powered by Smartlog 4.0</Text>
          </View>
        </View>
        <View style={{ flex: 3 }}>

        </View>
      </View>
    )
  }
}

