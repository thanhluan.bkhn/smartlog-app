import React, { Component, Fragment, PureComponent } from 'react';
import { BackHandler, StyleSheet, View, ActivityIndicator } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { Avatar, Button, Icon } from 'react-native-elements';
import uuid from 'uuid';
import { EColor } from '~/common/enums';
import { injectCommonStore } from '~/common/hoc';
import FHeader from '~/components/headers/FHeader';
import FCamera from '~/components/views/FCamera';
import AppLayout from '~/layouts/AppLayout';
import { IBaseScreenProps } from '~/screens/BaseScreen';
import navigationService from '~/services/navigation/navigation.service';
import { BindStore } from '~/stores/base/decorator';
import { CaptureReceiptStore } from '~/stores/inbound/capture-receipt.store';

interface IProps extends IBaseScreenProps {
    captureReceiptStore?: CaptureReceiptStore
}
@BindStore('captureReceiptStore')
class CaptureScreen extends Component<IProps> {

    private camera: RNCamera;
    private backHandler;

    takePicture = async () => {

        if (this.camera) {
            const { images } = this.props.captureReceiptStore
            const data = await this.camera.takePictureAsync({
                base64: true, fixOrientation: false,
                quality: 0.5,
                skipProcessing: true
            });
            images.push( {
                uri: data.uri,
                id: ''
            })
        }
    };


    saveImage = async () => {
        const { saveImages } = this.props.captureReceiptStore;
        await saveImages();
    }
    async  componentDidMount() {
        const { load } = this.props.captureReceiptStore
        await load();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            navigationService.goBack();
            return true;
        });

    }
    removeImage = (index) => {
        const { removeImage } = this.props.captureReceiptStore
        removeImage(index)
    }
    

    renderListImage = () => {
        const { isLoading, images } = this.props.captureReceiptStore
        if (isLoading) {
            return (
                <View style = {{flex:1,justifyContent:'center'}}>
                    <ActivityIndicator animating={true} size={'small'} color={EColor.primary} />
                </View>
            )
        }
        return (
            images.map((item, index) => {
                return (
                    <View key={`${index}`} style={{ margin: 5 }} >
                        <Avatar
                            key={uuid.v4()}
                            source={{
                                uri: item.uri,
                            }}
                            size='large'
                        />
                        <Icon
                            key={uuid.v4()}
                            onPress={this.removeImage.bind(this, index)}
                            type='font-awesome'
                            name='remove'
                            color={EColor.primary}
                            containerStyle={{ position: 'absolute', top: -6, right: -6 }}
                        />
                    </View>
                )
            })

        )
    }

    componentWillUnmount() {
        if (this.backHandler) {
            this.backHandler.remove();
        }
        // this.camera = null;
    }
    render() {
        const { translate, captureReceiptStore } = this.props;

        const { isLoading, images, removeImage } = captureReceiptStore

        return (
            <AppLayout >
                <FHeader title={translate('Camera')} navigation={this.props.navigation} typeLeftComponent='back' />
                <FCamera
                    refCamera={ref => {
                        this.camera = ref;
                    }}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                />
                <Icon onPress={this.takePicture} type='font-awesome' name='camera' color='white' size={50}></Icon>
                <View style={{ flex: 1, flexDirection: 'row', margin: 10 }}>
                    {this.renderListImage()}
                </View>
                <View style={{ flex: 1, marginLeft: 20, marginRight: 20 }}>
                    <Button
                        onPress={this.saveImage}
                        buttonStyle={{ backgroundColor: EColor.primary }}
                        title={translate('Save')}
                    ></Button>
                </View>
            </AppLayout>

        )
    }
}

class MyWrapComponent extends Component<IBaseScreenProps> {
    state = {
        isLoading: true
    }


    render() {
        if (!this.props.isFocused) {
            return (
                <View style={styles.containerStyle} />
            )
        }
        return (
            < CaptureScreen {...this.props} />
        )
    }
}
const styles = StyleSheet.create({
    containerStyle: { flex: 1, overflow: 'hidden', position: 'relative' },
});
export default injectCommonStore(MyWrapComponent)
