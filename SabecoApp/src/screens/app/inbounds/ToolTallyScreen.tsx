import React, { Component } from 'react';
import AppLayout from '~/layouts/AppLayout';
import FHeader from '~/components/headers/FHeader';
import { ICommonStore, injectCommonStore } from '~/common/hoc';
import FViewMatrix from '~/components/views/FViewMatrix';
import uuid from 'uuid';
import { BindStore } from '~/stores/base/decorator';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { ReceiptDetailStore } from '~/stores/inbound/receipt-detail.store';


interface IProps extends IBaseScreenProps {
    receiptDetailStore?: ReceiptDetailStore
}
@BindStore('receiptDetailStore')
class ToolTallyScreen extends BaseScreen<IProps> {
    async reload(){
        
    }

    render() {
        const { translate ,receiptDetailStore,navigation} = this.props
        const {itemSelected } = receiptDetailStore
        return (
            
                <AppLayout>
                    <FHeader title={translate('Tool tally')} navigation = {navigation} typeLeftComponent='back' />
                    <FViewMatrix
                        translate = {translate}
                        key={uuid.v4()}
                        divisionNumber={8}
                        numberOfCheck = {0}
                        total={itemSelected.openqtypallet}
                    />
                </AppLayout>
        )
    }
}
export default injectCommonStore(ToolTallyScreen)