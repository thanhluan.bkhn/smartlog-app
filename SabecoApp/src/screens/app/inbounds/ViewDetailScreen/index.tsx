import React, { Component } from 'react'
import { View, Text, FlatList, RefreshControl, Keyboard } from 'react-native'
import FHeader from '~/components/headers/FHeader'
import AppLayout from '~/layouts/AppLayout'
import { BindStore } from '~/stores/base/decorator';
import { STYLES, RECEIPT_STATUS } from '~/common/constants'
import { IScreenProps } from '~/stores/base/interfaces'
import { toJS } from 'mobx';
import { ICommonStore, injectCommonStore } from '~/common/hoc';
import FText from '~/components/texts/FText';
import { EColor } from '~/common/enums';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FButtonMatrix from '~/components/buttons/FButtonMatrix';
import navigationService from '~/services/navigation/navigation.service';
import uuid from 'uuid';
import Modal from '~/components/modals/Modal';
import { Input } from 'react-native-elements';
import { NavigationEventSubscription } from 'react-navigation';
import FInputDate from '~/components/inputs/FInputDate';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { ReceiptDetailStore } from '~/stores/inbound/receipt-detail.store';
import FTextIpunt from '~/components/texts/FTextInput';
import FDateTimePicker from '~/components/inputs/FDateTimePicker';
import Moment from 'moment-timezone';
interface IProps extends IBaseScreenProps {
    receiptDetailStore?: ReceiptDetailStore
}
@BindStore('receiptDetailStore')
class ViewDetailScreen extends BaseScreen<IProps> {
    async reload() {
        const { receiptDetailStore } = this.props

        receiptDetailStore.data = [];
        await receiptDetailStore.load();
    }
    private willFocusSubscription: NavigationEventSubscription;
    async  componentDidMount() {

        const { receiptDetailStore } = this.props

        receiptDetailStore.data = [];
        await receiptDetailStore.load();
    }
    onRefresh = async () => {
        const { receiptDetailStore } = this.props
        receiptDetailStore.data = [];
        await receiptDetailStore.onRefresh();
    }


   
    pipeValue = (valueDate) => {
        const  format = 'DD-MM-YYYY';
      
        if (!valueDate) {
            return ''
        }
        try {
            const output = Moment(valueDate).add(7,'hours').format(format);
            if (!output) {
                return ''
            }
            if (output == 'Invalid date') {
                return ''
            }

            return output;
        } catch (error) {
            alert(JSON.stringify(error))
            return '';
        }

    }
    
    renderItem = ({ item, index }) => {
        const {trailernumber,externalreceiptkey2}= this.props.receiptDetailStore.receiptSelected
        const { receiptkey, qtyexpected, qtyreceived, pallet, qtyreceivedcs,
            descr, sku, lottable01, lottable04, lottable05,toloc,
            qtyexpectedcs, unconfirm
        } = item;

        const checkConfirmLocation = Number(unconfirm) == 0;
        const { translate } = this.props

        let newPallet = 0;
        let newQty = 0;

        if (!isNaN(pallet)) {
            newPallet = Number(pallet);
        }
        if (!isNaN(pallet)) {
            newQty = Number(qtyexpected);
        }

        const oddPallet = pallet > 0 && newQty % pallet == 0 ? '' : newQty % pallet + '';
        const evenPallet = newPallet > 0 ? Math.floor(newQty / newPallet) + '' : qtyexpected + '';
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: 'column', margin: 15, padding: 2, borderColor: EColor.blueInfo, borderWidth: 1 }}>
                    <View>
                        <FText
                            label={translate('Car number')}
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5,

                            }}

                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={trailernumber} ></FText>
                        <FText
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5
                            }}
                            label={translate('Trip number')}
                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={externalreceiptkey2} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            fontSize={15}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            label={translate('Product')}
                            title={descr} ></FText>
                        <FText
                            fontSize={15}
                            label={translate('Sku')}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            title={sku} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Total CS')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${qtyexpectedcs}`} ></FText>
                        <FText
                            label={translate('Total Receive')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${qtyreceivedcs}`} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Pallet even')}
                            fontSize={14}

                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${evenPallet}`} ></FText>
                        <FText
                            label={translate('Pallet odd')}
                            fontSize={14}

                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${oddPallet}`} ></FText>


                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Location')}
                            fontSize={14}

                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${toloc}`} ></FText>
                        </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FTextIpunt
                            disable={true}
                            label={translate('Batch/lot')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                height: 80,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            returnKeyLabel={translate('Update')}
                            value={lottable01 || ''}
                        ></FTextIpunt>
                        <FDateTimePicker
                            disable = {true}
                            containerStyle={{
                                height: 80,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            label = {translate('NSX')}
                            value={lottable04 ? Moment(lottable04).add(7,'hours').toDate() : undefined}
                        />
                    </View>
                </View>
            </View>
        )
    }

    renderKey = (item, index) => {
        return `${item.receiptkey.toString()}${index}`;
    }


    renderEmpty = () => (
        <View style={{ ...STYLES.center }}>
            <Text style={{ textAlign: 'center', fontSize: 24 }}>{this.props.translate('Empty data')}</Text>
        </View>
    )

    
    
    render() {
        const { receiptDetailStore, translate, navigation } = this.props
        const { isLoading, isRefreshing ,isBack} = receiptDetailStore
       
        return (
            <AppLayout>
                <FHeader 
                title={translate('REVIEW RECEIPTS DETAIL')} 
                navigation={navigation} 
                typeLeftComponent='back' />
                <AppLayout isLoading={isLoading}>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={toJS(receiptDetailStore.data)}
                            renderItem={this.renderItem}
                            refreshControl={
                                <RefreshControl
                                    colors={['blue']}
                                    refreshing={isRefreshing}
                                    onRefresh={this.onRefresh} />
                            }
                            initialNumToRender={16}
                            keyExtractor={this.renderKey}
                        >
                        </FlatList>
                    </View>
                   
                </AppLayout>
               
            </AppLayout>
        )
    }
}
export default injectCommonStore(ViewDetailScreen)


