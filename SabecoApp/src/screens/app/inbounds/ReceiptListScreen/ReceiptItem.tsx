import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';
import { RECEIPT_STATUS } from '~/common/constants';
import { EColor } from '~/common/enums';
import { ICommonStore } from '~/common/hoc';
import { bindCommon, BinPropertyStore } from '~/stores/base/decorator';
import { CaptureReceiptStore } from '~/stores/inbound/capture-receipt.store';
import { ReceiptListStore } from '~/stores/inbound/receipt-list.store';
import { TouchableOpacity } from 'react-native-gesture-handler';

interface IProps extends ICommonStore {
    item: {
        [key: string]: any
    },
    sizeItem: number,
    fontSize: number,
    navigation: NavigationScreenProp<any>,
    receiptListStore?: ReceiptListStore,
    captureReceiptStore?: CaptureReceiptStore
}
@BinPropertyStore(store => {
    const common = bindCommon(store);
    return {
        ...common,
        receiptListStore: store.receiptListStore,
        captureReceiptStore: store.captureReceiptStore
    }
})
export default class ReceiptItem extends Component<IProps> {

    openCamera = () => {
        const { id } = this.props.item
        const { navigation, captureReceiptStore } = this.props;
        captureReceiptStore.idSelected = id;
        navigation.navigate('CaptureScreen')
    }

    pipeStatus = () => {
        const { status } = this.props.item
        const defaultValue = {
            title: '',
            color: EColor.black
        }
        const values = Object.values(RECEIPT_STATUS);
        const indexFind = values.findIndex(item => {
            return item.value == status
        });
        if (indexFind == -1) {
            return defaultValue;
        }
        const { translate } = this.props;
        return {
            title: translate(values[indexFind].text),
            color: values[indexFind].color
        }

    }
    gotoViewDetail = () => {
        const { receiptListStore, userInfo, item } = this.props
        const { gotoViewDetail } = receiptListStore
        const { receiptkey, trailernumber = '', id, externalreceiptkey2 } = item
        const body = {
            keeper: userInfo.username,
            id: id,
            externalreceiptkey2: externalreceiptkey2
        }
        gotoViewDetail(body, receiptkey, trailernumber)
    }
    gotoDetail = async () => {

        const { receiptListStore, userInfo, item } = this.props
        const { updateUserAction } = receiptListStore
        const { receiptkey, trailernumber = '', id, externalreceiptkey2 } = item
        const body = {
            keeper: userInfo.username,
            id: id,
            externalreceiptkey2: externalreceiptkey2
        }
        await updateUserAction(body, receiptkey, trailernumber);
    }
    renderKeeper = () => {
        const { translate, userInfo, fontSize, item } = this.props;
        const { status, keeper } = item;
        if (status == RECEIPT_STATUS.Received.value) {
            return (
                <Button
                    disabled={true}

                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.info
                    }} title={keeper} />
            )
        }
        if (keeper) {
            if (keeper != userInfo.username) {
                return (
                    <Button
                        disabled={true}

                        iconRight={true}
                        icon={() => {
                            return (
                                <Icon
                                    iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                    size={fontSize}
                                    color={EColor.white}
                                    name={'ios-arrow-forward'}
                                    type={'ionicon'}
                                />
                            )
                        }}
                        titleStyle={{
                            fontSize: fontSize
                        }}
                        buttonStyle={{
                            height: '80%',
                            backgroundColor: EColor.info
                        }} title={keeper} />
                )
            }
            return (
                <Button
                    onPress={this.gotoDetail.bind(this)}
                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.primary
                    }} title={keeper} />
            )
        }

        return (
            <Button

                onPress={this.gotoDetail.bind(this)}
                iconRight={true}
                icon={() => {
                    return (
                        <Icon
                            iconStyle={{ marginLeft: 8, marginTop: 4 }}
                            size={fontSize}
                            color={EColor.white}
                            name={'ios-arrow-forward'}
                            type={'ionicon'}
                        />
                    )
                }}
                titleStyle={{
                    fontSize: fontSize
                }}
                buttonStyle={{
                    height: '80%',
                    backgroundColor: '#00F2FE'
                }} title={translate('Start')} />
        )
    }

    render() {

        const { translate, sizeItem, fontSize, item } = this.props

        const { transaction_type, externreceiptkey, externalreceiptkey2, trailernumber, total_qtyexpected, status, sku } = item
        const valuePipeStatus = this.pipeStatus();
        return (
            <View style={{
                height: sizeItem * 5 + 25,
                flexDirection: 'column', margin: 5, flex: 1, borderRadius: 6, shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,

                elevation: 1
            }}>
                <View style={{

                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Command')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize, color: EColor.textColorTable }}>{externreceiptkey}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Status')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize, textAlign: 'center', color: valuePipeStatus.color }}>{valuePipeStatus.title}</Text>
                        </View>
                        <View style={{ width: 50, justifyContent: 'center' }}>
                            <TouchableOpacity 
                                onPress = {this.gotoViewDetail}
                            style={{ width: 40, height: 40, justifyContent: 'center', borderColor: EColor.blueInfo, backgroundColor: EColor.brandInfo, borderWidth: 1, borderRadius: 50 }}>
                                <Icon name='eye' type='entypo' color={EColor.brandWarning} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Car number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.4, color: '#58D2F3' }}>{trailernumber}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Quantity')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5, color: EColor.textColorTable, fontWeight: 'bold' }}>{total_qtyexpected}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Trip number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.4, color: '#58D2F3' }}>{externalreceiptkey2}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Sku')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5, fontWeight: 'bold', color: EColor.brandPrimary }}>{sku}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Transaction type')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize, color: EColor.textColorTable }}>{transaction_type}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Capture')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Icon onPress={this.openCamera} type='material-community' name='camera' size={40} color={EColor.purple}></Icon>
                        </View>
                    </View>
                </View>

                <View style={{
                    height: sizeItem,
                    margin: 16,
                    justifyContent: 'center'
                }}>
                    {this.renderKeeper()}

                </View>
            </View>
        )
    }
}
