import React, { Fragment, PureComponent } from 'react'
import { View, Text, FlatList, ActivityIndicator, RefreshControl, TouchableOpacity, LayoutChangeEvent } from 'react-native'
import FHeader from '~/components/headers/FHeader'
import AppLayout from '~/layouts/AppLayout'
import { Input, Icon, Button, ButtonGroup } from 'react-native-elements'
import { EColor, ESize } from '~/common/enums'
import { BindStore } from '~/stores/base/decorator'
import { RECEIPT_STATUS, STYLES, TYPE_HEADER } from '~/common/constants'
import { toJS } from 'mobx';
import { injectCommonStore } from '~/common/hoc';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen'
import { ReceiptListStore } from '~/stores/inbound/receipt-list.store'
import ReceiptItem from './ReceiptItem'

interface IProps extends IBaseScreenProps {
    receiptListStore?: ReceiptListStore;
}
const ITEM_SIZE = 60;
const scale = 8;
@BindStore('receiptListStore')
class ReceiptListScreen extends BaseScreen<IProps> {
    async reload() {
        // await this.onRefresh();
    }
    state = {
        sizeItem: ESize.WIDTH_WINDOW / scale,
        fontSize: ESize.WIDTH_WINDOW / (scale * 5)
    }

    async componentDidMount() {

        const { receiptListStore } = this.props;
        receiptListStore.data = [];
        receiptListStore.keySearch = '';
        receiptListStore.load(1);
    }

    onRefresh = async () => {
        const { receiptListStore } = this.props;
        await receiptListStore.onRefresh();
    }
    loadMore = async () => {

        const { receiptListStore } = this.props

        const { isLoading, data, total } = receiptListStore
        if (isLoading || data.length === total) {
            return
        }
        await receiptListStore.loadMore();
    }

    search = async () => {
        const { receiptListStore } = this.props

        await receiptListStore.search();
    }



    renderItem = ({ item, index }) => {
        const { sizeItem, fontSize } = this.state;
        const { navigation } = this.props
        return (
            <ReceiptItem item={item} sizeItem={sizeItem} fontSize={fontSize} navigation={navigation} />
        )
    }
    onLayout = (event: LayoutChangeEvent) => {
        const { height, width } = event.nativeEvent.layout
        const sizeItem = width / scale;
        const fontSize = width / (scale * 5);
        this.setState({
            sizeItem, fontSize
        })
    }
    getItemLayout = (data, index) => {
        const { sizeItem, fontSize } = this.state;
        return {
            length: sizeItem * 4 + 15,
            offset: (sizeItem * 4 + 15) * index,
            index
        }
    }
    renderKey = (item, index) => {
        return `${item.id.toString()}${index}`;
    }
    renderFooter = (isLoading: boolean) => {
        if (isLoading) {
            return (
                <Fragment>
                    <View style={{ ...STYLES.center, height: 60 }}>
                        {isLoading && <ActivityIndicator size='small' color='blue' />}
                    </View>
                </Fragment>
            );
        }
        return null
    };
    render() {
        const { receiptListStore, translate, navigation } = this.props
        const { isLoading, isRefreshing, isLoadingContainer } = receiptListStore

        return (

            <AppLayout onLayout={this.onLayout}>
                <FHeader title={translate('RECEIPT LIST')} typeLeftComponent='back' navigation={navigation} />

                <AppLayout isLoading={isLoadingContainer}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', borderColor: EColor.disable, borderWidth: 1 }}>
                            <View style={{ flex: 1, height: ITEM_SIZE }}>
                                <Input
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                    }}
                                    rightIcon={{ type: 'font-awesome', name: 'search', color: EColor.info }}
                                    keyboardType={'web-search'}
                                    returnKeyType='search'
                                    value={receiptListStore.keySearch}
                                    onChangeText={(text) => receiptListStore.keySearch = text}
                                    onSubmitEditing={this.search}
                                >
                                </Input>
                            </View>
                            <View style={{ width: ITEM_SIZE, height: ITEM_SIZE, justifyContent: 'center' }}>
                                <Icon type='material-community' name='barcode-scan'></Icon>
                            </View>

                        </View>
                        <AppLayout>
                            <View style={{ height: 50 }}>
                                <ButtonGroup
                                    onPress={receiptListStore.updateIndex}
                                    selectedIndex={receiptListStore.selectedIndex}
                                    selectedButtonStyle={{
                                        backgroundColor: EColor.purple
                                    }}
                                    buttons={TYPE_HEADER}
                                ></ButtonGroup>
                            </View>
                            <View style={{ flex: 1 }}>
                                <FlatList
                                    ListFooterComponent={this.renderFooter.bind(this, isLoading)}
                                    data={toJS(receiptListStore.data)}
                                    renderItem={this.renderItem}
                                    refreshControl={
                                        <RefreshControl
                                            colors={['blue']}
                                            refreshing={isRefreshing}
                                            onRefresh={this.onRefresh} />
                                    }
                                    getItemLayout={this.getItemLayout}
                                    initialNumToRender={16}
                                    keyExtractor={this.renderKey}
                                    onEndReachedThreshold={0.5}
                                    onEndReached={this.loadMore}
                                >

                                </FlatList>
                            </View>
                        </AppLayout>
                    </View>

                </AppLayout>
            </AppLayout>
        )
    }
}
export default injectCommonStore(ReceiptListScreen)