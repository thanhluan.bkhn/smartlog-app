import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { Input } from 'react-native-elements';
import { EColor } from '~/common/enums';
import { injectCommonStore } from '~/common/hoc';
import FHeader from '~/components/headers/FHeader';
import AppLayout from '~/layouts/AppLayout';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { BindStore } from '~/stores/base/decorator';
import { PalletTypeStore } from '~/stores/inbound/pallet-type.store';

interface IProps extends IBaseScreenProps {
    palletTypeStore?: PalletTypeStore
}

@BindStore('palletTypeStore')
class PalletTypeScreen extends BaseScreen<IProps> {
    async reload() {
       
    }
    async  componentDidMount() {
        const { countItemPallet } = this.props.palletTypeStore;
        await countItemPallet();
    }

    addOrMove = async () => {
        const { palletTypeStore} =this.props
        await palletTypeStore.addOrMove();
        
    }

    

    render() {

        const { navigation, palletTypeStore, translate, } = this.props;
        const { data,addOrMove ,isLoading} = palletTypeStore
        return (
            <AppLayout isLoading = {isLoading}>
                <FHeader title={translate('pallet type')} navigation={navigation} typeLeftComponent='back' />
                <View style={{ flexDirection: 'column' }}>
                    {Object.keys(data).map(key => {
                        return (
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={{ textAlign: 'center' }}>
                                        {key}
                                    </Text>
                                </View>
                                <View style={{ flex: 6 }}>
                                    <Input keyboardType= 'numeric' value={`${data[key]}`} onChangeText={(text) => {
                                        data[key] = text
                                    }} ></Input>
                                </View>
                            </View>
                        )
                    })}
                </View>
                <View style={{ flex: 1, margin: 10, marginTop: 50 }}>
                    <TouchableOpacity
                        onPress = {addOrMove}
                        style={{ width: '100%', backgroundColor: EColor.primary, borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center' }}
                    >
                        <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Complete')}</Text>
                    </TouchableOpacity>
                </View>
            </AppLayout>
        )
    }
}
export default injectCommonStore(PalletTypeScreen)
