import React, { Component } from 'react'
import { View, Text, FlatList, RefreshControl, Keyboard, BackHandler, NativeEventSubscription } from 'react-native'
import FHeader from '~/components/headers/FHeader'
import AppLayout from '~/layouts/AppLayout'
import { BindStore } from '~/stores/base/decorator';
import { STYLES, RECEIPT_STATUS } from '~/common/constants'
import { IScreenProps } from '~/stores/base/interfaces'
import { toJS } from 'mobx';
import { ICommonStore, injectCommonStore } from '~/common/hoc';
import FText from '~/components/texts/FText';
import { EColor } from '~/common/enums';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FButtonMatrix from '~/components/buttons/FButtonMatrix';
import navigationService from '~/services/navigation/navigation.service';
import uuid from 'uuid';
import Modal from '~/components/modals/Modal';
import { Input } from 'react-native-elements';
import { NavigationEventSubscription } from 'react-navigation';
import FInputDate from '~/components/inputs/FInputDate';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { ReceiptDetailStore } from '~/stores/inbound/receipt-detail.store';
import FTextIpunt from '~/components/texts/FTextInput';
import FDateTimePicker from '~/components/inputs/FDateTimePicker';
import Moment from 'moment-timezone';
import { toastService } from '~/services/toast/toast.service';
import { globalEvents } from '~/App';
import { EKeyEvent } from '~/common/enums/keyevent';
interface IProps extends IBaseScreenProps {
    receiptDetailStore?: ReceiptDetailStore,
}
@BindStore('receiptDetailStore')
class ReceiptDetailScreen extends BaseScreen<IProps> {

    constructor(props: IProps) {
        super(props);
        // globalEvents.on(EKeyEvent.RELOAD_RECEIPT_DETAIL, this.reload)
    }

    private backHandler: NativeEventSubscription;
    reload = async () => {

        const { receiptDetailStore } = this.props
        receiptDetailStore.data = [];
        await receiptDetailStore.load();
    }
    private willFocusSubscription: NavigationEventSubscription;
    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonPressAndroid);
        const { receiptDetailStore } = this.props

        receiptDetailStore.data = [];
        await receiptDetailStore.load();
    }
    handleBackButtonPressAndroid = () => {
        const { translate } = this.props
        const { isBack } = this.props.receiptDetailStore
        if (isBack) {
            return false
        } else {
            toastService.show(translate('Please completed the order entered'))
            return true
        }

    }
    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonPressAndroid
        );
    }
    onRefresh = async () => {
        const { receiptDetailStore } = this.props
        receiptDetailStore.data = [];
        await receiptDetailStore.onRefresh();
    }

    state = {
        isModalVisible: false,
        quantityNotEnough: ''

    }
    openModal = (item) => {
        const { receiptDetailStore } = this.props;
        receiptDetailStore.itemSelected = item;
        this.setState({ isModalVisible: true });
    };
    hidenModal = () => {
        this.setState({ isModalVisible: false });
    }

    gotoToolTallyScreen = (item) => {
        const { receiptDetailStore } = this.props;
        receiptDetailStore.itemSelected = item;
        navigationService.navigate('ToolTallyScreen')
    }
    pipeValue = (valueDate) => {
        const format = 'DD-MM-YYYY';

        if (!valueDate) {
            return ''
        }
        try {
            const output = Moment(valueDate).add(7, 'hours').format(format);
            if (!output) {
                return ''
            }
            if (output == 'Invalid date') {
                return ''
            }

            return output;
        } catch (error) {
            alert(JSON.stringify(error))
            return '';
        }

    }
    confirm = async () => {

        const { quantityNotEnough } = this.state;
        let qtylackof = 0;
        if (!isNaN(Number(quantityNotEnough))) {
            qtylackof = Number(quantityNotEnough)
        }

        const { receiptDetailStore, selectedWharehouse } = this.props
        console.log('-------------------');
        console.log({ code: selectedWharehouse.code });
        console.log('-------------------');
        const { storerkey, receiptkey, sku, lottable01, lottable04, lottable05 } = this.props.receiptDetailStore.itemSelected;
        this.hidenModal();

        await receiptDetailStore.confirm({
            qtylackof: qtylackof,
            whseid: selectedWharehouse.code,
            storerkey: storerkey,
            receiptkey: receiptkey,
            sku: sku,
            lottable01: lottable01 || undefined,
            lottable04: this.pipeValue(lottable04) || undefined,
            lottable05: this.pipeValue(lottable05) || undefined
        })


    }

    onChangeLottable01 = async (newLottable01: string = '', item) => {
        const { receiptDetailStore } = this.props;
        const { updateLottable01SBC } = this.props.receiptDetailStore;
        await updateLottable01SBC(newLottable01, item);
        receiptDetailStore.data = [];
        await receiptDetailStore.load();
    }

    onDateChange = async (newLottable04, item) => {
        const { receiptDetailStore } = this.props;
        const { updateProductionDateSBC } = this.props.receiptDetailStore;
        await updateProductionDateSBC(newLottable04, item);
        receiptDetailStore.data = [];
        await receiptDetailStore.load();
    }

    renderItem = ({ item, index }) => {
        const { isSpecial } = this.props
        const { trailernumber, externalreceiptkey2 } = this.props.receiptDetailStore.receiptSelected
        const { receiptkey, qtyexpected, qtyreceived, pallet, qtyreceivedcs,
            descr, sku, lottable01, lottable04, lottable05, toloc,
            qtyexpectedcs, unconfirm
        } = item;

        const checkConfirmLocation = isSpecial || Number(unconfirm) == 0;

        const { translate } = this.props

        let newPallet = 0;
        let newQty = 0;

        if (!isNaN(pallet)) {
            newPallet = Number(pallet);
        }
        if (!isNaN(pallet)) {
            newQty = Number(qtyexpected);
        }

        const oddPallet = pallet > 0 && newQty % pallet == 0 ? '' : newQty % pallet + '';
        const evenPallet = newPallet > 0 ? Math.floor(newQty / newPallet) + '' : qtyexpected + '';
        return (
            <View style={{ flex: 1 }}>
                <View style={{
                    flex: 1, flexDirection: 'column', margin: 15,
                    padding: 2, borderColor: EColor.blueInfo, borderWidth: 1
                }}>
                    <View>
                        <FText
                            label={translate('Car number')}
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5,

                            }}

                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={trailernumber} ></FText>
                        <FText
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5
                            }}
                            label={translate('Trip number')}
                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={externalreceiptkey2} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            fontSize={15}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            label={translate('Product')}
                            title={descr} ></FText>
                        <FText
                            fontSize={15}
                            label={translate('Sku')}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            title={sku} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Total CS')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${qtyexpectedcs}`} ></FText>
                        <FText
                            label={translate('Total Receive')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${qtyreceivedcs}`} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Pallet even')}
                            fontSize={14}

                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${evenPallet}`} ></FText>
                        <FText
                            label={translate('Pallet odd')}
                            fontSize={14}

                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${oddPallet}`} ></FText>


                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Location')}
                            fontSize={14}

                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${toloc}`} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FTextIpunt
                            disable={qtyreceived == qtyexpected || !checkConfirmLocation}
                            label={translate('Batch/lot')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                height: 80,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            returnKeyLabel={translate('Update')}
                            value={lottable01 || ''}
                            onSubmitEditing={(text) => {
                                this.onChangeLottable01(text, item)
                            }}
                        ></FTextIpunt>


                        <FDateTimePicker
                            disable={qtyreceived == qtyexpected || !checkConfirmLocation}
                            containerStyle={{
                                height: 80,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            onConfirm={(value) => {
                                this.onDateChange(value, item)
                            }}
                            label={translate('NSX')}
                            value={lottable04 ? Moment(lottable04).add(7, 'hours').toDate() : undefined}
                        />
                    </View>
                    <View style={{ flex: 1, margin: 20, flexDirection: 'row' }}>
                        <View style={{ flex: 8, marginLeft: 10, marginRight: 20 }}>
                            <TouchableOpacity
                                disabled={qtyreceived == qtyexpected || !checkConfirmLocation}
                                onPress={this.openModal.bind(this, item)}
                                style={{ flex: 1, backgroundColor: (qtyreceived == qtyexpected || !checkConfirmLocation) ? EColor.info : EColor.primary, height: 40, justifyContent: 'center' }}
                            >
                                <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Confirm')}</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <FButtonMatrix
                                disabled={qtyreceived == qtyexpected || !checkConfirmLocation}
                                color={(qtyreceived == qtyexpected || !checkConfirmLocation) ? EColor.info : EColor.primary}
                                key={uuid.v4()} size={40} onPress={this.gotoToolTallyScreen.bind(this, item)} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    renderKey = (item, index) => {
        return `${item.receiptkey.toString()}${index}`;
    }


    renderEmpty = () => (
        <View style={{ ...STYLES.center }}>
            <Text style={{ textAlign: 'center', fontSize: 24 }}>{this.props.translate('Empty data')}</Text>
        </View>
    )

    gotoPalletTypeScreen = () => {
        const { navigation } = this.props
        navigation.navigate('PalletTypeScreen')
    }
    checkBack = () => {
        const { isBack } = this.props.receiptDetailStore
        return isBack
    }
    render() {
        const { receiptDetailStore, translate, navigation } = this.props
        const { isLoading, isRefreshing, isBack } = receiptDetailStore
        const { isModalVisible, quantityNotEnough } = this.state
        return (
            <AppLayout>
                <FHeader
                    title={translate('RECEIPT DETAIL')}
                    navigation={navigation}
                    actionCondition={this.checkBack}
                    mesageCondition={translate('Please completed the order entered')}
                    typeLeftComponent='backCondition' />
                <AppLayout isLoading={isLoading}>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={toJS(receiptDetailStore.data)}
                            renderItem={this.renderItem}
                            refreshControl={
                                <RefreshControl
                                    colors={['blue']}
                                    refreshing={isRefreshing}
                                    onRefresh={this.onRefresh} />
                            }
                            initialNumToRender={16}
                            keyExtractor={this.renderKey}
                        >
                        </FlatList>
                    </View>
                    <View style={{ height: 40, width: '100%', marginBottom: 2 }}>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity
                                    disabled={!isBack}
                                    onPress={receiptDetailStore.reject}
                                    style={{
                                        width: '100%',
                                        backgroundColor: isBack ? EColor.white : EColor.disable,
                                        borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center'
                                    }}
                                >
                                    <Text style={{ textAlign: 'center', color: isBack ? EColor.primary : EColor.black }}>{translate('Reject')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity
                                    disabled={isBack}
                                    onPress={this.gotoPalletTypeScreen}
                                    style={{
                                        width: '100%', backgroundColor: isBack ? EColor.disable : EColor.primary,
                                        borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center'
                                    }}
                                >
                                    <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Complete')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </AppLayout>
                <Modal
                    translate={translate}
                    title={'Confirm CS'}
                    isVisible={isModalVisible}
                    onBackdropPress={this.hidenModal}
                    confirm={this.confirm}
                >
                    <Input
                        inputContainerStyle={{
                            borderWidth: 1,
                        }}
                        blurOnSubmit={false}
                        placeholderTextColor={EColor.blueInfo}

                        inputStyle={{
                            marginLeft: 10,
                        }}
                        onChangeText={(text) => {
                            this.setState({
                                quantityNotEnough: text
                            })
                        }}
                        keyboardType='numeric'
                        onSubmitEditing={() => Keyboard.dismiss()}
                        value={quantityNotEnough}
                        leftIcon={{ type: 'font-awesome', name: 'lock', color: EColor.white }}
                    />
                </Modal>
            </AppLayout>
        )
    }
}
export default injectCommonStore(ReceiptDetailScreen)


