import React, { Component, Fragment } from 'react'
import { Text, View, FlatList, RefreshControl, ActivityIndicator } from 'react-native'
import { BindStore } from '~/stores/base/decorator'
import { StockCountStore } from '~/stores/inventory/stock-count.store'
import { ICommonStore } from '~/common/hoc'
import { STYLES } from '~/common/constants'
import StockCountItem from './StockCountItem'

interface IProps {
    stockCountStore?: StockCountStore,
    sizeItem: number,
    fontSize: number,
}

@BindStore('stockCountStore')
export default class SkuxLotList extends Component<IProps> {

    async componentDidMount() {
        const { stockCountStore } = this.props;
        stockCountStore.data = [];
        await stockCountStore.load(1);
    }
    onRefresh = async () => {
        const { onRefresh } = this.props.stockCountStore
        await onRefresh();
    }
    renderItem = ({ item, index }) => {
        const { fontSize, sizeItem } = this.props
        return (
            <StockCountItem fontSize={fontSize} sizeItem={sizeItem} item={item} />
        )
    }
    loadMore = async () => {

        const { stockCountStore } = this.props

        const { isLoading, data, total } = stockCountStore

        if (isLoading || data.length === total) {
            return
        }
        await stockCountStore.loadMore();
    }

    renderFooter = () => {

        const { data, total, isLoading } = this.props.stockCountStore

        if (isLoading) {
            return (
                <Fragment>
                    <View style={{ ...STYLES.center, height: 60 }}>
                        <ActivityIndicator size='small' color='blue' />
                    </View>
                </Fragment>
            );
        }
        return null
    };
    renderKey = (item, index) => {
        return `${index}`;
    }
    getItemLayout = (data, index) => {
        const { sizeItem } = this.props;
        return {
            length: sizeItem * 3 + 15,
            offset: (sizeItem * 3 + 15) * index,
            index
        }
    }
    render() {
        const { stockCountStore } = this.props
        const { data, isLoading, isRefreshing } = stockCountStore
        return (
            <FlatList
                data={data}
                renderItem={this.renderItem}
                keyExtractor={this.renderKey}
                ListFooterComponent={this.renderFooter}
                refreshControl={
                    <RefreshControl
                        colors={['blue']}
                        refreshing={isRefreshing}
                        onRefresh={this.onRefresh} />
                }
                getItemLayout={this.getItemLayout}
                initialNumToRender={16}
                onEndReachedThreshold={0.5}
                onEndReached={this.loadMore}
            />
        )
    }
}
