import React, { Component, Fragment } from 'react'
import { Text, View, FlatList, RefreshControl, ActivityIndicator } from 'react-native'
import { BindStore } from '~/stores/base/decorator'
import { STYLES } from '~/common/constants'
import { StockCountSkuStore } from '~/stores/inventory/stock-count-sku.store'
import StockCountSkuItem from './StockCountSkuItem'

interface IProps {
    stockCountSkuStore?: StockCountSkuStore,
    sizeItem: number,
    fontSize: number,
}

@BindStore('stockCountSkuStore')
export default class SkuList extends Component<IProps> {

    async componentDidMount() {
        const { stockCountSkuStore } = this.props;
        stockCountSkuStore.data = [];
        await stockCountSkuStore.load(1);
    }
    onRefresh = async () => {
        const { onRefresh } = this.props.stockCountSkuStore
        await onRefresh();
    }
    renderItem = ({ item, index }) => {
        const { fontSize, sizeItem } = this.props
        return (
            <StockCountSkuItem fontSize={fontSize} sizeItem={sizeItem} item={item} />
        )
    }
    loadMore = async () => {

        const { stockCountSkuStore } = this.props

        const { isLoading, data, total } = stockCountSkuStore

        if (isLoading || data.length === total) {
            return
        }
        await stockCountSkuStore.loadMore();
    }

    renderFooter = () => {

        const { data, total, isLoading } = this.props.stockCountSkuStore

        if (isLoading) {
            return (
                <Fragment>
                    <View style={{ ...STYLES.center, height: 60 }}>
                        <ActivityIndicator size='small' color='blue' />
                    </View>
                </Fragment>
            );
        }
        return null
    };
    renderKey = (item, index) => {
        return `${index}`;
    }
    getItemLayout = (data, index) => {
        const { sizeItem } = this.props;
        return {
            length: sizeItem * 2 + 10,
            offset: (sizeItem * 2 + 10) * index,
            index
        }
    }
    render() {
        const { stockCountSkuStore } = this.props
        const { data, isLoading, isRefreshing } = stockCountSkuStore
        return (
            <FlatList
                data={data}
                renderItem={this.renderItem}
                keyExtractor={this.renderKey}
                ListFooterComponent={this.renderFooter}
                refreshControl={
                    <RefreshControl
                        colors={['blue']}
                        refreshing={isRefreshing}
                        onRefresh={this.onRefresh} />
                }
                getItemLayout={this.getItemLayout}
                initialNumToRender={16}
                onEndReachedThreshold={0.5}
                onEndReached={this.loadMore}
            />
        )
    }
}
