import React, { Component, Fragment } from 'react'
import { Text, View, FlatList, LayoutChangeEvent, ActivityIndicator, RefreshControl } from 'react-native'
import { injectCommonStore } from '~/common/hoc'
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen'
import AppLayout from '~/layouts/AppLayout'
import FHeader from '~/components/headers/FHeader'
import { StockCountStore } from '~/stores/inventory/stock-count.store'
import { ESize, EColor } from '~/common/enums'
import { STYLES, TYPE_HEADER_INVENTORY } from '~/common/constants'
import { BindStore } from '~/stores/base/decorator'
import StockCountItem from './StockCountItem'
import { Input, Icon, ButtonGroup } from 'react-native-elements'
import SkuxLotList from './SkuxLotList'
import SkuList from './SkuList'
import { StockCountSkuStore } from '~/stores/inventory/stock-count-sku.store'
interface IProps extends IBaseScreenProps {
    stockCountStore?: StockCountStore,
    stockCountSkuStore?: StockCountSkuStore
}
const ITEM_SIZE = 60;
const scale = 8;

@BindStore('stockCountStore', 'stockCountSkuStore')
class StockCountScreen extends BaseScreen<IProps> {

    state = {
        sizeItem: ESize.WIDTH_WINDOW / scale,
        fontSize: ESize.WIDTH_WINDOW / (scale * 5)
    }
    async reload(): Promise<void> {

    }
    search = async () => {
        const { stockCountStore, stockCountSkuStore } = this.props
        const { valueSelected } = stockCountStore
        if (valueSelected == "SKU") {
            await stockCountSkuStore.search();
        } else {
            await stockCountStore.search()
        }



    }

    onLayout = (event: LayoutChangeEvent) => {
        const { width } = event.nativeEvent.layout
        const sizeItem = width / scale;
        const fontSize = width / (scale * 5);
        this.setState({
            sizeItem, fontSize
        })
    }
    renderSelect = () => {
        const { valueSelected } = this.props.stockCountStore
        const { sizeItem, fontSize } = this.state
        if (valueSelected == "SKU") {
            return <SkuList fontSize={fontSize} sizeItem={sizeItem} />
        }
        return <SkuxLotList fontSize={fontSize} sizeItem={sizeItem} />
    }
    render() {
        const { translate, stockCountStore, navigation, stockCountSkuStore } = this.props

        const { valueSelected } = stockCountStore
        return (
            <AppLayout isLoading={false} onLayout={this.onLayout}>
                <FHeader title={translate('Inventory by ' + valueSelected)} navigation={navigation} typeLeftComponent='back' />
                <View style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row', borderColor: EColor.disable, borderWidth: 1 }}>
                        <View style={{ flex: 1, height: ITEM_SIZE }}>
                            <Input
                                inputContainerStyle={{
                                    borderBottomWidth: 0,
                                }}
                                rightIcon={{ type: 'font-awesome', name: 'search', color: EColor.info }}
                                keyboardType={'web-search'}
                                returnKeyType='search'
                                value={valueSelected == "SKU" ? stockCountSkuStore.keySearch : stockCountStore.keySearch}
                                onChangeText={(text) => {
                                    if (valueSelected == "SKU") {
                                        stockCountSkuStore.keySearch = text
                                    } else {
                                        stockCountStore.keySearch = text
                                    }
                                }}
                                onSubmitEditing={this.search}
                            >
                            </Input>
                        </View>
                        <View style={{ width: ITEM_SIZE, height: ITEM_SIZE, justifyContent: 'center' }}>
                            <Icon type='material-community' name='barcode-scan'></Icon>
                        </View>

                    </View>
                    <AppLayout >
                        <View style={{ height: 50 }}>
                            <ButtonGroup
                                onPress={stockCountStore.updateIndex}
                                selectedIndex={stockCountStore.selectedIndex}
                                selectedButtonStyle={{
                                    backgroundColor: EColor.purple
                                }}
                                buttons={TYPE_HEADER_INVENTORY}
                            ></ButtonGroup>
                        </View>
                        <View style={{ flex: 1 }}>
                            {this.renderSelect()}
                        </View>
                    </AppLayout>
                </View>
            </AppLayout>
        )
    }
}

export default injectCommonStore(StockCountScreen)