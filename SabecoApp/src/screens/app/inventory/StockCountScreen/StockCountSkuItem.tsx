import Moment from 'moment-timezone';
import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { EColor } from '~/common/enums';
import { ICommonStore } from '~/common/hoc';
import { bindCommon, BinPropertyStore } from '~/stores/base/decorator';
import { StockCountSkuStore } from '~/stores/inventory/stock-count-sku.store';
import { InventoryBySku } from '~/stores/inventory/inventory.models';


interface IProps extends ICommonStore {
    item: InventoryBySku,
    sizeItem: number,
    fontSize: number,
    stockCountSkuStore?: StockCountSkuStore
}

@BinPropertyStore(store => {
    const common = bindCommon(store);
    return {
        ...common,
        stockCountSkuStore: store.stockCountSkuStore
    }
})
export default class StockCountSkuItem extends Component<IProps> {
    pipePallet = ({ qty, pallet = 0 }) => {
        let newQty = qty;
        let newPallet = pallet;
        if (newPallet != 0) {
            newPallet = Number(newPallet);
        }
        if (newQty != 0) {
            newQty = Number(newQty);
        }
        const oddPallet = newPallet > 0 && newQty % newPallet == 0 ? 0 + '' : newQty % newPallet + '';
        const evenPallet = newPallet > 0 ? Math.floor(newQty / newPallet) + '' : newQty + '';
        return {
            oddPallet,
            evenPallet
        }
    }
    render() {
        const { item, translate, sizeItem, fontSize } = this.props
        const { sku, qtyavalable, pallet, qty, qtypickedAndAllocated, qtyallocated, qtypicked } = item
        const itemQty = this.pipePallet({ qty, pallet });
        const itemQtypickedAndAllocated = this.pipePallet({ qty: qtypickedAndAllocated, pallet })
        return (
            <View style={[styles.containerItem, { height: sizeItem * 2 + 40, margin: 10 }]}>
                <View style={[styles.rowItem, { height: sizeItem }]}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Sku')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5, fontWeight: 'bold', color: EColor.brandPrimary }}>{item.sku}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Sku group')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.5, fontWeight: 'bold', color: EColor.brandPrimary }}>{item.skugroup}</Text>
                        </View>

                    </View>
                </View>
                <View style={[styles.rowItem, { height: sizeItem }]}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Quantity')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5, color: EColor.textColorTable, fontWeight: 'bold' }}>{qty}</Text>
                            <Text style={{ fontSize: fontSize * 1.2, color: EColor.purple }}>{`(${itemQty.evenPallet}/${itemQty.oddPallet} )`}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Qty allocated and picked')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5, color: EColor.textColorTable, fontWeight: 'bold' }}>{qtypickedAndAllocated}</Text>
                            <Text style={{ fontSize: fontSize * 1.2, color: EColor.purple, fontWeight: 'bold' }}>{`(${itemQtypickedAndAllocated.evenPallet}/${itemQtypickedAndAllocated.oddPallet} )`}</Text>
                        </View>

                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    rowItem: {
        flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1,
        marginLeft: 16,
        flexDirection: 'row',
        marginRight: 16
    },
    containerItem: {
        flexDirection: 'column', margin: 5, flex: 1, borderRadius: 6, shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2
    }
})