import React, { Component } from 'react'
import { View, Picker } from 'react-native'
import FPicker from '~/components/picker/FPicker';
import { Button } from 'react-native-elements'
import { STYLES } from '~/common/constants';
import { BindStore } from '~/stores/base/decorator';
import { AuthStore } from '~/stores/auth/auth.store';
import { injectCommonStore, ICommonStore } from '~/common/hoc';


interface IProps extends ICommonStore {
    authStore?: AuthStore
}

@BindStore('authStore')
class DrawerScreen extends Component<IProps> {

    logout = () => {
        this.props.authStore.logout()
    }
    onValueChange = (value) => {
        const { translate, authStore } = this.props
        authStore.changeWarehouse(value);
    }
    render() {
        const { translate, authStore } = this.props
        const mapItems = authStore.whseids.map(item => {
            return {
                value: item,
                label: item
            }
        }) || []
        return (
            <View style={{ flex: 1, flexDirection: 'column' ,paddingLeft: 10}}>
                <View style={{ flex: 3 }}>

                </View>
                <View style={{ flex: 6 }}>
                    <FPicker
                        onValueChange={this.onValueChange}
                        title={`${translate('Select warehouse')}`}
                        items={mapItems} />
                </View>
                <View style={{ ...STYLES.center, flex: 1 }}>
                    <Button
                        containerStyle={{
                            width: '80%'
                        }}
                        onPress={this.logout}
                        title={'LOGOUT'}
                    ></Button>
                </View>
            </View>
        )
    }
}
export default injectCommonStore(DrawerScreen);
