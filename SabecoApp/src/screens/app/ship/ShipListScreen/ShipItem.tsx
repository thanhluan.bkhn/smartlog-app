import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { ORDER_STATUS } from '~/common/constants';
import { EColor } from '~/common/enums';
import { ICommonStore } from '~/common/hoc';
import { BindStore } from '~/stores/base/decorator';
import { ShipListStore } from '~/stores/ship/ship-list.store';
import { NavigationScreenProp } from 'react-navigation';
import { CaptureShipStore } from '~/stores/ship/capture-ship.store';


interface IProps extends ICommonStore {
    item: {
        [key: string]: any
    },
    sizeItem: number,
    fontSize: number,
    navigation: NavigationScreenProp<any>,
    shipListStore?: ShipListStore,
    captureShipStore?: CaptureShipStore
}
@BindStore('shipListStore','captureShipStore')
export default class ShipItem extends Component<IProps> {


    pipeStatus = (status: string) => {
        const defaultValue = {
            title: '',
            color: EColor.black
        }
        if (!status) return defaultValue;
        const values = Object.values(ORDER_STATUS);
        const indexFind = values.findIndex(item => {
            return item.value.trim() == status.trim()
        });
        if (indexFind == -1) {
            return defaultValue;
        }
        const { translate } = this.props;
        return {
            title: translate(values[indexFind].text),
            color: values[indexFind].color
        }
    }
    openCamera = () => {
        const { id } = this.props.item
        const { navigation ,captureShipStore} = this.props;
        captureShipStore.idSelected = id;
        navigation.navigate('CaptureShipScreen')
    }
    gotoDetail = async () => {

        const { shipListStore, userInfo, item } = this.props
        const { orderkey, trailernumber = '', id,externalorderkey2 } = item;
        const body = {
            keeper: userInfo.username,
            id: id,
            externalorderkey2:externalorderkey2
        }
        await shipListStore.updateUserAction(body, orderkey, trailernumber);

    }
    gotoViewDetail = () => {
        const { shipListStore, userInfo, item } = this.props
        const { orderkey, trailernumber = '', id,externalorderkey2 } = item;
        const {gotoViewDetail} = shipListStore
        const body = {
            keeper: userInfo.username,
            id: id,
            externalorderkey2:externalorderkey2
        }
        gotoViewDetail(body, orderkey, trailernumber)
    }
    renderKeeper = () => {

        const { translate, userInfo, item, fontSize } = this.props;

        const { status, keeper } = item;
        if (status == ORDER_STATUS.ShipCompleted.value ) {
            return (
                <Button
                    disabled={true}

                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.info
                    }} title={keeper} />
            )
        }
        if (keeper) {
            if (keeper != userInfo.username) {
                return (
                    <Button
                        disabled={true}

                        iconRight={true}
                        icon={() => {
                            return (
                                <Icon
                                    iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                    size={fontSize}
                                    color={EColor.white}
                                    name={'ios-arrow-forward'}
                                    type={'ionicon'}
                                />
                            )
                        }}
                        titleStyle={{
                            fontSize: fontSize
                        }}
                        buttonStyle={{
                            height: '80%',
                            backgroundColor: EColor.info
                        }} title={keeper} />
                )
            }
            return (
                <Button
                    onPress={this.gotoDetail}
                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.primary
                    }} title={keeper} />
            )
        }

        return (
            <Button

                onPress={this.gotoDetail}
                iconRight={true}
                icon={() => {
                    return (
                        <Icon
                            iconStyle={{ marginLeft: 8, marginTop: 4 }}
                            size={fontSize}
                            color={EColor.white}
                            name={'ios-arrow-forward'}
                            type={'ionicon'}
                        />
                    )
                }}
                titleStyle={{
                    fontSize: fontSize
                }}
                buttonStyle={{
                    height: '80%',
                    backgroundColor: '#00F2FE'
                }} title={translate('Start')} />
        )
    }

    render() {
        const { item, sizeItem, fontSize, translate } = this.props
        const { lifter, externalorderkey2, trailernumber, qtyoriginal, status,order_type ,sku,externorderkey,carriercode} = this.props.item
        const valuePipeStatus = this.pipeStatus(status);
        return (
            <View style={{
                height: sizeItem * 5 + 25,
                flexDirection: 'column', margin: 5, flex: 1, borderRadius: 6, shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,

                elevation: 1
            }}>
                <View style={{

                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Command')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize, color: EColor.textColorTable }}>{externorderkey}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Status')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize, color: valuePipeStatus.color,textAlign: 'center' }}>{valuePipeStatus.title}</Text>
                        </View>
                        <View style={{ width: 50, justifyContent: 'center' }}>
                            <TouchableOpacity 
                                onPress = {this.gotoViewDetail}
                            style={{ width: 40, height: 40, justifyContent: 'center', borderColor: EColor.blueInfo, backgroundColor: EColor.brandInfo, borderWidth: 1, borderRadius: 50 }}>
                                <Icon name='eye' type='entypo' color={EColor.brandWarning} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Car number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.4, color: '#58D2F3' }}>{trailernumber}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Quantity')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5,color:EColor.textColorTable,fontWeight: 'bold'}}>{qtyoriginal}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Trip number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.1 , color: '#58D2F3' }}>{externalorderkey2}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style = {{fontSize: fontSize * 1.1 }}>{translate('Sku')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize * 1.5,fontWeight: 'bold' ,color:EColor.brandPrimary}}>{sku}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height:sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize }}>{translate('Transaction type')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize ,color:EColor.textColorTable}}>{order_type}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize}}>{translate('Capture')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                        <Icon onPress = {this.openCamera} type='material-community' name='camera' size = {40} color = {EColor.purple}></Icon>
                        </View>
                    </View>
                </View>
                <View style={{
                    height: sizeItem,
                    margin: 16,
                    justifyContent: 'center'
                }}>
                    {this.renderKeeper()}

                </View>
            </View>
        )
    }
}
