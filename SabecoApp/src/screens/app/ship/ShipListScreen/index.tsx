import React, { Component, Fragment } from 'react';
import { ActivityIndicator, FlatList, LayoutChangeEvent, RefreshControl, View } from 'react-native';
import { Icon, Input, ButtonGroup } from 'react-native-elements';
import { STYLES, TYPE_HEADER } from '~/common/constants';
import { EColor, ESize } from '~/common/enums';
import { injectCommonStore } from '~/common/hoc';
import FHeader from '~/components/headers/FHeader';
import AppLayout from '~/layouts/AppLayout';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { BindStore } from '~/stores/base/decorator';

import ShipItem from './ShipItem';
import { ShipListStore } from '~/stores/ship/ship-list.store';



interface IProps extends IBaseScreenProps {
    shipListStore?: ShipListStore;
}

const ITEM_SIZE = 60;
const scale = 8;
@BindStore('shipListStore')
class ShipListScreen extends BaseScreen<IProps> {
    async reload() {

    }

    state = {
        sizeItem: ESize.WIDTH_WINDOW / scale,
        fontSize: ESize.WIDTH_WINDOW / (scale * 5)
    }

    async componentDidMount() {
        const { shipListStore } = this.props;
        shipListStore.data = [];
        await shipListStore.load(1);
    }



    onRefresh = async () => {
        const { onRefresh } = this.props.shipListStore
        await onRefresh();
    }
    loadMore = async () => {

        const { shipListStore } = this.props

        const { isLoading, data, total } = shipListStore
        if (isLoading || data.length === total) {
            return
        }
        await shipListStore.loadMore();
    }

    search = async () => {
        const { search } = this.props.shipListStore

        await search();
    }
    renderKey = (item, index) => {
        return `${item.id.toString()}${index}`;
    }
    onLayout = (event: LayoutChangeEvent) => {
        const { width } = event.nativeEvent.layout
        const sizeItem = width / scale;
        const fontSize = width / (scale * 5);
        this.setState({
            sizeItem, fontSize
        })
    }
    renderItem = ({ item, index }) => {
        const { translate, userInfo, navigation } = this.props
        const { fontSize, sizeItem } = this.state;
        return (
            <ShipItem navigation={navigation} item={item} fontSize={fontSize} sizeItem={sizeItem} translate={translate} userInfo={userInfo} />
        )
    }

    renderFooter = (isLoading: boolean) => {
        if (isLoading) {
            return (
                <Fragment>
                    <View style={{ ...STYLES.center, height: 60 }}>
                        {isLoading && <ActivityIndicator size='small' color='blue' />}
                    </View>
                </Fragment>
            );
        }
        return null
    };
    getItemLayout = (data, index) => {
        const { sizeItem } = this.state;
        return {
            length: sizeItem * 3 + 15,
            offset: (sizeItem * 3 + 15) * index,
            index
        }
    }
    render() {
        const { navigation, shipListStore, translate } = this.props
        const { data, isRefreshing, isLoading, isLoadingContainer } = shipListStore
        return (
            <AppLayout onLayout={this.onLayout}>
                <FHeader navigation={navigation} typeLeftComponent='back' title={translate('Ship list')} />
                <AppLayout isLoading={isLoadingContainer}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', borderColor: EColor.disable, borderWidth: 1 }}>
                            <View style={{ flex: 1, height: ITEM_SIZE }}>
                                <Input
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                    }}
                                    rightIcon={{ type: 'font-awesome', name: 'search', color: EColor.info }}
                                    keyboardType={'web-search'}
                                    returnKeyType='search'
                                    value={shipListStore.keySearch}
                                    onChangeText={(text) => shipListStore.keySearch = text}
                                    onSubmitEditing={this.search}
                                >

                                </Input>
                            </View>
                            <View style={{ width: ITEM_SIZE, height: ITEM_SIZE, justifyContent: 'center' }}>
                                <Icon type='material-community' name='barcode-scan'></Icon>
                            </View>

                        </View>
                        <AppLayout>
                            <View style={{ height: 50 }}>
                                <ButtonGroup
                                    onPress={shipListStore.updateIndex}
                                    selectedIndex={shipListStore.selectedIndex}
                                    selectedButtonStyle={{
                                        backgroundColor: EColor.purple
                                    }}
                                    buttons={TYPE_HEADER}
                                ></ButtonGroup>
                            </View>
                            <View style={{ flex: 1 }}>
                                <FlatList
                                    data={data}
                                    renderItem={this.renderItem}

                                    keyExtractor={this.renderKey}
                                    ListFooterComponent={this.renderFooter.bind(this, isLoading)}
                                    refreshControl={
                                        <RefreshControl
                                            colors={['blue']}
                                            refreshing={isRefreshing}
                                            onRefresh={this.onRefresh} />
                                    }
                                    getItemLayout={this.getItemLayout}
                                    initialNumToRender={16}
                                    onEndReachedThreshold={0.5}
                                    onEndReached={this.loadMore}
                                >

                                </FlatList>
                            </View>
                        </AppLayout>
                    </View>

                </AppLayout>
            </AppLayout>
        )
    }
}

export default injectCommonStore(ShipListScreen)
