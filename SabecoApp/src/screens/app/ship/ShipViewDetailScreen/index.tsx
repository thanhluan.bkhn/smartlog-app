import { toJS } from 'mobx';
import Moment from 'moment-timezone';
import React, { Component, Fragment } from 'react';
import { FlatList, RefreshControl, Text, View } from 'react-native';
import { STYLES } from '~/common/constants';
import { EColor } from '~/common/enums';
import { injectCommonStore } from '~/common/hoc';
import FHeader from '~/components/headers/FHeader';
import FText from '~/components/texts/FText';
import AppLayout from '~/layouts/AppLayout';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { BindStore } from '~/stores/base/decorator';
import { ShipDetailStore } from '~/stores/ship/ship-detail.store';


interface IProps extends IBaseScreenProps {
    shipDetailStore?: ShipDetailStore,
}
@BindStore('shipDetailStore')
class ShipViewDetailScreen extends BaseScreen<IProps> {
  
    async reload(){
    }
    async  componentDidMount() {
        
        const { shipDetailStore } = this.props
        shipDetailStore.data = [];
        await shipDetailStore.load();
    }


    

    onRefresh = async () => {
        const { shipDetailStore } = this.props

        shipDetailStore.data = [];
        await shipDetailStore.onRefresh();
    }
   

    renderItem = ({ item, index }) => {

        const { shipDetailStore, translate } = this.props
        const { orderkey, trailernumber,externalorderkey2 } = shipDetailStore.orderSelected
        let { pallet = 0,
            shippedqty,
            originalqty = 0,
            lottable04 = ''
        } = item;
        const { descr, sku, lottable01 = '', location = '', status } = toJS(item);
        if (pallet != 0) {
            pallet = Number(pallet);
        }
        if (originalqty != 0) {
            originalqty = Number(originalqty);
        }
        const oddPallet = pallet > 0 && originalqty % pallet == 0 ? '' : originalqty % pallet + '';
        const evenPallet = pallet > 0 ? Math.floor(originalqty / pallet) + '' : originalqty + '';
        
    
        return (

            <View style={{ flex: 1 }}>

                <View style={{ flex: 1, flexDirection: 'column', margin: 15, padding: 2, borderColor: EColor.blueInfo, borderWidth: 1 }}>
                    <View>
                        <FText
                            label={translate('Car number')}
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5,

                            }}

                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={trailernumber} ></FText>
                        <FText
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5
                            }}
                            label={translate('Trip number')}
                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={externalorderkey2} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            fontSize={15}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            label={translate('Product')}
                            title={descr} ></FText>
                        <FText
                            fontSize={15}
                            label={translate('Sku')}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            title={sku} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Total CS')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${originalqty}`} ></FText>
                        <FText
                            label={translate('Total case picked')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${shippedqty}`} ></FText>


                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Date')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${lottable04 ? Moment(lottable04).format("DD-MM-YYYY") : ''}`} ></FText>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Pallet even')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${evenPallet}`} ></FText>
                        <FText
                            label={translate('Pallet odd')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${oddPallet}`} ></FText>

                        <FText
                            label={translate('Batch/lot')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${lottable01}`} ></FText>
                        <FText

                            label={translate('Location')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${location}`} ></FText>
                    </View>
                </View>
            </View>
        )
    }



    renderKey = (item, index) => {
        return `${index}`;
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };
    renderEmpty = () => (
        <View style={{ ...STYLES.center }}>
            <Text style={{ textAlign: 'center', fontSize: 24 }}>{this.props.translate('Empty data')}</Text>
        </View>
    )

    

    render() {
        const { shipDetailStore, navigation, translate } = this.props
        const { isLoading, isRefreshing,isBack } = shipDetailStore
        return (
            <AppLayout isLoading={isLoading}>
                <FHeader 
                title={translate('Ship detail')} 
                navigation={navigation} 
                typeLeftComponent='back'
                />
                <AppLayout>
                    <View style={{ flex: 1 }}>
                        <FlatList

                            data={toJS(shipDetailStore.data)}
                            renderItem={this.renderItem}
                            refreshControl={
                                <RefreshControl
                                    colors={['blue']}
                                    refreshing={isRefreshing}
                                    onRefresh={this.onRefresh} />
                            }
                            initialNumToRender={16}
                            keyExtractor={this.renderKey}
                            ItemSeparatorComponent={this.renderSeparator}
                        >
                        </FlatList>
                    </View>
                </AppLayout>
            </AppLayout>
        )
    }
}
export default injectCommonStore(ShipViewDetailScreen)


