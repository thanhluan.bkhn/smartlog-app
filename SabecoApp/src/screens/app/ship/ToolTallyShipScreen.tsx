import React, { Component } from 'react';
import AppLayout from '~/layouts/AppLayout';
import FHeader from '~/components/headers/FHeader';
import { ICommonStore, injectCommonStore } from '~/common/hoc';
import FViewMatrix from '~/components/views/FViewMatrix';
import uuid from 'uuid';
import { BindStore } from '~/stores/base/decorator';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { ShipDetailStore } from '~/stores/ship/ship-detail.store';


interface IProps extends IBaseScreenProps {
    shipDetailStore?: ShipDetailStore
}
@BindStore('shipDetailStore')
class ToolTallyShipScreen extends BaseScreen<IProps> {
    async reload(){
    }
    render() {
        const { translate ,shipDetailStore,navigation} = this.props
        const { qtypalletpicked} = shipDetailStore.itemSelected
        const total = qtypalletpicked == Math.floor(qtypalletpicked) ? qtypalletpicked : Math.floor(qtypalletpicked) + 1
       
        return (
                <AppLayout>
                    <FHeader title={translate('Tool tally')} navigation = {navigation} typeLeftComponent='back' />
                    <FViewMatrix
                        numberOfCheck = {0} 
                        translate = {translate}
                        key={uuid.v4()}
                        divisionNumber={8}
                        total={total}
                    />
                </AppLayout>
        )
    }
}
export default injectCommonStore(ToolTallyShipScreen)