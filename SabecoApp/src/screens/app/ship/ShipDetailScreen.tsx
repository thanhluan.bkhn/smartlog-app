
import Moment from 'moment-timezone';
import React, { Component, Fragment } from 'react';
import { FlatList, RefreshControl, Text, View, BackHandler, NativeEventSubscription } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import uuid from 'uuid';
import {  STYLES, PICKDETAIL_STATUS } from '~/common/constants';
import { EColor } from '~/common/enums';
import { injectCommonStore } from '~/common/hoc';
import FButtonMatrix from '~/components/buttons/FButtonMatrix';
import FHeader from '~/components/headers/FHeader';
import FText from '~/components/texts/FText';
import AppLayout from '~/layouts/AppLayout';
import { toJS } from 'mobx';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import navigationService from '~/services/navigation/navigation.service';
import { BindStore } from '~/stores/base/decorator';
import { ShipDetailStore } from '~/stores/ship/ship-detail.store';
import { toastService } from '~/services/toast/toast.service';

interface IProps extends IBaseScreenProps {
    shipDetailStore?: ShipDetailStore,
}
@BindStore('shipDetailStore')
class ShipDetailScreen extends BaseScreen<IProps> {
    private backHandler: NativeEventSubscription;
    async reload(){
    }
    async  componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonPressAndroid);
        const { shipDetailStore } = this.props
        shipDetailStore.data = [];
        await shipDetailStore.load();
    }


    handleBackButtonPressAndroid = () => {
        const { translate} = this.props
        const {isBack} = this.props.shipDetailStore
        if(isBack) {
            return false
        } else {
            toastService.show(translate('Please completed the order entered'))
            return true
        }
       
    }
    componentWillUnmount() {
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.handleBackButtonPressAndroid
        );
      }

    onRefresh = async () => {
        const { shipDetailStore } = this.props

        shipDetailStore.data = [];
        await shipDetailStore.onRefresh();
    }
    checkBack = () => {
        const {isBack} = this.props.shipDetailStore
        return  isBack
    }
    ship = async (item) => {
        const { ship } = this.props.shipDetailStore;
        await ship(item);

    }
    gotoToolTallyScreen = (item) => {
        const { shipDetailStore } = this.props;
        shipDetailStore.itemSelected = item;
        navigationService.navigate('ToolTallyShipScreen')
    }

    renderItem = ({ item, index }) => {

        const { shipDetailStore, translate,isSpecial } = this.props
        
        const { orderkey, trailernumber,externalorderkey2 } = shipDetailStore.orderSelected
        let { pallet = 0,
            shippedqty,
            originalqty = 0,
            lottable04 = ''
        } = item;
        const { descr, sku, lottable01 = '', location = '', status } = toJS(item);
        if (pallet != 0) {
            pallet = Number(pallet);
        }
        if (originalqty != 0) {
            originalqty = Number(originalqty);
        }
        const oddPallet = pallet > 0 && originalqty % pallet == 0 ? '' : originalqty % pallet + '';
        const evenPallet = pallet > 0 ? Math.floor(originalqty / pallet) + '' : originalqty + '';
        let renderActions = null;
        let isHiden = true
        if(isSpecial) {
            isHiden = PICKDETAIL_STATUS.Shipped.value == status 
        } else {
            isHiden = PICKDETAIL_STATUS.Shipped.value == status  || PICKDETAIL_STATUS.Allocated.value == status
        }
        
        
        if (isHiden) {
            const itemOk = Object.values(PICKDETAIL_STATUS).find(item => item.value == status);
            const textStatus = itemOk ? itemOk.text : ''
            renderActions = (
                <View style={{ height: 40, justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', color: itemOk.color }}> {translate(textStatus)}</Text>
                </View>
            )
        } else {
            renderActions = (
                <Fragment>
                    <TouchableOpacity
                        onPress={this.ship.bind(this, item)}
                        style={{ flex: 1, backgroundColor: EColor.primary, height: 40, justifyContent: 'center' }}
                    >
                        <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Ship')}</Text>
                    </TouchableOpacity>
                </Fragment>
            )
        }


        return (

            <View style={{ flex: 1 }}>

                <View style={{ flex: 1, flexDirection: 'column', margin: 15, padding: 2, borderColor: EColor.blueInfo, borderWidth: 1 }}>
                    <View>
                        <FText
                            label={translate('Car number')}
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5,

                            }}

                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={trailernumber} ></FText>
                        <FText
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5
                            }}
                            label={translate('Trip number')}
                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={externalorderkey2} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            fontSize={15}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            label={translate('Product')}
                            title={descr} ></FText>
                        <FText
                            fontSize={15}
                            label={translate('Sku')}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            title={sku} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Total CS')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${originalqty}`} ></FText>
                        <FText
                            label={translate('Total case picked')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${shippedqty}`} ></FText>


                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Date')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${lottable04 ? Moment(lottable04).format("DD-MM-YYYY") : ''}`} ></FText>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Pallet even')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${evenPallet}`} ></FText>
                        <FText
                            label={translate('Pallet odd')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${oddPallet}`} ></FText>

                        <FText
                            label={translate('Batch/lot')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${lottable01}`} ></FText>
                        <FText

                            label={translate('Location')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${location}`} ></FText>
                    </View>
                    <View style={{ flex: 1, margin: 20, flexDirection: 'row' }}>
                        <View style={{ flex: 8, marginLeft: 10, marginRight: 20 }}>
                            {renderActions}
                        </View>

                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <FButtonMatrix
                                disabled={isHiden}
                                color={isHiden ? EColor.info : EColor.primary}
                                key={uuid.v4()} size={40} onPress={this.gotoToolTallyScreen.bind(this, item)} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }



    renderKey = (item, index) => {
        return `${index}`;
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };
    renderEmpty = () => (
        <View style={{ ...STYLES.center }}>
            <Text style={{ textAlign: 'center', fontSize: 24 }}>{this.props.translate('Empty data')}</Text>
        </View>
    )

    gotoPalletTypeScreen = () => {
        const { navigation } = this.props
        navigation.navigate('PalletTypeShipScreen')
    }

    render() {
        const { shipDetailStore, navigation, translate } = this.props
        const { isLoading, isRefreshing,isBack } = shipDetailStore
        return (
            <AppLayout isLoading={isLoading}>
                <FHeader 
                title={translate('Ship detail')} 
                navigation={navigation} 
                actionCondition = {this.checkBack}
                mesageCondition= {translate('Please completed the order entered')}
                typeLeftComponent='backCondition'
                />
                <AppLayout>
                    <View style={{ flex: 1 }}>
                        <FlatList

                            data={toJS(shipDetailStore.data)}
                            renderItem={this.renderItem}
                            refreshControl={
                                <RefreshControl
                                    colors={['blue']}
                                    refreshing={isRefreshing}
                                    onRefresh={this.onRefresh} />
                            }
                            initialNumToRender={16}
                            keyExtractor={this.renderKey}
                            ItemSeparatorComponent={this.renderSeparator}
                        >
                        </FlatList>
                    </View>
                </AppLayout>
                <View style={{ height: 40, width: '100%', marginBottom: 2 }}>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity
                                 disabled = {!isBack}
                                onPress={shipDetailStore.reject}
                                style={{ width: '100%', borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center' }}
                            >
                                <Text style={{ textAlign: 'center', color: isBack ?EColor.primary : EColor.black }}>{translate('Reject')}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity
                                disabled = {isBack}
                                onPress={this.gotoPalletTypeScreen}
                                style={{ width: '100%', backgroundColor: isBack ? EColor.disable : EColor.primary, borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center' }}
                            >
                                <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Complete')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </AppLayout>
        )
    }
}
export default injectCommonStore(ShipDetailScreen)


