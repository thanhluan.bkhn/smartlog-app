import React, { Component, Fragment, PureComponent } from 'react'
import { View, Text, FlatList, ActivityIndicator, RefreshControl, TouchableOpacity, LayoutChangeEvent } from 'react-native'
import FHeader from '~/components/headers/FHeader'
import AppLayout from '~/layouts/AppLayout'
import { Input, Icon, Button, ButtonGroup } from 'react-native-elements'
import { EColor, ESize } from '~/common/enums'
import { BindStore } from '~/stores/base/decorator'
import { RECEIPT_STATUS, STYLES, TYPE_HEADER } from '~/common/constants'
import { IScreenProps } from '~/stores/base/interfaces'
import { toJS } from 'mobx';
import { ICommonStore, injectCommonStore } from '~/common/hoc';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen'
import { PutawayListStore } from '~/stores/putaway/putaway-list.store'

interface IProps extends IBaseScreenProps {
    putawayListStore?: PutawayListStore;
}
const ITEM_SIZE = 60;
const scale = 8;
@BindStore('putawayListStore')
class PutawayListScreen extends BaseScreen<IProps> {
    async reload() {

    }
    state = {
        sizeItem: ESize.WIDTH_WINDOW / scale,
        fontSize: ESize.WIDTH_WINDOW / (scale * 5)
    }
    async componentDidMount() {

        const { putawayListStore } = this.props;

        putawayListStore.data = [];
        putawayListStore.keySearch = '';
        putawayListStore.load(1);
    }

    onRefresh = async () => {
        const { onRefresh } = this.props.putawayListStore
        await onRefresh();
    }
    loadMore = async () => {

        const { putawayListStore } = this.props

        const { isLoading, data, total } = putawayListStore
        if (isLoading || data.length === total) {
            return
        }
        await putawayListStore.loadMore();
    }

    search = async () => {
        const { search } = this.props.putawayListStore;
        await search();
    }

    renderItem = ({ item, index }) => {

        const { sizeItem, fontSize } = this.state;
        const { translate } = this.props
        const { externreceiptkey, trailernumber, total_qtyexpected, status, transaction_type, carrierkey, externalreceiptkey2, sku } = item
        const valuePipeStatus = this.pipeStatus(status);

        return (
            <View style={{
                height: sizeItem * 5 + 25,
                flexDirection: 'column', margin: 5, flex: 1, borderRadius: 6, shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,
                elevation: 1
            }}>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Command')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize, color: EColor.textColorTable }}>{externreceiptkey}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Status')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize, color: valuePipeStatus.color }}>{valuePipeStatus.title}</Text>
                        </View>
                    </View>
                </View>

                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Car number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.4, color: '#58D2F3' }}>{trailernumber}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Quantity')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.5, color: EColor.textColorTable, fontWeight: 'bold' }}>{total_qtyexpected}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Trip number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.4, color: '#58D2F3' }}>{externalreceiptkey2}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Sku')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5, fontWeight: 'bold', color: EColor.brandPrimary }}>{sku}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Transaction type')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize, color: EColor.textColorTable }}>{transaction_type}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize }}>{translate('Carrier')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize, color: EColor.textColorTable }}>{carrierkey}</Text>
                        </View>
                    </View>
                </View>

                <View style={{
                    height: sizeItem,
                    margin: 16,
                    justifyContent: 'center'
                }}>
                    {this.renderLifter(item)}

                </View>
            </View>
        )
    }
    onLayout = (event: LayoutChangeEvent) => {
        const { height, width } = event.nativeEvent.layout
        const sizeItem = width / scale;
        const fontSize = width / (scale * 5);
        this.setState({
            sizeItem, fontSize
        })
    }
    getItemLayout = (data, index) => {
        const { sizeItem, fontSize } = this.state;
        return {
            length: sizeItem * 3 + 15,
            offset: (sizeItem * 3 + 15) * index,
            index
        }
    }
    renderKey = (item, index) => {
        return `${item.id.toString()}${index}`;
    }
    renderFooter = (isLoading: boolean) => {
        if (isLoading) {
            return (
                <Fragment>
                    <View style={{ ...STYLES.center, height: 60 }}>
                        {isLoading && <ActivityIndicator size='small' color='blue' />}
                    </View>
                </Fragment>
            );
        }
        return null
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };
    pipeStatus = (status: string) => {
        const defaultValue = {
            title: '',
            color: EColor.black
        }
        const values = Object.values(RECEIPT_STATUS);
        const indexFind = values.findIndex(item => {
            return item.value == status
        });
        if (indexFind == -1) {
            return defaultValue;
        }
        const { translate } = this.props;
        return {
            title: translate(values[indexFind].text),
            color: values[indexFind].color
        }

    }
    renderLifter = (item) => {
        const { translate, userInfo } = this.props;
        const { fontSize } = this.state;
        const { lifter, unconfirm } = item;
        const check = Number(unconfirm) === 0;
        if (check) {
            return (
                <Button
                    disabled={true}
                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.info
                    }} title={lifter || ''} />
            )
        }


        if (lifter) {
            if (lifter != userInfo.username) {
                return (
                    <Button
                        disabled={true}

                        iconRight={true}
                        icon={() => {
                            return (
                                <Icon
                                    iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                    size={fontSize}
                                    color={EColor.white}
                                    name={'ios-arrow-forward'}
                                    type={'ionicon'}
                                />
                            )
                        }}
                        titleStyle={{
                            fontSize: fontSize
                        }}
                        buttonStyle={{
                            height: '80%',
                            backgroundColor: EColor.info
                        }} title={lifter} />
                )
            }
            return (
                <Button
                    // disabled = {disable}
                    onPress={this.gotoDetail.bind(this, item)}
                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.primary
                    }} title={lifter} />
            )
        }

        return (
            <Button
                // disabled = {disable}
                onPress={this.gotoDetail.bind(this, item)}
                iconRight={true}
                icon={() => {
                    return (
                        <Icon
                            iconStyle={{ marginLeft: 8, marginTop: 4 }}
                            size={fontSize}
                            color={EColor.white}
                            name={'ios-arrow-forward'}
                            type={'ionicon'}
                        />
                    )
                }}
                titleStyle={{
                    fontSize: fontSize
                }}
                buttonStyle={{
                    height: '80%',
                    backgroundColor: '#00F2FE'
                }} title={translate('Start')} />
        )
    }
    gotoDetail = async (item) => {

        const { userInfo, putawayListStore } = this.props
        const { receiptkey, trailernumber = '', id, externalreceiptkey2 } = item;
        const body = {
            lifter: userInfo.username,
            id: id,
            externalreceiptkey2: externalreceiptkey2
        }
        await putawayListStore.updateUserAction(body, receiptkey, trailernumber);

    }
    render() {
        const { putawayListStore, translate, navigation } = this.props
        const { isLoading, isRefreshing, isLoadingContainer } = putawayListStore
        return (

            <AppLayout>
                <FHeader title={translate('Putaway list')} navigation={navigation} typeLeftComponent='back' />
                <AppLayout isLoading={isLoadingContainer} >
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', borderColor: EColor.disable, borderWidth: 1 }}>
                            <View style={{ flex: 1, height: ITEM_SIZE }}>
                                <Input
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                    }}
                                    rightIcon={{ type: 'font-awesome', name: 'search', color: EColor.info }}
                                    keyboardType={'web-search'}
                                    returnKeyType='search'
                                    value={putawayListStore.keySearch}
                                    onChangeText={(text) => putawayListStore.keySearch = text}
                                    onSubmitEditing={this.search}
                                >

                                </Input>
                            </View>
                            <View style={{ width: ITEM_SIZE, height: ITEM_SIZE, justifyContent: 'center' }}>
                                <Icon type='material-community' name='barcode-scan'></Icon>
                            </View>

                        </View>
                        <AppLayout>
                            <View style={{ height: 50 }}>
                                <ButtonGroup
                                    onPress={putawayListStore.updateIndex}
                                    selectedIndex={putawayListStore.selectedIndex}
                                    selectedButtonStyle={{
                                        backgroundColor: EColor.purple
                                    }}
                                    buttons={TYPE_HEADER}
                                ></ButtonGroup>
                            </View>
                            <View style={{ flex: 1 }}>
                                <FlatList
                                    ListFooterComponent={this.renderFooter.bind(this, isLoading)}
                                    data={toJS(putawayListStore.data)}
                                    renderItem={this.renderItem}
                                    refreshControl={
                                        <RefreshControl
                                            colors={['blue']}
                                            refreshing={isRefreshing}
                                            onRefresh={this.onRefresh} />
                                    }
                                    getItemLayout={this.getItemLayout}
                                    initialNumToRender={16}
                                    keyExtractor={this.renderKey}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    onEndReachedThreshold={0.5}
                                    onEndReached={this.loadMore}
                                >

                                </FlatList>
                            </View>

                        </AppLayout>
                    </View>

                </AppLayout>
            </AppLayout>
        )
    }
}

export default injectCommonStore(PutawayListScreen)