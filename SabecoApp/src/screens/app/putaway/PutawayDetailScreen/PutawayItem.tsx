import Moment from 'moment-timezone';
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { EColor } from '~/common/enums';
import { ICommonStore } from '~/common/hoc';
import FText from '~/components/texts/FText';
import { bindCommon, BinPropertyStore } from '~/stores/base/decorator';
import { PutawayDetailStore } from '~/stores/putaway/putaway-detail.store';

interface IProps  extends ICommonStore{
    putawayDetailStore?: PutawayDetailStore,
    item: any,
    openModal?: (item: any) => void 
}
@BinPropertyStore( store => {
    const common = bindCommon(store);
    return {
        ...common,
        putawayDetailStore: store.putawayDetailStore
    }
})
export class PutawayItem extends Component<IProps> {

    openModal = () => {
        const { item,openModal} = this.props;
        openModal(item);
    }
    render() {
        const { putawayDetailStore, translate ,item} = this.props
        const { receiptkey, trailernumber,externalreceiptkey2 } = putawayDetailStore.receiptSelected
        const { sku, descr, lottable01, suggestlocation, pallet, qty,total,total_confirm,lottable04 } = item;
        let newPallet = 0;
        let newQty = 0;

        if (!isNaN(pallet)) {
            newPallet = Number(pallet);
        }
        if (!isNaN(pallet)) {
            newQty = Number(qty);
        }

        const oddPallet = pallet > 0 && newQty % pallet == 0  ? '' : newQty % pallet + '';
        const evenPallet = newPallet > 0 ? Math.floor(newQty / newPallet) + '' : qty + '';
        return (

            <View style={{ flex: 1 }}>

                <View style={{ flex: 1, flexDirection: 'column', margin: 15, padding: 2, borderColor: EColor.blueInfo, borderWidth: 1 }}>
                    <View>
                        <FText
                            label={translate('Car number')}
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5,

                            }}

                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={trailernumber} ></FText>
                        <FText
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5
                            }}
                            label={translate('Trip number')}
                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={externalreceiptkey2} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            fontSize={15}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            label={translate('Product')}
                            title={descr} ></FText>
                        <FText
                            fontSize={15}
                            label={translate('Sku')}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            title={sku} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Pallet even')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${evenPallet}`} ></FText>
                        <FText
                            label={translate('Pallet odd')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${oddPallet}`} ></FText>

                        <FText
                            label={translate('Batch/lot')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${lottable01}`} ></FText>
                        <FText

                            label={translate('Location')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${suggestlocation}`} ></FText>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Date')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${lottable04 ? Moment(lottable04).format("DD-MM-YYYY") : ''}`} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        

                        <FText
                            label={translate('Total')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${total}`} ></FText>
                        <FText

                            label={translate('Total confirm')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${total_confirm}`} ></FText>
                    </View>


                    <View style={{ flex: 1, margin: 20, flexDirection: 'row' }}>
                        <View style={{ flex: 8, marginLeft: 10, marginRight: 20 }}>
                            <TouchableOpacity
                                disabled = {total_confirm == total}
                                onPress={this.openModal}
                                style={{ flex: 1, backgroundColor: (total_confirm == total) ? EColor.info :EColor.primary, height: 40, justifyContent: 'center' }}
                            >
                                <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Putaway')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
