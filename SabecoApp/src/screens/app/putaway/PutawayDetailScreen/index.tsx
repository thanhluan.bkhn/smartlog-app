import { toJS } from 'mobx';
import React, { Component, Fragment } from 'react';
import { FlatList, Keyboard, RefreshControl, Text, TouchableOpacity, View } from 'react-native';
import { Input } from 'react-native-elements';
import { STYLES } from '~/common/constants';
import { EColor } from '~/common/enums';
import { ICommonStore, injectCommonStore } from '~/common/hoc';
import { CustomError } from '~/common/modules';
import FHeader from '~/components/headers/FHeader';
import Modal from '~/components/modals/Modal';
import FText from '~/components/texts/FText';
import AppLayout from '~/layouts/AppLayout';
import { toastService } from '~/services/toast/toast.service';
import { BindStore } from '~/stores/base/decorator';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { PutawayItem } from './PutawayItem';
import { PutawayDetailStore } from '~/stores/putaway/putaway-detail.store';
import { globalEvents } from '~/App';
import { EKeyEvent } from '~/common/enums/keyevent';

interface IProps extends IBaseScreenProps {
    putawayDetailStore?: PutawayDetailStore
}
@BindStore('putawayDetailStore')
class PutawayDetailScreen extends BaseScreen<IProps> {
    async reload(){
       
    }
    
    private refInputLocation;
    state = {
        isModalVisible: false,
        newLoc: '',
        qtyNewLoc: ''
    }

    async  componentDidMount() {
        const { putawayDetailStore } = this.props
        putawayDetailStore.data = [];
        await putawayDetailStore.load();
    }

    onRefresh = async () => {
        const { putawayDetailStore } = this.props
        putawayDetailStore.data = [];
        await putawayDetailStore.onRefresh();
    }

    openModal = (item) => {
        const { putawayDetailStore } = this.props;
        putawayDetailStore.itemSelected = item;
        const { suggestlocation, total_confirm,total } = item;
        this.setState({ isModalVisible: true, newLoc: suggestlocation || '', qtyNewLoc: total - total_confirm + ''});

    };
    hidenModal = () => {
        this.setState({ isModalVisible: false });
    }

    confirmPutAway = async () => {
        try {
            const { translate } = this.props;
            const { confirmPutaway } = this.props.putawayDetailStore;
            const { newLoc, qtyNewLoc } = this.state;
            if (!newLoc) {
                throw new CustomError(translate('Location is required'));
            }
            if (!qtyNewLoc || isNaN(Number(qtyNewLoc))) {
                throw new CustomError(translate('Qty is required'));
            }
            this.hidenModal();
            await confirmPutaway(newLoc, Number(qtyNewLoc));
            globalEvents.emit(EKeyEvent.RELOAD_RECEIPT_DETAIL,{})
        } catch (error) {
            this.hidenModal();
            const { message } = error as CustomError;
            toastService.error(message)
        }
    }

    renderItem = ({ item, index }) => {
        return (
            <PutawayItem item = {item} openModal = {this.openModal} />
        )
    }



    renderKey = (item, index) => {
        return `${index}`;
    }
    gotoPutawayList = () => {
        const {navigation} = this.props
        navigation.navigate('PutawayListScreen',{
            reload: true
        })
    }
   
    renderEmpty = () => (
        <View style={{ ...STYLES.center }}>
            <Text style={{ textAlign: 'center', fontSize: 24 }}>{this.props.translate('Empty data')}</Text>
        </View>
    )
    render() {
        const { putawayDetailStore, translate, navigation } = this.props
        const { isLoading, isRefreshing } = putawayDetailStore
        const { isModalVisible, newLoc, qtyNewLoc } = this.state
        return (
            <AppLayout isLoading={isLoading}>
                <FHeader title={translate('Confirm Putaway')} navigation={navigation} typeLeftComponent='back' />
                <AppLayout>
                    <View style={{ flex: 1 }}>
                        <FlatList

                            data={toJS(putawayDetailStore.data)}
                            renderItem={this.renderItem}
                            refreshControl={
                                <RefreshControl
                                    colors={['blue']}
                                    refreshing={isRefreshing}
                                    onRefresh={this.onRefresh} />
                            }
                            initialNumToRender={16}
                            keyExtractor={this.renderKey}
                           
                        >
                        </FlatList>
                    </View>
                    <View style={{ height: 40, width: '100%', marginBottom: 2 }}>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity
                                    onPress={putawayDetailStore.reject}
                                    style={{ width: '100%', borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center' }}
                                >
                                    <Text style={{ textAlign: 'center', color: EColor.primary }}>{translate('Reject')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity
                                   onPress={this.gotoPutawayList}
                                    style={{ width: '100%', backgroundColor: EColor.primary, borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center' }}
                                >
                                    <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Complete')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </AppLayout>
                <Modal
                    translate={translate}
                    title={'Confirm putawy'}
                    isVisible={isModalVisible}
                    onBackdropPress={this.hidenModal}
                    confirm={this.confirmPutAway}
                >
                    <Input
                        inputContainerStyle={{
                            borderWidth: 1,
                        }}
                        blurOnSubmit={false}
                        placeholderTextColor={EColor.blueInfo}
                        placeholder={`${translate('Qty confirm')}`}
                        inputStyle={{
                            marginLeft: 10,
                        }}
                        onChangeText={(text) => {
                            this.setState({
                                qtyNewLoc: text
                            })
                        }}
                        keyboardType='numeric'
                        returnKeyType='next'

                        onSubmitEditing={() => this.refInputLocation.focus()}
                        value={qtyNewLoc}
                        leftIcon={{ type: 'font-awesome', name: 'calculator' }}
                    />
                    <Input
                        ref={(ref) => this.refInputLocation = ref}
                        inputContainerStyle={{
                            borderWidth: 1,
                        }}
                        blurOnSubmit={false}
                        placeholderTextColor={EColor.blueInfo}
                        placeholder={`${translate('Location')}`}
                        inputStyle={{
                            marginLeft: 10,
                        }}
                        onChangeText={(text) => {
                            this.setState({
                                newLoc: text
                            })
                        }}

                        onSubmitEditing={() => Keyboard.dismiss()}
                        value={newLoc}
                        leftIcon={{ type: 'font-awesome', name: 'location-arrow' }}
                    />
                </Modal>
            </AppLayout>
        )
    }
}
export default injectCommonStore(PutawayDetailScreen)


