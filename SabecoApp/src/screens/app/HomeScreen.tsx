import React, { Component } from 'react';
import { GestureResponderEvent, Image, Text, TouchableOpacity, View } from 'react-native';
import { NavigationFocusInjectedProps } from 'react-navigation';
import { IMAGES, STYLES } from '~/common/constants';
import { ICommonStore, injectCommonStore } from '~/common/hoc';
import FHeader from '~/components/headers/FHeader';
import AppLayout from '~/layouts/AppLayout';

interface IProps extends ICommonStore,NavigationFocusInjectedProps<any> {

}
 class HomeScreen extends Component<IProps> {

   


    gotoScreen = (screenName: string) => {
        const {navigation} = this.props
        navigation.navigate(screenName)
    }
    render() {
        const {translate} = this.props
        return (
            <AppLayout isLoading={false}>
                <FHeader title={translate('Home')} typeLeftComponent='menu' />
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <ButtonImage title={translate('Receive')} source={IMAGES.inbound} onPress={this.gotoScreen.bind(this, 'ReceiptListScreen')} />
                       
                        <ButtonImage title={translate('Putaway')} source={IMAGES.putaway} onPress={this.gotoScreen.bind(this, 'PutawayListScreen')} />
                        
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                    

                    <ButtonImage title= {translate('Pick')} source={IMAGES.pick}  onPress={this.gotoScreen.bind(this, 'PickListScreen')} />

                    <ButtonImage title={translate('Ship')} source={IMAGES.ship} onPress={this.gotoScreen.bind(this, 'ShipListScreen')} />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                    

                    <ButtonImage title= {translate('Inventory')} source={IMAGES.stock_count}  onPress={this.gotoScreen.bind(this, 'StockCountScreen')} />
                    <View style = {{flex:1,margin:5}}></View>
                   
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                    
                    </View>
                </View>
            </AppLayout>
        )
    }
}


interface IButtonImageProps {
    source: any,
    title?: string,
    imageSize?: {
        height?: number,
        width?: number
    }
    transform?:number
    onPress?: (event: GestureResponderEvent) => void;
}
class ButtonImage extends Component<IButtonImageProps>{
    render() {
        const { source, title = '', imageSize = {
            height: 60,
            width: 60
        },transform = 0, onPress } = this.props
        const newRransform =  transform % 360;
        return (
            <React.Fragment>
                <TouchableOpacity style={{ ...STYLES.center, margin: 5, borderWidth: 1, borderColor: 'gray' }} onPress={onPress} >
                    <Image

                        resizeMode='contain'
                        source={source}
                        style={{
                            height: imageSize.height + '%',
                            width: imageSize.width + '%',
                            transform: [{ rotate: newRransform +  'deg' }]
                        }}
                    >
                    </Image>
                    <Text style={{ fontSize: 25 }}> {title} </Text>
                </TouchableOpacity>
            </React.Fragment>
        )
    }
}

export default injectCommonStore(HomeScreen)