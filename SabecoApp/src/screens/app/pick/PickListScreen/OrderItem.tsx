import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { ORDER_STATUS } from '~/common/constants';
import { EColor } from '~/common/enums';
import { ICommonStore } from '~/common/hoc';
import { BindStore } from '~/stores/base/decorator';
import { PickListStore } from '~/stores/pick/pick-list.store';


interface IProps extends ICommonStore {
    item: {
        [key: string]: any
    },
    sizeItem: number,
    fontSize: number,
    pickListStore?: PickListStore
}
@BindStore('pickListStore')
export default class OrderItem extends Component<IProps> {


    pipeStatus = (status: string) => {
        const defaultValue = {
            title: '',
            color: EColor.black
        }
        if (!status) return defaultValue;
        const values = Object.values(ORDER_STATUS);
        const indexFind = values.findIndex(item => {
            return item.value.trim() == status.trim()
        });
        if (indexFind == -1) {
            return defaultValue;
        }
        const { translate } = this.props;
        return {
            title: translate(values[indexFind].text),
            color: values[indexFind].color
        }
    }
    gotoDetail = async () => {

        const { pickListStore, userInfo, item } = this.props
        const { orderkey, trailernumber = '', id,externalorderkey2 } = item;
        const body = {
            lifter: userInfo.username,
            id: id,
            externalorderkey2: externalorderkey2
        }
        await pickListStore.updateUserAction(body, orderkey, trailernumber);

    }
    renderLifter = () => {

        const { translate, userInfo, item, fontSize } = this.props;

        const { status, lifter } = item;
        if (status == ORDER_STATUS.Picked.value || status == ORDER_STATUS.ShipCompleted.value) {
            return (
                <Button
                    disabled={true}

                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize * 1.1
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.info
                    }} title={lifter} />
            )
        }
        if (lifter) {
            if (lifter != userInfo.username) {
                return (
                    <Button
                        disabled={true}

                        iconRight={true}
                        icon={() => {
                            return (
                                <Icon
                                    iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                    size={fontSize}
                                    color={EColor.white}
                                    name={'ios-arrow-forward'}
                                    type={'ionicon'}
                                />
                            )
                        }}
                        titleStyle={{
                            fontSize: fontSize * 1.1
                        }}
                        buttonStyle={{
                            height: '80%',
                            backgroundColor: EColor.info
                        }} title={lifter} />
                )
            }
            return (
                <Button
                    onPress={this.gotoDetail}
                    iconRight={true}
                    icon={() => {
                        return (
                            <Icon
                                iconStyle={{ marginLeft: 8, marginTop: 4 }}
                                size={fontSize}
                                color={EColor.white}
                                name={'ios-arrow-forward'}
                                type={'ionicon'}
                            />
                        )
                    }}
                    titleStyle={{
                        fontSize: fontSize * 1.1
                    }}
                    buttonStyle={{
                        height: '80%',
                        backgroundColor: EColor.primary
                    }} title={lifter} />
            )
        }

        return (
            <Button

                onPress={this.gotoDetail}
                iconRight={true}
                icon={() => {
                    return (
                        <Icon
                            iconStyle={{ marginLeft: 8, marginTop: 4 }}
                            size={fontSize}
                            color={EColor.white}
                            name={'ios-arrow-forward'}
                            type={'ionicon'}
                        />
                    )
                }}
                titleStyle={{
                    fontSize: fontSize * 1.1
                }}
                buttonStyle={{
                    height: '80%',
                    backgroundColor: '#00F2FE'
                }} title={translate('Start')} />
        )
    }

    render() {
        const { item, sizeItem, fontSize, translate } = this.props
        
        const { lifter, externalorderkey2, trailernumber, qtyoriginal, status,sku,order_type,carriercode ,externorderkey} = this.props.item
        const valuePipeStatus = this.pipeStatus(status);
        return (
            <View style={{
                height: sizeItem * 5+ 25,
                flexDirection: 'column', margin: 5, flex: 1, borderRadius: 6, shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,

                elevation: 1
            }}>
                <View style={{

                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Command')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1, color: EColor.textColorTable }}>{externorderkey}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Status')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.1, color: valuePipeStatus.color }}>{valuePipeStatus.title}</Text>
                        </View>
                    </View>
                </View>

                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Car number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.1 * 1.4, color: '#58D2F3' }}>{trailernumber}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Quantity')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.5,color:EColor.textColorTable,fontWeight: 'bold' }}>{qtyoriginal}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height: sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Trip number')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text style={{ fontSize: fontSize * 1.1 , color: '#58D2F3' }}>{externalorderkey2}</Text>
                        </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1 }}>{translate('Carrier')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style={{ fontSize: fontSize * 1.1, color: EColor.textColorTable }}>{carriercode}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    flex: 1, borderBottomColor: '#E8E8E8', borderBottomWidth: 1, height:sizeItem,
                    marginLeft: 16,
                    flexDirection: 'row',
                    marginRight: 16
                }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize * 1.1}}>{translate('Transaction type')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize * 1.1,color:EColor.textColorTable}}>{order_type}</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 2, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize * 1.1}}>{translate('Sku')}</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', }}>
                            <Text style = {{fontSize: fontSize * 1.5,fontWeight: 'bold',color:EColor.brandPrimary}}>{sku}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    height: sizeItem,
                    margin: 16,
                    justifyContent: 'center'
                }}>
                    {this.renderLifter()}

                </View>
            </View>
        )
    }
}
