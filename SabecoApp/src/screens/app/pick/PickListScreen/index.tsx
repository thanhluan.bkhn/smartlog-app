import React, { Component, Fragment } from 'react';
import { ActivityIndicator, FlatList, LayoutChangeEvent, RefreshControl, View } from 'react-native';
import { Icon, Input, ButtonGroup } from 'react-native-elements';
import { STYLES, TYPE_HEADER } from '~/common/constants';
import { EColor, ESize } from '~/common/enums';
import { injectCommonStore } from '~/common/hoc';
import FHeader from '~/components/headers/FHeader';
import AppLayout from '~/layouts/AppLayout';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { BindStore } from '~/stores/base/decorator';
import { PickListStore } from '~/stores/pick/pick-list.store';

import OrderItem from './OrderItem';


interface IProps extends IBaseScreenProps {
    pickListStore?: PickListStore;

}

const ITEM_SIZE = 60;
const scale = 8;
@BindStore('pickListStore')
class PickListScreen extends BaseScreen<IProps> {
    async reload() {

    }

    state = {
        sizeItem: ESize.WIDTH_WINDOW / scale,
        fontSize: ESize.WIDTH_WINDOW / (scale * 5)
    }

    async componentDidMount() {
        const { pickListStore } = this.props;
        pickListStore.data = [];
        await pickListStore.load(1);
    }


    onRefresh = async () => {
        const { onRefresh } = this.props.pickListStore
        await onRefresh();
    }
    loadMore = async () => {

        const { pickListStore } = this.props

        const { isLoading, data, total } = pickListStore
        if (isLoading || data.length === total) {
            return
        }
        await pickListStore.loadMore();
    }

    search = async () => {
        const { search } = this.props.pickListStore

        await search();
    }
    renderKey = (item, index) => {
        return `${item.id.toString()}${index}`;
    }
    onLayout = (event: LayoutChangeEvent) => {
        const { height, width } = event.nativeEvent.layout
        const sizeItem = width / scale;
        const fontSize = width / (scale * 5);
        this.setState({
            sizeItem, fontSize
        })
    }
    renderItem = ({ item, index }) => {
        const { translate, userInfo } = this.props
        const { fontSize, sizeItem } = this.state;
        return (
            <OrderItem item={item} fontSize={fontSize} sizeItem={sizeItem} translate={translate} userInfo={userInfo} />
        )
    }

    renderFooter = (isLoading: boolean) => {
        if (isLoading) {
            return (
                <Fragment>
                    <View style={{ ...STYLES.center, height: 60 }}>
                        {isLoading && <ActivityIndicator size='small' color='blue' />}
                    </View>
                </Fragment>
            );
        }
        return null
    };
    getItemLayout = (data, index) => {
        const { sizeItem } = this.state;
        return {
            length: sizeItem * 3 + 15,
            offset: (sizeItem * 3 + 15) * index,
            index
        }
    }
    render() {
        const { navigation, pickListStore, translate } = this.props
        const { data, isRefreshing, isLoading, isLoadingContainer } = pickListStore
        return (
            <AppLayout onLayout={this.onLayout}>
                <FHeader navigation={navigation} typeLeftComponent='back' title={translate('Pick list')} />
                <AppLayout isLoading={isLoadingContainer}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', borderColor: EColor.disable, borderWidth: 1 }}>
                            <View style={{ flex: 1, height: ITEM_SIZE }}>
                                <Input
                                    inputContainerStyle={{
                                        borderBottomWidth: 0,
                                    }}
                                    rightIcon={{ type: 'font-awesome', name: 'search', color: EColor.info }}
                                    keyboardType={'web-search'}
                                    returnKeyType='search'
                                    value={pickListStore.keySearch}
                                    onChangeText={(text) => pickListStore.keySearch = text}
                                    onSubmitEditing={this.search}
                                >

                                </Input>
                            </View>
                            <View style={{ width: ITEM_SIZE, height: ITEM_SIZE, justifyContent: 'center' }}>
                                <Icon type='material-community' name='barcode-scan'></Icon>
                            </View>

                        </View>
                        <AppLayout>
                            <View style={{ height: 50 }}>
                                <ButtonGroup
                                    onPress={pickListStore.updateIndex}
                                    selectedIndex={pickListStore.selectedIndex}
                                    selectedButtonStyle={{
                                        backgroundColor: EColor.purple
                                    }}
                                    buttons={TYPE_HEADER}
                                ></ButtonGroup>
                            </View>
                            <View style={{ flex: 1 }}>
                                <FlatList
                                    data={data}
                                    renderItem={this.renderItem}

                                    keyExtractor={this.renderKey}
                                    ListFooterComponent={this.renderFooter.bind(this, isLoading)}
                                    refreshControl={
                                        <RefreshControl
                                            colors={['blue']}
                                            refreshing={isRefreshing}
                                            onRefresh={this.onRefresh} />
                                    }
                                    getItemLayout={this.getItemLayout}
                                    initialNumToRender={16}
                                    onEndReachedThreshold={0.5}
                                    onEndReached={this.loadMore}
                                >

                                </FlatList>
                            </View>
                        </AppLayout>
                    </View>

                </AppLayout>
            </AppLayout>
        )
    }
}

export default injectCommonStore(PickListScreen)
