import React, { Component, Fragment } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { ICommonStore } from '~/common/hoc'
import { PickDetailStore } from '~/stores/pick/pick-detail.store'
import {  BinPropertyStore, bindCommon } from '~/stores/base/decorator'
import { PICKDETAIL_STATUS } from '~/common/constants'
import FText from '~/components/texts/FText'
import { EColor } from '~/common/enums'
import Moment from 'moment-timezone';   


interface IProps extends ICommonStore {
    item: {
        [key: string]: any
    },
    pickDetailStore?: PickDetailStore
}
@BinPropertyStore( store => {
    const common = bindCommon(store);
    return {
        ...common,
        pickDetailStore: store.pickDetailStore
    }
})
export default class PickDetailItem extends Component<IProps> {


    pick = async () => {
        const {item} = this.props
        const { pick,load } = this.props.pickDetailStore;
        await pick(item);
        await load();
    }

    render() {
        const { pickDetailStore, translate,item } = this.props
        const { orderkey, trailernumber ,externalorderkey2} = pickDetailStore.orderSelected
        let { pallet = 0,
            descr, sku, lottable01 = '', location = '',
            status,
            originalqty = 0,
            qtypicked,
            lottable04,
            
        } = item;
        if (pallet != 0) {
            pallet = Number(pallet);
        }
        if (originalqty != 0) {
            originalqty = Number(originalqty);
        }
        const oddPallet = pallet > 0 && originalqty % pallet == 0  ? '' : originalqty % pallet + '';
        const evenPallet = pallet > 0 ? Math.floor(originalqty / pallet) + '' : originalqty + '';
        let renderActions = null;

        if (PICKDETAIL_STATUS.PickCompleted.value == status) {
            renderActions = (
                <View style={{ height: 40, justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', color: PICKDETAIL_STATUS.PickCompleted.color }}> {translate(PICKDETAIL_STATUS.PickCompleted.text)}</Text>
                </View>
            )
        } else {
            renderActions = (
                <Fragment>
                    <TouchableOpacity
                        onPress={this.pick}
                        style={{ flex: 1, backgroundColor: EColor.primary, height: 40, justifyContent: 'center' }}
                    >
                        <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Pick')}</Text>
                    </TouchableOpacity>
                </Fragment>
            )
        }


        return (

            <View style={{ flex: 1 }}>

                <View style={{ flex: 1, flexDirection: 'column', margin: 15, padding: 2, borderColor: EColor.blueInfo, borderWidth: 1 }}>
                    <View>
                        <FText
                            label={translate('Car number')}
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5,

                            }}

                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={trailernumber} ></FText>
                        <FText
                            containerStyle={{
                                backgroundColor: EColor.yellow,
                                height: 60,
                                borderColor: EColor.white,
                                borderWidth: 0.5
                            }}
                            label={translate('Trip number')}
                            colorLabel={EColor.white}
                            colorTitle={EColor.white}
                            title={externalorderkey2} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Total CS')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${originalqty}`} ></FText>
                        <FText
                            label={translate('Total case picked')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${qtypicked}`} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            fontSize={15}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            label={translate('Product')}
                            title={descr} ></FText>
                        <FText
                            fontSize={15}
                            label={translate('Sku')}
                            containerStyle={{
                                height: 50,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5
                            }}
                            title={sku} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FText
                            label={translate('Date')}
                            fontSize={14}
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${lottable04 ? Moment(lottable04).format("DD-MM-YYYY") : ''}`} ></FText>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        <FText
                            label={translate('Pallet even')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,
                            }}
                            title={`${evenPallet}`} ></FText>
                        <FText
                            label={translate('Pallet odd')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${oddPallet}`} ></FText>

                        <FText
                            label={translate('Batch/lot')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${lottable01}`} ></FText>
                        <FText

                            label={translate('Location')}
                            fontSize={14}
                            type='row'
                            containerStyle={{
                                backgroundColor: '#F2F2F2',
                                height: 60,
                                borderColor: EColor.blueInfo,
                                borderWidth: 0.5,

                            }}
                            title={`${location}`} ></FText>
                    </View>
                    <View style={{ flex: 1, margin: 20, flexDirection: 'row' }}>
                        <View style={{ flex: 8, marginLeft: 10, marginRight: 20 }}>
                            {renderActions}
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
