import React, { Component, Fragment } from 'react';
import { toJS } from 'mobx';
import { FlatList, RefreshControl, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { STYLES } from '~/common/constants';
import { EColor } from '~/common/enums';
import { injectCommonStore } from '~/common/hoc';
import FHeader from '~/components/headers/FHeader';
import AppLayout from '~/layouts/AppLayout';
import BaseScreen, { IBaseScreenProps } from '~/screens/BaseScreen';
import { BindStore } from '~/stores/base/decorator';
import { PickDetailStore } from '~/stores/pick/pick-detail.store';

import PickDetailItem from './PickDetailItem';

interface IProps extends IBaseScreenProps {
    pickDetailStore?: PickDetailStore
}
@BindStore('pickDetailStore')
class PickDetailScreen extends BaseScreen<IProps> {
    async reload(){
        
    }

    async  componentDidMount() {
        const { pickDetailStore } = this.props
        pickDetailStore.data = [];
        await pickDetailStore.load();
    }

    onRefresh = async () => {
        const { pickDetailStore } = this.props

        pickDetailStore.data = [];
        await pickDetailStore.onRefresh();
    }

    


    renderItem = ({ item, index }) => {
        return (
            <PickDetailItem item = {item} />
        )
    }



    renderKey = (item, index) => {
        return `${index}`;
    }

   
    renderEmpty = () => (
        <View style={{ ...STYLES.center }}>
            <Text style={{ textAlign: 'center', fontSize: 24 }}>{this.props.translate('Empty data')}</Text>
        </View>
    )
    gotoPickList = () => {
        const {navigation} = this.props
        navigation.navigate('PickListScreen')
    }
    render() {
        const { pickDetailStore ,navigation,translate} = this.props
        const { isLoading, isRefreshing } = pickDetailStore
        return (
            <AppLayout isLoading={isLoading}>
                <FHeader title={translate('Order detail')} navigation = {navigation} typeLeftComponent='back' />
                <AppLayout>
                    <View style={{ flex: 1 }}>
                        <FlatList

                            data={toJS(pickDetailStore.data)}
                            renderItem={this.renderItem}
                            refreshControl={
                                <RefreshControl
                                    colors={['blue']}
                                    refreshing={isRefreshing}
                                    onRefresh={this.onRefresh} />
                            }
                            initialNumToRender={16}
                            keyExtractor={this.renderKey}
                           
                        >
                        </FlatList>
                    </View>
                    <View style={{ height: 40, width: '100%', marginBottom: 2 }}>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity
                                    onPress={pickDetailStore.reject}
                                    style={{ width: '100%', borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center' }}
                                >
                                    <Text style={{ textAlign: 'center', color: EColor.primary }}>{translate('Reject')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity
                                   onPress={this.gotoPickList}
                                    style={{ width: '100%', backgroundColor: EColor.primary, borderColor: EColor.primary, borderWidth: 0.5, height: 40, justifyContent: 'center' }}
                                >
                                    <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Complete')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </AppLayout>
            </AppLayout>
        )
    }
}
export default injectCommonStore(PickDetailScreen)


