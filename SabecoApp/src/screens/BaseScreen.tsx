import React, { Component } from 'react'
import { ICommonStore } from '~/common/hoc';
import { NavigationFocusInjectedProps } from 'react-navigation';

export interface IBaseScreenProps extends ICommonStore, NavigationFocusInjectedProps<any> {

}
export default abstract class BaseScreen<Props extends IBaseScreenProps = any> extends Component<Props> {

    async UNSAFE_componentWillReceiveProps(nextProps: Props) {
        
        if (this.props.isFocused && !nextProps.isFocused) {
            await this.reload();
        }
    }
    abstract reload(): Promise<void>
   
}
