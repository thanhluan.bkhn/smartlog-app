import React, { Component, Fragment } from 'react'
import { Text, View, Button, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import RNModal from "react-native-modal";
import { EColor } from '~/common/enums';
interface IProps {
    translate: (key: string) => string,
    title?: string,
    confirm?: () => void,
    isVisible: boolean,
    onBackdropPress: () => void
}
export default class Modal extends Component<IProps> {
    render() {
        const { translate, title = '', isVisible = false, onBackdropPress, confirm } = this.props
        return (
            <Fragment>
                <RNModal isVisible={isVisible} onBackdropPress={onBackdropPress}>
                    <View style={{ backgroundColor: EColor.white }}>
                        <View style={{ height: 40, justifyContent: 'center', borderBottomColor: EColor.blueInfo, borderBottomWidth: 1 }}>
                            <Text style={{ textAlign: 'center', color: EColor.blueInfo }}>{translate(title)}</Text>
                        </View>
                        <View style={{ height: 20 }}></View>
                        {this.props.children}
                        <View style={{ height: 20 }}></View>
                        <TouchableOpacity
                            onPress={confirm}
                            style={{ backgroundColor: EColor.primary, height: 40, justifyContent: 'center', marginLeft: 5, marginRight: 5 }}>
                            <Text style={{ textAlign: 'center', color: EColor.white }}>{translate('Confirm')}</Text>
                        </TouchableOpacity>
                        <View style={{ height: 20 }}></View>
                    </View>
                </RNModal>



            </Fragment>


        );
    }
}
