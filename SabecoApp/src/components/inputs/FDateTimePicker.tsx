import React, { Component } from 'react'
import { Text, View, StyleProp, ViewStyle, Keyboard } from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Input } from 'react-native-elements';

import Moment from 'moment-timezone'
import { EColor } from '~/common/enums';
import Icon from 'react-native-vector-icons/Fontisto';

interface IProps {
    value: Date;
    colorLabel?: any;
    label?: string;
    fontSize?: number;
    containerStyle?: StyleProp<ViewStyle>;
    format?: 'YYYY-MM-DD' | 'DD-MM-YYYY',
    onConfirm?: (date: string) => void;
    disable?: boolean
}
export default class FDateTimePicker extends Component<IProps> {

    state = {
        isShow: false,
        valueDate: this.props.value
    }
    show = () => {
        Keyboard.dismiss();
        this.setState({ isShow: true })
    }
    rightIcon = () => {
        return (
            <Icon name='date' size={36} color={EColor.primary} onPress={this.show} />
        )
    }
    pipeValue = (valueDate) => {
        const { format = 'DD-MM-YYYY' } = this.props
      
        if (!valueDate) {
            return ''
        }
        try {
            
            const output = Moment(valueDate).format(format);
            if (!output) {
                return ''
            }
            if (output == 'Invalid date') {
                return ''
            }

            return output;
        } catch (error) {
           
            return '';
        }

    }
   
   
    onConfirm = (date) => {
        const {onConfirm} = this.props
        const tranDate = this.pipeValue(date);
        if(onConfirm && tranDate) {
            onConfirm(tranDate);
        }
        this.setState({
            valueDate: date,
            isShow: false
        })
       
    }
    onCancel = () => {
        this.setState({
            isShow: false
        })
    }
    render() {
        const { isShow, valueDate } = this.state;
        const { value, label = '', containerStyle,disable = false } = this.props
        const style = Object.assign({}, containerStyle, {
            flex: 1, flexDirection: 'column'
        })
        const parseValue = this.pipeValue(valueDate)
        return (
            <View style={style} pointerEvents={disable ? 'none' : 'auto'}>

                <Text style={{ textAlign: 'center', height: 25 }} numberOfLines={1}>
                    {label}
                </Text>
                <Input
                    inputStyle={{
                        borderWidth: 0.5,
                        paddingLeft: 5
                    }}
                    inputContainerStyle={{
                        height: 50,
                        borderBottomWidth: 0
                    }}
                    underlineColorAndroid={EColor.transparent}
                    rightIcon={this.rightIcon}
                    onFocus={this.show}
                    value={parseValue} />
                <DateTimePicker
                    isVisible={isShow}
                    date={valueDate}
                    onConfirm={this.onConfirm}
                    onCancel={this.onCancel}
                    mode={'date'}
                    is24Hour={true}
                />
            </View>

        )
    }
}
