import React, { Component } from 'react';
import { StyleProp, Text, View, ViewStyle } from 'react-native';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Fontisto';
import { STYLES } from '~/common/constants';
import { EColor } from '~/common/enums';
import { Input } from 'react-native-elements';

interface IProps {
    colorLabel?: any;
    label?: string;
    fontSize?: number;
    value: string;
    containerStyle?: StyleProp<ViewStyle>;
    format?: 'YYYY-MM-DD' | 'DD-MM-YYYY',
    minDate?: string;
    maxDate?: string;
    confirmBtnText?: string;
    cancelBtnText?: string;
    onDateChange?: (date: string) => void;
    disable?: boolean
}

export default class FInputDate extends Component<IProps> {

    state = {
        isShowEmpty: this.props.value ? false : true
    }

    rightIcon = () => {
        return (
            <Icon name='date' size={30} color={EColor.primary} onPress = {this.show} />
        )
    }
    renderPicker = () => {
        const { colorLabel, label = '', containerStyle,
            fontSize = 20,
            onDateChange,
            value,
            minDate, maxDate,
            confirmBtnText = 'Confirm',
            cancelBtnText = "Cancel",
            format = 'DD-MM-YYYY', disable = false } = this.props
        if (value || !this.state.isShowEmpty) {
            return (
                <View style={{ flex: 1, height: 50 }} pointerEvents={disable ? 'none' : 'auto'}>
                    <DatePicker
                        date={value}
                        mode="date"
                        format={format}
                        minDate={minDate}
                        maxDate={maxDate}
                        confirmBtnText={confirmBtnText}
                        cancelBtnText={cancelBtnText}
                        onDateChange={onDateChange}
                    />
                </View>
            )
        }
        return (
            <Input
                onFocus = {this.show}
                rightIcon={this.rightIcon}
                value={''} />
        )
    }
    show = () => {
        this.setState({isShowEmpty: false})
    }
    render() {
        const { colorLabel, label = '', containerStyle,
        fontSize = 20,
        onDateChange,
        value,
        minDate, maxDate,
        confirmBtnText = 'Confirm',
        cancelBtnText = "Cancel",
        format = 'DD-MM-YYYY', disable = false } = this.props
        const style: any = Object.assign({}, containerStyle, STYLES.center)
        return (
            <View style={{ ...style, flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 1, height: 25 }}>
                    <Text style={{ textAlign: 'center', color: colorLabel }} numberOfLines={1} >
                        {label}
                    </Text>
                </View>
                <View style={{ flex: 1, height: 50 }} pointerEvents={disable ? 'none' : 'auto'}>
                    <DatePicker
                        date={value}
                        mode="date"
                        format={format}
                        minDate={minDate}
                        maxDate={maxDate}
                        confirmBtnText={confirmBtnText}
                        cancelBtnText={cancelBtnText}
                        onDateChange={onDateChange}
                    />
                </View>
            </View>
        )

    }
}

