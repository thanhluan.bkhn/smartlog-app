import React, { Component, Fragment } from 'react'
import { Text, View, Picker, StyleProp, ViewStyle } from 'react-native'
import { STYLES } from '~/common/constants';

interface IItemProps {
    label?: string,
    value: string
}

interface IPickerProps {
    items: IItemProps[];
    title: string,
    defaultLabel?: string,
    containerStyle?: StyleProp<ViewStyle>,
    onValueChange?: (value) => void
}

export default class FPicker extends Component<IPickerProps> {
    state = {
        selectedValue: this.props.defaultLabel || '',
        items: this.props.items
    }
    onValueChange = (value: string) => {
        const { items = [], onValueChange, title = '', defaultLabel = '' } = this.props;
        this.setState({
            selectedValue: value
        })
        onValueChange(value);
    }
    render() {
        const { items = [], title = '', defaultLabel = '', containerStyle } = this.props;
        const { selectedValue } = this.state
        const style = Object.assign({}, containerStyle, {
            flex: 1
        })
        return (
            <View style={style}>
                <View style={{ height: 10 }}>
                    <Text > {title}</Text>
                </View>
                <Picker
                    selectedValue={selectedValue}
                    onValueChange={this.onValueChange}>
                    {items.map((item, index) => {
                        const { value, label = value } = item;
                        return (
                            <Picker.Item key={`${index}`} value={value} label={label} />
                        )
                    })}
                </Picker>
            </View>
        )
    }
}
