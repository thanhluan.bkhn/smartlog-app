import React, { Component } from 'react'
import { Text, View, StyleProp, ViewStyle } from 'react-native'
import { ESize, EColor } from '~/common/enums'
import { STYLES } from '~/common/constants'

export default class FText extends Component<{
    type?: 'row' | 'column',
    title?: string,
    colorTitle?: any,
    colorLabel?: any,
    label?: string,
    fontSize?: number,
    containerStyle?: StyleProp<ViewStyle>
}> {
    render() {

        const { type = 'column', title = '', colorLabel, colorTitle, label = '', containerStyle, fontSize = 20 } = this.props
        const style = Object.assign({}, containerStyle, STYLES.center)
        if (type === 'column') {
            return (
                <View style={style}>
                    <Text style={{ textAlign: 'center' }} numberOfLines={1}>
                        <Text style={{ textAlign: 'center', color: colorLabel, fontSize }}>{`${label} : `}</Text>
                        <Text style={{ textAlign: 'center', color: colorTitle, fontSize }}>{title}</Text>
                    </Text>
                </View>
            )
        }
        return (
            <View style={style}>
                <Text style={{ textAlign: 'center' }} numberOfLines={1}>
                    {label}
                </Text>
                <Text style={{ textAlign: 'center' }} numberOfLines={1}>
                    {title}
                </Text>
            </View>
        )

    }
}
