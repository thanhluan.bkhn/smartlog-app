import React, { Component } from 'react'
import { Text, View, StyleProp, ViewStyle } from 'react-native'
import { STYLES } from '~/common/constants'
import { Input } from 'react-native-elements'
import { EColor } from '~/common/enums'
interface IProps {
    type?: 'row' | 'column',
    colorLabel?: any,
    label?: string,
    fontSize?: number,
    containerStyle?: StyleProp<ViewStyle>
    value: string,
    placeholder?: string,
    // onChangeText?: (text: string) => void,
    returnKeyLabel?: string,
    onSubmitEditing?: (text: string) => void,
    disable?: boolean
}
export default class FTextIpunt extends Component<IProps> {

    state = {
        valueText: this.props.value || ''
    }
    onChangeText = (text = '') => {
        this.setState({
            valueText: text
        })
    }
    onSubmitEditing = () => {
        const { onSubmitEditing } = this.props
        const { valueText } = this.state
        onSubmitEditing(valueText)
    }
    render() {
        const { valueText } = this.state
        const { type = 'column', disable = false, returnKeyLabel = 'UPDATE', placeholder = '', colorLabel, label = '', containerStyle, fontSize = 20, value = '' } = this.props
        const style = Object.assign({}, containerStyle, STYLES.center)
        if (type === 'column') {
            return (
                <View style={style} pointerEvents={disable ? 'none' : 'auto'}>
                    <Text style={{ textAlign: 'center' }} numberOfLines={1}>
                        <Text style={{ textAlign: 'center', color: colorLabel, fontSize }}>{`${label} : `}</Text>
                        <Input
                            onSubmitEditing={this.onSubmitEditing}
                            returnKeyLabel={returnKeyLabel}
                            value={valueText}
                            onChangeText={this.onChangeText}
                            inputContainerStyle={{
                                borderWidth: 1
                            }}
                            placeholderTextColor={EColor.disable}
                            placeholder={placeholder}
                            inputStyle={{
                                height: 30,
                                marginLeft: 10,
                            }}
                        />
                    </Text>
                </View>
            )
        }
        return (
            <View style={style} pointerEvents={disable ? 'none' : 'auto'}>
                <Text style={{ textAlign: 'center',height:25 }} numberOfLines={1}>
                    {label}
                </Text>
                <Input
                    onSubmitEditing={this.onSubmitEditing}
                    returnKeyLabel={returnKeyLabel}
                    value={valueText}
                    onChangeText={this.onChangeText}
                    inputContainerStyle={{
                        borderWidth: 1,
                        marginBottom:5
                    }}
                    placeholderTextColor={EColor.disable}
                    placeholder={placeholder}
                    inputStyle={{
                        height: 50,
                        marginLeft: 10,
                    }}
                />
            </View>
        )

    }
}
