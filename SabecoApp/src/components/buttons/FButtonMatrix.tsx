import React, { Component, Fragment } from 'react'
import { Text, View } from 'react-native'
import { EColor } from '~/common/enums'
import { v4 } from 'uuid'
import { TouchableOpacity } from 'react-native-gesture-handler';
interface IProps {
    numberOfSize?: number,
    size: number
    margin?: number,
    color?: string,
    onPress?: () => void,
    disabled?: boolean

}

export default class FButtonMatrix extends Component<IProps> {

    render() {
        const { numberOfSize = 3, size = 50, margin = 1, color = EColor.primary, onPress, disabled = false } = this.props;
        const matrix = [];
        for (let column = 0; column < numberOfSize; column++) {
            const rows = [];
            for (let row = 0; row < numberOfSize; row++) {
                const element = (
                    <View key={v4()} style={{ backgroundColor: color, flex: 1, margin: margin }}>
                    </View>
                );
                rows.push(element)
            }
            const elementColumn = (
                <View key={v4()} style={{ flex: 1, flexDirection: 'row' }}>
                    {rows}
                </View>
            )
            matrix.push(elementColumn)
        }
        return (
            <Fragment>
                <TouchableOpacity disabled={disabled} onPress={onPress} style={{ height: size, width: size, padding: margin }}>
                    {matrix}
                </TouchableOpacity>
            </Fragment>
        )
    }
}
