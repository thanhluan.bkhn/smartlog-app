import React, { Component } from 'react'
import FLeftHeader from './FLeftHeader'
import { NavigationScreenProp } from 'react-navigation'
import { EColor } from '~/common/enums'
import { View, Text } from 'react-native'
import { Header } from 'react-navigation-stack'
import navigationService from '~/services/navigation/navigation.service'
import { observer } from 'mobx-react'
import BuildConfig from '~/common/config/BuildConfig'
interface IHeaderProps {
    title?: string,
    typeLeftComponent: 'menu' | 'back' |'backCondition',
    navigation?: NavigationScreenProp<any>,
    actionCondition?: () => boolean,
    mesageCondition?: string
}
@observer
export default class FHeader extends Component<IHeaderProps> {
    render() {
        const { typeLeftComponent, title,navigation ,actionCondition,mesageCondition} = this.props
        const height = Header.HEIGHT + 20;
        return (
            <View style={{ height, backgroundColor: EColor.primary, flexDirection: 'row' }}>
                <View style={{ width: height, height, justifyContent: 'center', alignItems: 'center' }}>
                    <FLeftHeader 
                        navigation = {navigation || navigationService} 
                        type={typeLeftComponent} 
                        actionCondition = {actionCondition}
                        mesageCondition = {mesageCondition}
                    />
                </View>
                <View style={{ flex: 1, height, justifyContent: 'center' }}>
                    <Text style = {{color: EColor.white,textAlign: 'center',fontSize: 24}}>{title}</Text>
                </View>
                <View style={{ width: height, height, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style = {{color: EColor.white}}>{BuildConfig.versionSortName}</Text>
                </View>
            </View>
        )
    }
}
