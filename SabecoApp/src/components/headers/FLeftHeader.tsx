import React, { Component } from 'react'
import { Icon } from 'react-native-elements'
import navigationService, { NavigationService } from '~/services/navigation/navigation.service';
import { NavigationScreenProp } from 'react-navigation';
import { toastService } from '~/services/toast/toast.service';
import { TouchableOpacity } from 'react-native-gesture-handler';
interface IHeaderProps {
    type: 'back' | 'menu' | 'backCondition',
    navigation?: NavigationScreenProp<any> | NavigationService,
    actionCondition?: () => boolean,
    mesageCondition?: string
}
export default class FLeftHeader extends Component<IHeaderProps> {

    toggleDrawer = () => {

        const { navigation } = this.props
        navigation.toggleDrawer();
    }
    goBack = async () => {
        const { navigation } = this.props
        navigation.goBack();
    }
    actionCondition = async () => {
        const { navigation, actionCondition, mesageCondition = '' } = this.props
        const check = actionCondition();
        if (check) {
            navigation.goBack();
        } else {
            toastService.show(mesageCondition)
        }
    }

    render() {
        const { type } = this.props;
        if (type === 'menu') {
            return (
                <TouchableOpacity onPress={this.toggleDrawer}>
                    <Icon
                        color='#fff'
                        name='menu'
                        size={40}
                        style={{
                            marginLeft: 25
                        }}
                    >
                    </Icon>
                </TouchableOpacity>
            )
        }
        if (type == 'backCondition') {
            return (

                <TouchableOpacity onPress={this.actionCondition}>
                    <Icon
                        color='#fff'
                        name='md-arrow-back'
                        type='ionicon'
                        size={40}
                        style={{
                            marginLeft: 25
                        }}
                    >
                    </Icon>
                </TouchableOpacity>

            )
        }
        return (

            <TouchableOpacity onPress={this.goBack}>
                <Icon
                    color='#fff'
                    name='md-arrow-back'
                    type='ionicon'
                    size={40}
                    style={{
                        marginLeft: 25
                    }}
                >
                </Icon>
            </TouchableOpacity>

        )

    }
}