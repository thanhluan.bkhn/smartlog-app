import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import React, { Component } from 'react'

export default class DismissKeyboard extends Component {
  render() {
    return (
      <KeyboardAwareScrollView enableOnAndroid contentContainerStyle={{ flex: 1 }}  >
        {this.props.children}
      </KeyboardAwareScrollView>

    )
  }
}
