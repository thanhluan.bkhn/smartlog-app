import React,{ Component, Fragment } from "react";
import { InteractionManager, View, ActivityIndicator, StyleSheet } from "react-native";
import { SafeAreaView } from "react-navigation";

export default class FOptimized extends Component {
    state = { interactionsComplete: true };

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({ interactionsComplete: false });
        });
    }
    render() {
        const { interactionsComplete } = this.state
        if (interactionsComplete) {
            return (
                <SafeAreaView style = {{flex:1}}>
                    <View
                        style={styles.view}
                    >
                        <ActivityIndicator animating={true} size={'large'} color={'blue'} />
                    </View>
                </SafeAreaView>
            )
        }
        return (
            <SafeAreaView style = {{flex:1}}>
                {this.props.children}
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
        position: 'absolute',
        zIndex: 999,
        width: '100%',
        height: '100%'
    }
})