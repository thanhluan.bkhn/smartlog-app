import React, { Component, Fragment } from 'react'
import { Observer } from 'mobx-react'

export default class FView extends Component {
    render() { 
        return (
            <Observer >
                {() => (
                    <Fragment>
                        {this.props.children}
                    </Fragment>
                )}
            </Observer>
        )
    }
}
