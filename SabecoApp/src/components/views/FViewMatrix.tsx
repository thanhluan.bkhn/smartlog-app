import React, { Component, Fragment } from 'react'
import { Text, View, Dimensions } from 'react-native'
import { EColor, ESize } from '~/common/enums';
import { TouchableOpacity } from 'react-native-gesture-handler';
import uuid from 'uuid';
import FText from '../texts/FText';


interface IProps {
    divisionNumber: number,
    total: number,
    numberOfCheck: number
    margin?: number,
    colorCheck?: string,
    colorUncheck?: string
    onPressItem?: () => void,
    translate: (key: string) => string
}

export default class FViewMatrix extends Component<IProps> {

    state = {
        totalCheck: this.props.numberOfCheck,
        listBtn: [],
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    }

    onLayout = ({ nativeEvent: { layout } }) => {
        const { width, height } = layout;
        this.setState({
             width, height
        });
    };
    componentDidMount() {
        const { 
            total,
            numberOfCheck,
            
        } = this.props;
        const listBtn = [];
        for (let i = 0; i < total; i++) {
            if (i < numberOfCheck) {
                listBtn.push(true)
            } else {
                listBtn.push(false);
            }
        }
        this.setState({
            listBtn: listBtn
        })
    }

    onPressItem = async (index: number) => {
        let { listBtn ,totalCheck} = this.state
        
        
        listBtn[index] = !listBtn[index]
        if(listBtn[index]) {
            totalCheck ++;
        } else{
            totalCheck --
        }
       
        await this.setState({
            listBtn: listBtn,
            totalCheck: totalCheck
        })
    }

    renderMatrix = (sizeBtn: number) => {
        const matrixBtn = [];
        const {
            divisionNumber,
            total,
            margin = 5,
            colorCheck = EColor.primary,
            colorUncheck = EColor.success,
            onPressItem
        } = this.props;
        const { listBtn } = this.state

        for (let index = 0; index < Math.floor(total / divisionNumber) + 1; index++) {

            const rows = [];
            const size = (total - index * divisionNumber) >= divisionNumber ? divisionNumber : total - index * divisionNumber
            for (let i = 0; i < size; i++) {
                const indexBtn = index * divisionNumber + i;
                const element = (
                    <TouchableOpacity key={indexBtn}
                        onPress={this.onPressItem.bind(this, indexBtn)}
                        style={{
                            backgroundColor: listBtn[indexBtn] ? colorCheck : colorUncheck,
                            height: sizeBtn, width: sizeBtn, margin
                        }}>

                    </TouchableOpacity>
                )
                rows.push(element)
            }
            const columnElement = (
                <Fragment key={uuid.v4()}>
                    <View style={{ flexDirection: 'row' }}>
                        {rows}
                    </View>
                </Fragment>
            )
            matrixBtn.push(columnElement);

        }
        return matrixBtn;
    }

    render() {
        const {
            divisionNumber,
            margin = 5,
            translate,
        } = this.props;
        const { width,totalCheck} = this.state
        
        const sizeBtn = Math.floor((width - (divisionNumber) * 2 * margin) / divisionNumber)
        const matrixBtn = this.renderMatrix(sizeBtn);
         
        return (
            <View onLayout = {this.onLayout} style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ height: 55, flexDirection: 'row' }}>
                        <FText
                            fontSize={12}
                            containerStyle={{

                                height: 50,
                                borderColor: EColor.black,
                                borderWidth: 0.5
                            }}
                            label={translate('Total check')}
                            title={`${totalCheck}`} ></FText>
                    </View>
                {matrixBtn}
            </View>
        )
    }
}
