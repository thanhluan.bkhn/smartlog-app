import React, { Component } from 'react'
import { Text, View, Platform, ViewProperties ,StyleSheet} from 'react-native'
import { RNCamera, RNCameraProps } from 'react-native-camera';


type Props = RNCameraProps & ViewProperties & {
    refCamera: (ref: RNCamera) => void 
}
export default class FCamera extends Component<Props> {

    state = {
        layoutInfo: null,
    };
    handleLayout = ({ nativeEvent: { layout } }) => {
        const { width, height } = layout;
        this.setState({
            layoutInfo: { width, height },
        });
    };
    fixStyleLayout = () => {
        let style;
        const { width, height } = this.state.layoutInfo;
        if (Platform.OS === "android") {
            let viewfinderHeight = 0,
                viewfinderWidth = 0,
                viewFinderPaddingX = 0,
                viewFinderPaddingY = 0;

            if (width && height) {
                const ratio = 4 / 3;
                if (ratio * height < width) {
                    viewfinderHeight = width / ratio;
                    viewfinderWidth = width;
                } else {
                    viewfinderWidth = ratio * height;
                    viewfinderHeight = height;
                }
                viewFinderPaddingX = (width - viewfinderWidth) / 2;
                viewFinderPaddingY = (height - viewfinderHeight) / 2;
            }
            style = {
                position: "absolute",
                width: viewfinderWidth,
                height: viewfinderHeight,
                left: viewFinderPaddingX,
                top: viewFinderPaddingY,
            };
        } else {
            style = { flex: 1 };
        }
        return style;
    }
    onLayout = ({ nativeEvent: { layout } }) => {
        const { x, y, width, height } = layout
        this.setState({
            layoutInfo: { width, height },
        });
    };
    render() {

        if (!this.state.layoutInfo) {
            return <View key="pre-info" onLayout={this.handleLayout} style={styles.containerStyle} />;
        }
        const cameraStyle = this.fixStyleLayout();
        return (
            <View
                style={{ overflow: "hidden", flex: 3, marginLeft: 10, marginTop: 10, marginRight: 10, marginBottom: -50 }}
                onLayout={this.onLayout}
                removeClippedSubviews
            >
                <RNCamera
                    
                    {...this.props}
                    ref = {this.props.refCamera}
                    style={cameraStyle}
                />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: { flex: 1, overflow: 'hidden', position: 'relative' },
  });