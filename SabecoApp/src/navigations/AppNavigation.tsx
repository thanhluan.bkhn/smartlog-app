import React from 'react';
import HomeScreen from "~/screens/app/HomeScreen"
import DrawerScreen from "~/screens/app/DrawerScreen";
import ReceiptListScreen from '~/screens/app/inbounds/ReceiptListScreen';
import CaptureScreen from '~/screens/app/inbounds/CaptureScreen';
import ReceiptDetailScreen from '~/screens/app/inbounds/ReceiptDetailScreen';
import ToolTallyScreen from '~/screens/app/inbounds/ToolTallyScreen';
import PutawayListScreen from '~/screens/app/putaway/PutawayListScreen';
import ShipDetailScreen from '~/screens/app/ship/ShipDetailScreen';
import ShipListScreen from '~/screens/app/ship/ShipListScreen';
import ToolTallyShipScreen from '~/screens/app/ship/ToolTallyShipScreen';
import PalletTypeScreen from '~/screens/app/inbounds/PalletTypeScreen';
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from 'react-navigation-drawer';
import Example from '~/screens/Example';
import PickListScreen from '~/screens/app/pick/PickListScreen';
import PalletTypeShipScreen from '~/screens/app/ship/PalletTypeShipScreen';
import PutawayDetailScreen from '~/screens/app/putaway/PutawayDetailScreen';
import CaptureShipScreen from '~/screens/app/ship/CaptureShipScreen';
import PickDetailScreen from '~/screens/app/pick/PickDetailScreen';
import StockCountScreen from '~/screens/app/inventory/StockCountScreen';
import ViewDetailScreen from '~/screens/app/inbounds/ViewDetailScreen';
import ShipViewDetailScreen from '~/screens/app/ship/ShipViewDetailScreen';


const HomeDrawer = createDrawerNavigator({
  HomeScreen: HomeScreen,
}, {
  contentComponent: (props) => <DrawerScreen {...props} />
})

const routes = {
  HomeDrawer: HomeDrawer,
  ReceiptListScreen: ReceiptListScreen,
  CaptureScreen: CaptureScreen,
  ReceiptDetailScreen: ReceiptDetailScreen,
  ToolTallyScreen: ToolTallyScreen,
  PickDetailScreen: PickDetailScreen,
  PutawayListScreen: PutawayListScreen,
  PutawayDetailScreen: PutawayDetailScreen,
  ShipListScreen: ShipListScreen,
  ShipDetailScreen: ShipDetailScreen,
  ToolTallyShipScreen: ToolTallyShipScreen,
  PalletTypeScreen: PalletTypeScreen,
  PickListScreen: PickListScreen,
  PalletTypeShipScreen: PalletTypeShipScreen,
  CaptureShipScreen: CaptureShipScreen,
  Example: Example,
  StockCountScreen: StockCountScreen,
  ViewDetailScreen: ViewDetailScreen,
  ShipViewDetailScreen: ShipViewDetailScreen
}


export default createStackNavigator(routes, {
  initialRouteName: 'HomeDrawer',
  headerMode: 'none'
})
