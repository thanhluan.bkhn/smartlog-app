

import SignInScreen from '~/screens/auth/SignInScreen'
import {createStackNavigator} from 'react-navigation-stack'

const routes = {
  SignInScreen: SignInScreen,
  
}
const options = {
  initialRouteName: 'SignInScreen',
  headerMode: 'none',
  navigationOptions: {
    header: null
  }
}
export default createStackNavigator(routes, options as any)
