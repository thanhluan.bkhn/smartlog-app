export interface IEnvConfig {
    host: string,
    hostAuth: string,
    hostMedia: string,
    appSecret: string,
}
export interface IAppEnv {
    release: IEnvConfig;
    debug: IEnvConfig;
}
export const ENV_BUIL_DAPP = {
    production: "release",
    development: "debug",
}