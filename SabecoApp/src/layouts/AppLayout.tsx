import React, { Component, Fragment } from 'react'
import { ActivityIndicator, StyleSheet, View, LayoutChangeEvent, Text } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { EColor } from '~/common/enums';
interface ILayoutProps {
    naviagtion?: NavigationScreenProp<any>
    isLoading?: boolean,
    labelLoading?: string
    onLayout?: (event: LayoutChangeEvent) => void;
}
export default class AppLayout extends Component<ILayoutProps> {
    render() {
        const { isLoading, onLayout, labelLoading } = this.props
        return (
            <View onLayout={onLayout} style={{ flex: 1 }}>
                {isLoading && (
                    <View
                        style={styles.view}
                    >
                        <ActivityIndicator animating={true} size={'large'} color={'blue'} />
                        {labelLoading &&
                            <View style={{ backgroundColor: EColor.white }}>
                                <Text style={{ textAlign: 'center', color: EColor.black }}>{labelLoading} ...</Text>
                            </View>}
                    </View>
                )}
                {this.props.children}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
        position: 'absolute',
        zIndex: 999,
        width: '100%',
        height: '100%'
    }
})




