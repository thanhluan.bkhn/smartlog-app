import { action, computed } from 'mobx';
import { rootStore } from '~/App';
import { ORDER_STATUS, TYPE_HEADER, TYPE_HEADER_INVENTORY } from '~/common/constants';
import { API } from '~/services/network/api.endpoint';
import { apiService } from '~/services/network/api.service';
import { toastService } from '~/services/toast/toast.service';
import { BaseList } from '../base/base.store';
import { bindProp } from '../base/decorator';
import { IResponseList } from '../base/interfaces';

export class StockCountSkuStore extends BaseList {

    @bindProp keySearch = '';
    @action
    load = async (page: number) => {
        if (!this.isRefreshing) {
            this.isLoading = true;
        }
        try {
            const { storerCodes = [] } = rootStore.stores.authStore.selectedWharehouse
            let whereClause = ` STORERKEY IN ('${storerCodes.join(",")}')  `;
            if (this.keySearch && this.keySearch.trim().length > 0) {
                whereClause += `AND ( skugroup like '%${this.keySearch.trim()}%' 
                    OR sku like '%${this.keySearch.trim()}%' )
                `;
            }
            const result: IResponseList = await apiService.post(API.INVENTORY.LISTBYSKU, {
                page: page,
                pageSize: this.pageSize,
                whereClause
            })
            const { res, total } = result;
            this.data = [...this.data, ...res];
            this.total = total;
            this.page = page;
            this.isLoading = false;
            this.isRefreshing = false;
        } catch (error) {
            toastService.error(error.message)
            this.isLoading = false;
            this.isRefreshing = false
        }
    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }

        this.isRefreshing = true;
        this.data = [];
        await this.load(1);
    }
    @action
    loadMore = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        await this.load(this.page + 1);
    }
    @action
    search = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.data = [];
        await this.load(1);
    }
}