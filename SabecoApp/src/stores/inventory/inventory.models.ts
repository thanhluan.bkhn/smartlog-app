export interface InventoryBySku {
    whseid?: string;
    storerkey?: string;
    sku?: string;
    skugroup?: string;
    qty?: number;
    qtypickedAndAllocated?: number;
    qtypicked?: number;
    qtyallocated?: number;
    qtyavalable?: number;
    pallet?: number
}