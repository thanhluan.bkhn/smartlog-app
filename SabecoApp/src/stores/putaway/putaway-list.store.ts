import { bindProp } from "../base/decorator";
import { action, computed } from "mobx";
import { BaseList } from "../base/base.store";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { IResponseList } from "../base/interfaces";
import { rootStore } from "~/App";
import navigationService from "~/services/navigation/navigation.service";
import { toastService } from "~/services/toast/toast.service";
import { CustomError } from "~/common/modules";
import { RECEIPT_STATUS, TYPE_HEADER } from "~/common/constants";

export class PutawayListStore extends BaseList {

    @bindProp keySearch = '';
    @bindProp isLoadingContainer = false
    @bindProp valueSelected = TYPE_HEADER[0]
    @action
    load = async (page: number) => {

        if (!this.isRefreshing) {
            this.isLoading = true;
        }
        try {

            const { selectedWharehouse } = rootStore.stores.authStore;
            let whereClause = ` WHSEID = '${selectedWharehouse.code}' AND transaction_type = '${this.valueSelected} '
            AND STORERKEY IN ('${selectedWharehouse.storerCodes.join(",")}') 
            AND STATUS IN (${RECEIPT_STATUS.InReceiving.value},${RECEIPT_STATUS.Received.value},${RECEIPT_STATUS.Arrived.value})
            AND KEEPER IS NOT NULL AND KEEPER <> '' 
            `;
            if (this.keySearch && this.keySearch.trim().length > 0) {
                whereClause += `AND ( trailernumber is not null AND trailernumber like '%${this.keySearch.trim()}%'  
                    OR externreceiptkey like '%${this.keySearch.trim()}%' 
                    OR lifter like '%${this.keySearch.trim()}%')
                `;
            }
            const result: IResponseList = await apiService.post(API.RECEIPTS.LIST, {
                page: page,
                pageSize: this.pageSize,
                whereClause
            })

            const { res, total } = result;
            this.data = [...this.data, ...res]
            this.total = total;
            this.page = page;
            this.isLoading = false;
            this.isRefreshing = false;
        } catch (error) {
            const { message } = error as CustomError;
            this.isLoading = false;
            this.isRefreshing = false
        }

    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }

        this.isRefreshing = true;
        this.data = [];
        await this.load(1);
    }
    @action
    loadMore = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }

        await this.load(this.page + 1);
    }
    @action
    search = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.data = [];
        await this.load(1);
    }

    @action
    updateUserAction = async (body = { lifter: '', id: '', externalreceiptkey2: '' }, receiptkey?: string, trailernumber?: string) => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        const { receiptSelected } = rootStore.stores.putawayDetailStore
        receiptSelected.id = body.id
        receiptSelected.receiptkey = receiptkey;
        receiptSelected.trailernumber = trailernumber;
        receiptSelected.externalreceiptkey2 = body.externalreceiptkey2;
        this.isLoadingContainer = true;
        try {
            console.log('-------------------');
            console.log("B1", new Date());
            console.log('-------------------');
            const result = await apiService.post(API.RECEIPTS.UPDATE, {
                receipt: {
                    ...body
                }
            });
            console.log('-------------------');
            console.log("B2", new Date());
            console.log('-------------------');
            const indexUpdate = this.data.findIndex(item => {
                return item.id == body.id
            });
            if (indexUpdate !== -1) {
                this.data[indexUpdate].lifter = body.lifter;
            }
            console.log('-------------------');
            console.log("B3", new Date());
            console.log('-------------------');
            const putaway = await apiService.post(API.RECEIPTDETAILS.SUGGESTLOCATIONSBC, {
                receiptkey: receiptkey || ''
            });

            console.log('-------------------');
            console.log("B4", new Date());
            console.log('-------------------');
            this.isLoadingContainer = false;
            navigationService.navigate('PutawayDetailScreen')
        } catch (error) {
            console.log('-------------------');
            console.log("B5", new Date());
            console.log('-------------------');
            const { message } = error as CustomError;
            toastService.error(message);
            this.isLoadingContainer = false;
        }
    }
    @action
    updateIndex = async (selectedIndex) => {
        this.valueSelected = TYPE_HEADER[selectedIndex];
        this.data = [];
        await this.load(1);
    }

    @computed get selectedIndex() {
        return TYPE_HEADER.findIndex(item => item == this.valueSelected);
    }
}