import { bindProp } from "../base/decorator";
import { action, toJS } from "mobx";
import { BaseList } from "../base/base.store";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { CustomError } from "~/common/modules";
import { toastService } from "~/services/toast/toast.service";
import { IResponseList } from "../base/interfaces";
import { rootStore } from "~/App";
import navigationService from "~/services/navigation/navigation.service";

export class PutawayDetailStore extends BaseList {

    @bindProp receiptSelected = {
        receiptkey: '',
        trailernumber: '',
        lifter: '',
        id: '',
        externalreceiptkey2: ''
    };
    @bindProp itemSelected;
    @action
    load = async () => {
        console.log('-------------------');
        console.log("LOAD 1: ", new Date());
        console.log('-------------------');
        if (!this.isRefreshing) {
            this.isLoading = true;
        }
        try {
            const { selectedWharehouse } = rootStore.stores.authStore;
            const whereClause = ` WHSEID = '${selectedWharehouse.code}' 
                 AND STORERKEY IN ('${selectedWharehouse.storerCodes.join(",")}') 
                 AND RECEIPTKEY = '${this.receiptSelected.receiptkey}'  
            `;
            const result: IResponseList = await apiService.post(API.RECEIPTDETAILS.LISTBYSUGGESTLOCATIONSBC, {
                page: 1,
                pageSize: 100,
                whereClause,
                receiptkey: this.receiptSelected.receiptkey
            })

            let { res } = result;
            this.data = [...res]
            this.isLoading = false;
            this.isRefreshing = false;

        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(message)
            this.isLoading = false;
            this.isRefreshing = false
        }
        console.log('-------------------');
        console.log("LOAD 2: ", new Date());
        console.log('-------------------');
    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.isRefreshing = true;
        this.data = [];
        await this.load();
    }

    @action
    confirmPutaway = async (newLoc: string, qtyNewLoc: number) => {
        this.isLoading = true;
        const { translate } = rootStore.stores.languageStore
        try {
            let result: any = await apiService.post(API.RECEIPTDETAILS.CONFIRMSUGGESTLOCATION, {
                newLoc,
                qtyNewLoc,
                objFinds: {
                    ...this.itemSelected
                }
            })
            if (result && result.length > 0) {
                throw new CustomError(result[0].message || 'NOT FOUND');
            }
            await this.load();
            this.isLoading = false;

            toastService.success(translate('Done'));
        } catch (err) {
            const { message } = err as CustomError;
            toastService.error(translate(message));
            this.isLoading = false;

        }
    }

    @action
    reject = async () => {
        this.isLoading = true;
        try {
            const result = await apiService.post(API.RECEIPTS.UPDATE, {
                receipt: {
                    lifter: '',
                    id: this.receiptSelected.id,
                }
            });
            let { data } = rootStore.stores.putawayListStore
            const indexUpdate = data.findIndex(item => {
                return item.id == this.receiptSelected.id
            });
            if (indexUpdate !== -1) {
                data[indexUpdate].lifter = '';
            }
            this.isLoading = false;
            const { translate } = rootStore.stores.languageStore
            toastService.success(translate('Done'));
            navigationService.navigate('PutawayListScreen')
        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(message);
            this.isLoading = false;

        }
    }
}
