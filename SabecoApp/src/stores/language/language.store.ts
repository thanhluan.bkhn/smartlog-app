import { observable, computed, action, autorun } from "mobx";
import { bindProp } from "../base/decorator";
import { LanguageResource } from "./language.model";


export class LanguageStore {

  @bindProp resource = {}
  @bindProp locale = 'vn'

  @action
   loadResource = async() =>{
      this.resource = LanguageResource[this.locale]
  }

  @action
  translate = (key: string) => {
    if(!key) return '';
    try {
      if(!this.resource ) {
        return key;
      }
      if(!this.resource[key] && !this.resource[key.toLowerCase()] && !this.resource[key.toUpperCase()]){
        return key
      }
    
      return this.resource[key] || this.resource[key.toLowerCase()] || this.resource[key.toUpperCase()]
    } catch (error) {

      return key
    }
    
  }
}