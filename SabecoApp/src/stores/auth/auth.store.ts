import React from 'react';
import { bindProp } from "../base/decorator";
import { action, toJS, computed } from "mobx";
import { apiAuthService } from "~/services/network/api.service";
import { AUTH_ENPOINT } from "~/services/network/auth.endpoint";
import AsyncStorage from "@react-native-community/async-storage";
import navigationService from "~/services/navigation/navigation.service";
import { toastService } from "~/services/toast/toast.service";
import { CustomError } from '~/common/modules';
import { rootStore } from '~/App';


interface IWarehouse {
    code?: string,
    susr01?: string,
    susr02?: string,
    susr03?: string,
    susr04?: string,
    susr05?: string,
    claim?: {
        [key: string]: string
    },
    companyCode?: string,
    isVisiable?: boolean,
    storerCodes?: string[]
}


export class AuthStore {
    @bindProp isLoading = false;
    @bindProp token = '';
    @bindProp warehouse = undefined;
    @bindProp selectedWharehouse: IWarehouse = undefined;
    @bindProp userInfo = {
        username: '',
    }



    @action
    signIn = async (username: string, password: string) => {
        this.isLoading = true
        try {
            const res: any = await apiAuthService.post(AUTH_ENPOINT.LOGIN, {
                username: username,
                password: password
            })
            if (!res.warehouse) {
                throw 'User not find Wharehouse'
            }
            const warehouse = res.warehouse
            const whseids = Object.values(warehouse).map((wh: any) => {
                return wh.code
            })
            if (whseids.length === 0) {
                throw 'User not find Wharehouse'
            }
            const whseid = whseids[0];

            this.token = res.token

            this.warehouse = warehouse
            this.selectedWharehouse = warehouse[whseid]

            this.userInfo.username = username
            await AsyncStorage.setItem('Token', this.token)
            await AsyncStorage.setItem('Whseid', this.selectedWharehouse.code)
            const { translate } = rootStore.stores.languageStore
            toastService.success(translate('Done'));;
            navigationService.navigate('AppNavigation')
            this.isLoading = false
        } catch (err) {
            const { message } = err as CustomError;
            toastService.error(message);
            this.isLoading = false
        }
    }
    @action
    logout = async () => {
        try {
            await AsyncStorage.clear();
            this.userInfo.username = '';
            this.token = '';
            this.warehouse = undefined;
            this.selectedWharehouse = undefined;
            navigationService.navigate('AuthNavigation')
        } catch (err) {
            this.userInfo.username = '';
            this.token = '';
            this.warehouse = undefined;
            this.selectedWharehouse = undefined;
            navigationService.navigate('AuthNavigation')
        }
    }

    @action
    authenticate = async (navigation: any) => {
        try {
            const res: any = await apiAuthService.post(AUTH_ENPOINT.AUTHENTICATE, {
            })
            if (!res.warehouse) {
                throw 'User not find Wharehouse'
            }
            const warehouse = res.warehouse
            const whseids = Object.values(warehouse).map((wh: any) => {
                return wh.code
            })
            if (whseids.length === 0) {
                throw 'User not find Wharehouse'
            }
            const whseid = whseids[0];
            this.token = res.token
            this.warehouse = warehouse
            this.userInfo.username = res.username
            this.selectedWharehouse = warehouse[whseid]

            await AsyncStorage.setItem('Token', this.token)
            await AsyncStorage.setItem('Whseid', this.selectedWharehouse.code)

            this.isLoading = false
            navigationService.navigate('AppNavigation')
        } catch (err) {
            // const {message} =  err as CustomError;
            // toastService.error(message);
            this.isLoading = false
            navigation.navigate('AuthNavigation')
        }
    }

    @action
    changeWarehouse = async (whseid: string) => {
        try {
            this.selectedWharehouse = this.warehouse[whseid]
            await AsyncStorage.setItem('Whseid', this.selectedWharehouse.code)
        } catch (err) {
            toastService.error(err.message)
        }
    }

    @computed get whseids() {
        if (!this.warehouse) return [];
        return Object.values(this.warehouse).filter((v: any) => v.isVisiable).map((wh: any) => {
            return wh.code
        })
    }
    @computed get isSpecial() {
        try {
            // return true;
            const { susr01 } = this.selectedWharehouse

            return susr01 === 'CPKV1';
        } catch (error) {
            return false
        }
    }
}