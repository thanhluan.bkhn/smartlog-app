import { AuthStore } from './auth/auth.store';
import { LanguageStore } from './language/language.store';
import { PickDetailStore } from './pick/pick-detail.store';
import { PickListStore } from './pick/pick-list.store';
import { PutawayDetailStore } from './putaway/putaway-detail.store';
import { PutawayListStore } from './putaway/putaway-list.store';
import { ReceiptListStore } from './inbound/receipt-list.store';
import { ReceiptDetailStore } from './inbound/receipt-detail.store';
import { ShipListStore } from './ship/ship-list.store';
import { ShipDetailStore } from './ship/ship-detail.store';
import { PalletTypeStore } from './inbound/pallet-type.store';
import { PalletTypeShipStore } from './ship/pallet-type-ship.store';
import { StockCountStore } from './inventory/stock-count.store';
import { CaptureReceiptStore } from './inbound/capture-receipt.store';
import { CaptureShipStore } from './ship/capture-ship.store';
import { StockCountSkuStore } from './inventory/stock-count-sku.store';

export interface IStore {
    authStore: AuthStore,
    languageStore: LanguageStore,
    receiptListStore: ReceiptListStore,
    receiptDetailStore: ReceiptDetailStore,
    pickListStore: PickListStore
    pickDetailStore: PickDetailStore,
    putawayListStore: PutawayListStore,
    shipListStore: ShipListStore,
    shipDetailStore: ShipDetailStore,
    palletTypeStore: PalletTypeStore,
    palletTypeShipStore: PalletTypeShipStore,
    putawayDetailStore: PutawayDetailStore,
    stockCountStore: StockCountStore,
    captureReceiptStore: CaptureReceiptStore,
    captureShipStore: CaptureShipStore,
    stockCountSkuStore: StockCountSkuStore,
}
export type IKeyStore = keyof IStore
export type IPropsComponent = {
    [k in keyof IStore]?: any
}

export class RootStore {
    public stores: IStore;
    constructor() {
        this.stores = {
            authStore: new AuthStore(),
            languageStore: new LanguageStore(),
            receiptListStore: new ReceiptListStore(),
            receiptDetailStore: new ReceiptDetailStore(),
            pickListStore: new PickListStore(),
            pickDetailStore: new PickDetailStore(),
            putawayListStore: new PutawayListStore(),
            shipListStore: new ShipListStore(),
            shipDetailStore: new ShipDetailStore(),
            palletTypeStore: new PalletTypeStore(),
            palletTypeShipStore: new PalletTypeShipStore(),
            putawayDetailStore: new PutawayDetailStore(),
            stockCountStore: new StockCountStore(),
            captureReceiptStore: new CaptureReceiptStore(),
            captureShipStore: new CaptureShipStore(),
            stockCountSkuStore: new StockCountSkuStore()
        }
    }
}