import { bindProp } from "../base/decorator";
import { action,toJS, computed } from "mobx";
import { BaseList } from "../base/base.store";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { CustomError } from "~/common/modules";
import { toastService } from "~/services/toast/toast.service";
import navigationService from "~/services/navigation/navigation.service";
import { IResponseList } from "../base/interfaces";
import { rootStore } from "~/App";
import { ORDER_STATUS, PICKDETAIL_STATUS } from "~/common/constants";

export class PickDetailStore extends BaseList {

    @bindProp orderSelected = {
        orderkey : '',
        trailernumber: '',
        id: '',
        externalorderkey2: ''
    };
    @action
    load = async () => {
        if (!this.isRefreshing) {
            this.isLoading = true;
        }
        try {
            const whereClause = ` ORDERKEY = ('${this.orderSelected.orderkey}') `;
            const result: IResponseList = await apiService.post(API.PICKDETAIL.LISTSBC, {
                page: 1,
                pageSize: 100,
                whereClause,
                orderkey: this.orderSelected.orderkey
            })
            let { res } = result;
            this.data = [...res]
            this.isLoading = false;
            this.isRefreshing = false;
        } catch (error) {
            const {message} =  error as CustomError;
            toastService.error(message);
            this.isLoading = false;
            this.isRefreshing = false
        }
    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.isRefreshing = true;
        this.data = [];
        await this.load();
    }
  
   
    @action
    reject = async () => {
        this.isLoading = true;
        try {
            const result = await apiService.post(API.ORDERS.UPDATE, {
                order: {
                    lifter: '',
                    id: toJS(this.orderSelected.id)
                }
            });
            let {data} = rootStore.stores.pickListStore
            const indexUpdate =  data.findIndex(item => {
                return  item.id == this.orderSelected.id
             });
             if(indexUpdate !== -1) {
                 data[indexUpdate].lifter = '';
             }
            this.isLoading = false;
            this.load();
            const {translate} = rootStore.stores.languageStore
            toastService.success(translate('Done'));
            navigationService.navigate('PickListScreen')
        } catch (error) { 
            const {message} =  error as CustomError;
            toastService.error(message);
            this.isLoading = false;
        }
    }

    @action
    pick = async (pickdetail = {}) => {
        this.isLoading = true;
        const {translate} = rootStore.stores.languageStore
        try {
           
            const res = await apiService.post(API.PICKDETAIL.PICK,{
                ...pickdetail
            })
            this.isLoading = false;
            toastService.success(translate('Done'));
        } catch (error) {
            const {message} =  error as CustomError;
            this.isLoading = false;
            toastService.error(message);
        }
    }
}