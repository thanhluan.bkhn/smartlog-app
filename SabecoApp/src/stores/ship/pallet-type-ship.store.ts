import { action, toJS } from 'mobx';
import { rootStore } from '~/App';
import { CustomError } from '~/common/modules';
import navigationService from '~/services/navigation/navigation.service';
import { API } from '~/services/network/api.endpoint';
import { apiService } from '~/services/network/api.service';
import { toastService } from '~/services/toast/toast.service';

import { bindProp } from '../base/decorator';

export class PalletTypeShipStore {

    @bindProp data = {};
    @bindProp isLoading = false;

    @action
    countItemPallet = async () => {
        this.isLoading = true;
        try {
            const { orderkey, id } = rootStore.stores.shipDetailStore.orderSelected;
            const res = await apiService.post(API.ORDERS.COUNTITEMPALLET, {
                orderId: id
            })
            this.data = res;
            this.isLoading = false;
        } catch (error) {
            this.isLoading = false;
            const { message } = error as CustomError
            toastService.error(message)
        }
    }
    @action 
    addOrMove = async () => {
        this.isLoading = true;
        try {
            const { orderkey, id } = rootStore.stores.shipDetailStore.orderSelected;
            const {captureShipStore } = rootStore.stores
            
            const res = await apiService.post(API.ORDERS.ADDORMOVE, {
                orderId: id,
                qtyPallet: toJS(this.data)
            })
            this.isLoading = false;
            captureShipStore.idSelected = id;
            navigationService.navigate("ShipListScreen")
            const {translate} = rootStore.stores.languageStore
            toastService.success(translate('Done'));
        } catch (error) {
            const { message } = error as CustomError
            this.isLoading = false;
            toastService.error(message)
        }
    }
}