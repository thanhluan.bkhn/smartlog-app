import { action, computed } from 'mobx';
import { rootStore } from '~/App';
import { ORDER_STATUS, TYPE_HEADER } from '~/common/constants';
import { CustomError } from '~/common/modules';
import navigationService from '~/services/navigation/navigation.service';
import { API } from '~/services/network/api.endpoint';
import { apiService } from '~/services/network/api.service';
import { toastService } from '~/services/toast/toast.service';

import { BaseList } from '../base/base.store';
import { bindProp } from '../base/decorator';
import { IResponseList } from '../base/interfaces';

export class ShipListStore extends BaseList {

    @bindProp keySearch = '';
    @bindProp isLoadingContainer = false;
    @bindProp valueSelected = TYPE_HEADER[0]

    @action
    load = async (page: number) => {
        if (!this.isRefreshing) {
            this.isLoading = true;
        }
        try {
            const { isSpecial } = rootStore.stores.authStore

            console.log('-------------------');
            console.log(isSpecial);
            console.log('-------------------');

            const { storerCodes = [] } = rootStore.stores.authStore.selectedWharehouse
            let andWhere = '';
            if (isSpecial) {
                andWhere = `AND STATUS IN ('${ORDER_STATUS.Picked.value}','${ORDER_STATUS.PartShipped.value}','${ORDER_STATUS.ShipCompleted.value}','${ORDER_STATUS.Allocated.value}','${ORDER_STATUS.PartAllocate.value}','${ORDER_STATUS.New.value}') `
            } else {
                andWhere = `AND STATUS IN ('${ORDER_STATUS.Picked.value}','${ORDER_STATUS.PartShipped.value}','${ORDER_STATUS.ShipCompleted.value}','${ORDER_STATUS.Allocated.value}','${ORDER_STATUS.PartAllocate.value}') `
            }
            let whereClause = ` STORERKEY IN ('${storerCodes.join(",")}') AND order_type = '${this.valueSelected}' ` + andWhere;

            if (this.keySearch && this.keySearch.trim().length > 0) {
                whereClause += `AND ( trailernumber is not null AND trailernumber like '%${this.keySearch.trim()}%'  
                    OR externalorderkey2 like '%${this.keySearch.trim()}%' 
                    OR keeper like '%${this.keySearch.trim()}%')
                `;
            }
            const result: IResponseList = await apiService.post(API.ORDERS.LISTSBC, {
                page: page,
                pageSize: this.pageSize,
                whereClause
            })
            const { res, total } = result;
            this.data = [...this.data, ...res];
            this.total = total;
            this.page = page;
            this.isLoading = false;
            this.isRefreshing = false;
        } catch (error) {
            toastService.error(error.message)
            this.isLoading = false;
            this.isRefreshing = false
        }
    }
    @action
    gotoViewDetail = async (body = { keeper: '', id: '', externalorderkey2: '' }, orderkey: string, trailernumber?: string) => {
        const { orderSelected } = rootStore.stores.shipDetailStore
        orderSelected.id = body.id
        orderSelected.orderkey = orderkey;
        orderSelected.trailernumber = trailernumber;
        orderSelected.externalorderkey2 = body.externalorderkey2;
        navigationService.navigate('ShipViewDetailScreen')
    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }

        this.isRefreshing = true;
        this.data = [];
        await this.load(1);
    }
    @action
    loadMore = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }

        await this.load(this.page + 1);
    }
    @action
    search = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.data = [];

        await this.load(1);
    }

    @action
    updateUserAction = async (body = { keeper: '', id: '', externalorderkey2: '' }, orderkey: string, trailernumber?: string) => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        const { orderSelected } = rootStore.stores.shipDetailStore
        const { isSpecial } = rootStore.stores.authStore
        orderSelected.id = body.id
        orderSelected.orderkey = orderkey;
        orderSelected.trailernumber = trailernumber;
        orderSelected.externalorderkey2 = body.externalorderkey2;
        this.isLoadingContainer = true;

        try {
            const result = await apiService.post(API.ORDERS.UPDATE, {
                order: {
                    ...body,
                },
            });

            const indexUpdate = this.data.findIndex(item => {
                return item.id == body.id
            });
            if (indexUpdate !== -1) {

                this.data[indexUpdate].keeper = body.keeper;
            }
            if (isSpecial) {
                await apiService.post(API.OUTBOUNDS.ALLOCATE, {
                    orderIds: [body.id]
                });
            }
            this.isLoadingContainer = false;
            navigationService.navigate('ShipDetailScreen')
        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(message)
            this.isLoadingContainer = false;

        }
    }
    @action
    updateIndex = async (selectedIndex) => {
        this.valueSelected = TYPE_HEADER[selectedIndex];
        this.data = [];
        await this.load(1);
    }

    @computed get selectedIndex() {
        return TYPE_HEADER.findIndex(item => item == this.valueSelected);
    }
}