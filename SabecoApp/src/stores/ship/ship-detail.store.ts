import { bindProp } from "../base/decorator";
import { action, runInAction, toJS, computed } from "mobx";
import { BaseList } from "../base/base.store";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { CustomError } from "~/common/modules";
import { toastService } from "~/services/toast/toast.service";
import navigationService from "~/services/navigation/navigation.service";
import { IResponseList } from "../base/interfaces";
import { rootStore } from "~/App";
import { ORDER_STATUS, PICKDETAIL_STATUS } from "~/common/constants";

export class ShipDetailStore extends BaseList {

    @bindProp orderSelected = {
        orderkey : '',
        trailernumber: '',
        id: '',
        externalorderkey2: ''
    };

    @bindProp itemSelected;

    @action
    load = async () => {
        if (!this.isRefreshing) {
            this.isLoading = true;
        }
        try {
            const whereClause = ` ORDERKEY = ('${this.orderSelected.orderkey}') `;
            const result: IResponseList = await apiService.post(API.PICKDETAIL.LISTSBC, {
                page: 1,
                pageSize: 1000,
                whereClause,
                orderkey: this.orderSelected.orderkey
            })
            const { res } = result;
            this.data = [...res]
            this.isLoading = false;
            this.isRefreshing = false;
        } catch (error) {
            const {message} =  error as CustomError;
            toastService.error(message);
            this.isLoading = false;
            this.isRefreshing = false
        }
    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.isRefreshing = true;
        this.data = [];
        await this.load();
    }
  
    @computed get isBack(){
        const objTotal =  this.data.reduce( (prevValue,currentValue) => {
            return {
                originalqty: prevValue.originalqty + currentValue.originalqty,
                shippedqty: prevValue.shippedqty + currentValue.shippedqty
            }
        }, {
            originalqty: 0,
            shippedqty: 0
        })
        return !(objTotal.originalqty > 0 && objTotal.originalqty === objTotal.shippedqty)
    }
   
    @action
    reject = async () => {
        this.isLoading = true;
        try {
            const result = await apiService.post(API.ORDERS.UPDATE, {
                order: {
                    keeper: '',
                    id: this.orderSelected.id
                }
            });
            let {data} = rootStore.stores.shipListStore
            const indexUpdate =  data.findIndex(item => {
                return  item.id == this.orderSelected.id
             });
             if(indexUpdate !== -1) {
                 data[indexUpdate].keeper = '';
             }
            this.isLoading = false;
            this.load();
            const {translate} = rootStore.stores.languageStore
            toastService.success(translate('Done'));
            navigationService.navigate('ShipListScreen')
        } catch (error) { 
            const {message} =  error as CustomError;
            toastService.error(message);
            this.isLoading = false;
        }
    }

    @action
    ship = async (receiptDetail = {}) => {
        this.isLoading = true;
        try {
            
            const res = await apiService.post(API.PICKDETAIL.SHIP,{
                ...receiptDetail
            })
           
            this.isLoading = false;
            const {translate} = rootStore.stores.languageStore
            toastService.success(translate('Done'));
            this.load();
        } catch (error) {
            const {message} =  error as CustomError;
            this.isLoading = false;
            toastService.error(message);
        }
    }

    
}