import { bindProp } from "./decorator";
export class BaseList {
    @bindProp isLoading = false;
    @bindProp isRefreshing = false;
    @bindProp page = 1;
     pageSize = 8;

    @bindProp  data = []
    @bindProp total = 0

}