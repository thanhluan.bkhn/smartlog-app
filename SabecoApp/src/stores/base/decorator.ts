import { observer, inject } from "mobx-react";
import { IKeyStore, IStore } from "../index";
import { observable, action } from 'mobx'

export const bindCommon = (store: IStore) => {
    return {
        selectedWharehouse: store.authStore.selectedWharehouse,
        translate: store.languageStore.translate,
        userInfo: store.authStore.userInfo,
        isSpecial: store.authStore.isSpecial
    }
}
export function BindStore(...storeNames: IKeyStore[]): ClassDecorator {
    return function (target: any) {
        return {
            ...inject(...storeNames)(target),
            ...observer(target)
        }
    }
}


type TFuncBindStore = (store: IStore) => object
export function BinPropertyStore(func: TFuncBindStore): ClassDecorator {
    return function (target: any) {
        return {
            ...inject(func)(target),
            ...observer(target)
        }
    }
}



export function bindProp(target: Object, key: string | symbol, baseDescriptor?: PropertyDescriptor) {
    return observable(target, key, baseDescriptor)
}