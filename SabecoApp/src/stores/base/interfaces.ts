import { NavigationScreenProp } from 'react-navigation'
export interface IScreenProps {
    navigation?: NavigationScreenProp<any>
}
export interface IBodyList {
    page?: number,
    pageSize?: number,
    whereClause?: string
}
export interface IResponseList<T = any> {
    res: T [],
    total: number
}
