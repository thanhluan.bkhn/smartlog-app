import { bindProp } from "../base/decorator";
import { action, computed } from "mobx";
import { BaseList } from "../base/base.store";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { IResponseList } from "../base/interfaces";
import { rootStore } from "~/App";
import navigationService from "~/services/navigation/navigation.service";
import { toastService } from "~/services/toast/toast.service";
import { CustomError } from "~/common/modules";
import { RECEIPT_STATUS, TYPE_HEADER } from "~/common/constants";


export class ReceiptListStore extends BaseList {

    @bindProp keySearch = '';
    @bindProp isLoadingContainer = false

    @bindProp valueSelected = TYPE_HEADER[0]


    @action
    load = async (page: number) => {
        if (!this.isRefreshing || !this.isLoadingContainer) {
            this.isLoading = true;
        }
        const { translate } = rootStore.stores.languageStore
        try {
            const { selectedWharehouse } = rootStore.stores.authStore
            let whereClause = ` WHSEID = '${selectedWharehouse.code}' AND transaction_type = '${this.valueSelected}'
            AND STATUS <> '${RECEIPT_STATUS.New.value}'
            AND STORERKEY IN ('${selectedWharehouse.storerCodes.join(",")}') `;

            if (this.keySearch && this.keySearch.trim().length > 0) {
                whereClause += ` 
                    AND ( trailernumber is not null AND trailernumber like '%${this.keySearch.trim()}%'  
                    OR externreceiptkey like '%${this.keySearch.trim()}%' 
                    OR keeper like '%${this.keySearch.trim()}%' )
                `;
            }
            const result: IResponseList = await apiService.post(API.RECEIPTS.LIST, {
                page: page,
                pageSize: this.pageSize,
                whereClause: whereClause
            })
            const { res, total } = result;
            this.data = [...this.data, ...res]
            this.total = total;
            this.page = page;
            this.isLoading = false;
            this.isRefreshing = false;
        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(translate(message))
            this.isLoading = false;
            this.isRefreshing = false
        }
    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.isRefreshing = true;
        this.data = [];
        await this.load(1);
    }
    @action
    loadMore = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        await this.load(this.page + 1);
    }
    @action
    search = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.data = [];
        await this.load(1);
    }

    @action
    gotoViewDetail = async (body = { keeper: '', id: '', externalreceiptkey2: '' }, receiptkey?: string, trailernumber?: string) => {
        const { receiptSelected } = rootStore.stores.receiptDetailStore
        receiptSelected.id = body.id
        receiptSelected.receiptkey = receiptkey;
        receiptSelected.trailernumber = trailernumber;
        receiptSelected.externalreceiptkey2 = body.externalreceiptkey2;
        navigationService.navigate('ViewDetailScreen')
    }

    @action
    updateUserAction = async (body = { keeper: '', id: '', externalreceiptkey2: '' }, receiptkey?: string, trailernumber?: string) => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        const { receiptSelected } = rootStore.stores.receiptDetailStore
        receiptSelected.id = body.id
        receiptSelected.receiptkey = receiptkey;
        receiptSelected.trailernumber = trailernumber;
        receiptSelected.externalreceiptkey2 = body.externalreceiptkey2;
        this.isLoadingContainer = true;
        try {
            const result = await apiService.post(API.RECEIPTS.CHANGEKEEPERANDPALLETIONSBC, {
                receipt: {
                    ...body
                }
            });
            const indexUpdate = this.data.findIndex(item => {
                return item.id == body.id
            });
            if (indexUpdate !== -1) {
                this.data[indexUpdate].keeper = body.keeper;
            }

            this.isLoadingContainer = false;
            navigationService.navigate('ReceiptDetailScreen')
        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(message);
            this.isLoadingContainer = false;
        }
    }

    @action
    updateIndex =  async (selectedIndex) => {
        this.valueSelected = TYPE_HEADER[selectedIndex];
        this.data = [];
        await this.load(1);
    }

    @computed get selectedIndex(){
        return TYPE_HEADER.findIndex( item => item == this.valueSelected);
    }
}