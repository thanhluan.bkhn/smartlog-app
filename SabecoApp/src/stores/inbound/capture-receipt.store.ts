import { bindProp } from "../base/decorator";
import { action } from "mobx";
import { rootStore } from "~/App";
import { CustomError } from "~/common/modules";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { TABLE_KEY_IMAGES } from "~/common/constants";
import { toastService } from "~/services/toast/toast.service";
import navigationService from "~/services/navigation/navigation.service";
import { configEnv } from "~/@config";

export class CaptureReceiptStore {
    @bindProp idSelected = '';

    @bindProp isLoading = false;


    @bindProp images: {
        uri: string,
        id?: ''
    }[] = [];

    private upload = async (url, data) => {

        const { token, selectedWharehouse } = rootStore.stores.authStore;

        let options: any = {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                token: token,
                whseid: selectedWharehouse.code
            },
            method: 'POST'
        };

        options.body = new FormData();
        for (let key in data) {
            options.body.append(key, data[key]);
        }
        return fetch(url, options)
    }

    private saveImage = async (image: { uri: string, id: string }) => {

        const { code } = rootStore.stores.authStore.selectedWharehouse
        if (image.uri.startsWith('http')) {
            return;
        }
        const uri: string = image.uri;
        const arr = uri.split("/");
        const fileName = arr[arr.length - 1];
        const { host, hostAuth, hostMedia } = configEnv();
        const data = await this.upload(host + API.MEDIA.UPLOAD, {
            file: {
                uri: uri,
                type: 'image/jpeg',
                name: fileName,
            }
        })

        const imageData = await data.json();
        const jsonData = imageData.data

        const entity = {
            id: image.id,
            whseid: code,
            urlpath: jsonData.path,
            extension: jsonData.mimetype,
            tableid: this.idSelected,
            tablekey: TABLE_KEY_IMAGES.RECEIPT
        }
        const result = await apiService.post(API.MEDIA.SAVE, {
            entity
        })

    }

    @action
    load = async () => {
        this.images = [];
        this.isLoading = true;
        const { translate } = rootStore.stores.languageStore
        try {
            const data = await apiService.post(API.MEDIA.FINDPAGINATION, {
                where: {
                    tablekey: TABLE_KEY_IMAGES.RECEIPT,
                    tableid: this.idSelected,
                },
                order: {
                    editdate: 'DESC'
                },
                pageSize: 2,
                pageIndex: 1
            })
            const { res = [], total = 0 } = data;
            this.images = res.map(item => {
                return {
                    id: item.id,
                    uri: item.urlpath
                }
            });
            this.isLoading = false
        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(translate(message))
            this.isLoading = false;
        }

    }
    @action
    removeImage = (index) => {
        this.images.splice(index, 1)
    }

    @action
    saveImages = async () => {
        this.isLoading = true;
        const { translate } = rootStore.stores.languageStore
        try {

            if (!this.images || this.images.length === 0) {
                throw new CustomError("Image empty");
            }
            for (let idx = 0; idx < this.images.length; idx++) {
                const item = this.images[idx];
                await this.saveImage({
                    id: item.id,
                    uri: item.uri
                })
            }
            toastService.success(translate('Done'));;
            this.isLoading = false
            navigationService.navigate('ReceiptListScreen')
        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(translate(message))
            this.isLoading = false;

        }
    }

}