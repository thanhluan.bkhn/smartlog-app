import { bindProp } from "../base/decorator";
import { action, toJS, computed } from "mobx";
import { BaseList } from "../base/base.store";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { CustomError } from "~/common/modules";
import { toastService } from "~/services/toast/toast.service";
import navigationService from "~/services/navigation/navigation.service";
import { rootStore, globalEvents } from "~/App";
import Moment from 'moment-timezone'
import { EKeyEvent } from "~/common/enums/keyevent";


const pipeDateTime = (value) => {
    if (!value) return '';
    return Moment(value).format("DD-MM-YYYY");
}


interface ICacheCount {
    [key: string]: number
}

export class ReceiptDetailStore extends BaseList {

    constructor() {
        super();
        globalEvents.on(EKeyEvent.RELOAD_RECEIPT_DETAIL, this.reload)
    }

    @bindProp receiptSelected = {
        receiptkey: '',
        trailernumber: '',
        lifter: '',
        id: '',
        externalreceiptkey2: ''
    };
    @bindProp itemSelected;

    @bindProp cacheCount: ICacheCount = {}


    private mapCache = (data = []) => {
        data.forEach(item => {
            const { whseid, storerkey, receiptkey, sku, lottable01 = '', lottable04 = '', lottable05 = '' } = item;
            const key = whseid + storerkey + receiptkey + sku + lottable01 + lottable04 + lottable05;
            // if(!this.cacheCount.get(key)){
            //     this.cacheCount.set(key,0);
            // }
        })
    }

    reload = async () => {
        this.data = [];
        await this.load();
    }

    @computed get isBack() {
        const objTotal = this.data.reduce((prevValue, currentValue) => {
            return {
                qtyexpected: prevValue.qtyexpected + currentValue.qtyexpected,
                qtyreceived: prevValue.qtyreceived + currentValue.qtyreceived
            }
        }, {
            qtyexpected: 0,
            qtyreceived: 0
        })
        return !(objTotal.qtyexpected > 0 && objTotal.qtyexpected === objTotal.qtyreceived)
    }

    @action
    load = async () => {
        if (!this.isRefreshing) {
            this.isLoading = true;
        }
        try {
            const { receiptDetailStore, authStore } = rootStore.stores
            const { receiptkey } = receiptDetailStore.receiptSelected
            const { selectedWharehouse } = authStore
            const body = {
                receiptkey: receiptkey,
                whseid: selectedWharehouse.code
            }
            let result: any[] = await apiService.post(API.RECEIPTDETAILS.LISTSBC, body)
            this.data = [...result]
            this.mapCache(this.data);
            this.isLoading = false;
            this.isRefreshing = false;
        } catch (error) {
            this.isLoading = false;
            this.isRefreshing = false
        }
    }
    @action
    onRefresh = async () => {
        if (this.isLoading || this.isRefreshing) {
            return;
        }
        this.isRefreshing = true;
        this.data = [];
        await this.load();
    }

    @action
    confirm = async (receiptDetail) => {
        this.isLoading = true;
        const { translate } = rootStore.stores.languageStore
        try {
            let result: any[] = await apiService.post(API.RECEIPTDETAILS.CONFIRMRECEIVE, receiptDetail)
            if (result && result.length > 0) {
                throw new CustomError(translate(`${result.length} not receive`))
            }
            this.isLoading = false;
            await this.load();
            toastService.success(translate('Done'));
        } catch (err) {

            const { message } = err as CustomError;
            toastService.error(translate(message));
            this.isLoading = false;

        }
    }
    @action
    reject = async () => {
        this.isLoading = true;
        try {
            const result = await apiService.post(API.RECEIPTS.UPDATE, {
                receipt: {
                    keeper: '',
                    id: this.receiptSelected.id,
                }
            });
            let { data } = rootStore.stores.receiptListStore
            const indexUpdate = data.findIndex(item => {
                return item.id == this.receiptSelected.id
            });
            if (indexUpdate !== -1) {
                data[indexUpdate].keeper = '';
            }
            this.isLoading = false;
            const { translate } = rootStore.stores.languageStore
            toastService.success(translate('Done'));
            navigationService.navigate('ReceiptListScreen')
        } catch (error) {
            const { message } = error as CustomError;
            toastService.error(message);
            this.isLoading = false;

        }
    }
    @action
    updateProductionDateSBC = async (newlottable04: string, item) => {

        this.isLoading = true;
        let { whseid, storerkey, receiptkey, sku, lottable01, lottable04, lottable05 } = item;
        const key = whseid + storerkey + receiptkey + sku + lottable01 + lottable04 + lottable05;
        try {
            let result: any = await apiService.post(API.RECEIPTDETAILS.UPDATEPRODUCTIONDATESBC, {
                whseid, storerkey, receiptkey, sku, lottable01, lottable04,
                newlottable04
            });
            //  const totalCheck = this.cacheCount.get(key) || 0;
            const newKey = whseid + storerkey + receiptkey + sku + lottable01 + newlottable04 + lottable05;
            // this.cacheCount.set(newKey,totalCheck);
            this.isLoading = false;
            const { translate } = rootStore.stores.languageStore
            toastService.success(translate('Done'));
        } catch (err) {
            const { message } = err as CustomError;
            toastService.error(message);
            this.isLoading = false;

        }
    }
    @action
    updateLottable01SBC = async (newlottable01: string, item) => {

        this.isLoading = true;
        let { whseid, storerkey, receiptkey, sku, lottable01, lottable04, lottable05 } = item;
        const key = whseid + storerkey + receiptkey + sku + lottable01 + lottable04 + lottable05;
        try {
            let result: any = await apiService.post(API.RECEIPTDETAILS.UPDATELOTTABLE01SBC, {
                whseid, storerkey, receiptkey, sku, lottable01, lottable04,
                newlottable01
            });
            //  const totalCheck = this.cacheCount.get(key) || 0;
            const newKey = whseid + storerkey + receiptkey + sku + lottable01 + newlottable01 + lottable05;
            // this.cacheCount.set(newKey,totalCheck);
            this.isLoading = false;
            const { translate } = rootStore.stores.languageStore
            toastService.success(translate('Done'));
        } catch (err) {
            const { message } = err as CustomError;
            toastService.error(message);
            this.isLoading = false;

        }
    }

}