import { bindProp } from "../base/decorator";
import { action, runInAction, toJS } from "mobx";
import { BaseList } from "../base/base.store";
import { apiService } from "~/services/network/api.service";
import { API } from "~/services/network/api.endpoint";
import { CustomError } from "~/common/modules";
import { toastService } from "~/services/toast/toast.service";
import navigationService from "~/services/navigation/navigation.service";
import { IResponseList } from "../base/interfaces";
import { rootStore } from "~/App";
import { ORDER_STATUS } from "~/common/constants";

export class PalletTypeStore {


    @bindProp data = {};
    @bindProp isLoading = false;

    @action
    countItemPallet = async () => {
        this.isLoading = true;
        try {
            const { receiptkey, id } = rootStore.stores.receiptDetailStore.receiptSelected;
            const res = await apiService.post(API.RECEIPTS.COUNTITEMPALLET, {
                receiptId: id
            })
            this.data = res;
            this.isLoading = false;
        } catch (error) {
            this.isLoading = false;
            const { message } = error as CustomError
            toastService.error(message)
        }
    }
    @action 
    addOrMove = async () => {
        this.isLoading = true;
        try {
            const { receiptkey, id } = rootStore.stores.receiptDetailStore.receiptSelected;
            const res = await apiService.post(API.RECEIPTS.ADDORMOVE, {
                receiptId: id,
                qtyPallet: toJS(this.data)
            })
            this.isLoading = false;
            navigationService.navigate("ReceiptListScreen")
            const {translate} = rootStore.stores.languageStore
            toastService.success(translate('Done'));
        } catch (error) {
            const { message } = error as CustomError
            this.isLoading = false;
            toastService.error(message)
        }
    }
}