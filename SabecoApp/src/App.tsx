import 'react-native-gesture-handler'
import React, { Component, Fragment } from 'react'
import { Provider } from 'mobx-react';
import RootNavigation from '~/RootNavigation'
import codePush, { CodePushOptions } from "react-native-code-push";
import { RootStore } from './stores';
import Moment from 'moment-timezone'
import { useScreens } from 'react-native-screens';
import 'moment/locale/vi';
import { Platform, UIManager, View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import Toast from './components/toast/Toast';
import { toastService } from './services/toast/toast.service';
import { SafeAreaView } from 'react-navigation';
useScreens();
Moment.tz.setDefault('Asia/Ho_Chi_Minh');
Moment.locale('vi');
import { EventEmitter } from 'events';
import { configEnv } from './@config';
import { EColor } from './common/enums';
import { ScrollView } from 'react-native-gesture-handler';
interface IProps {

}


export const globalEvents: EventEmitter = new EventEmitter();
// configure({
//   enforceActions: 'never'
// })

const envConfig = configEnv();
console.log('-------------------');
console.log(envConfig);
console.log('-------------------');
export const rootStore = new RootStore();


let codePushOptions: CodePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL };


class App extends Component<IProps> {

  state = {
    isLoading: false
  }
  checkUpdateCodePush = async () => {
    this.setState({
      isLoading: true
    })
    try {
      const check = await codePush.checkForUpdate(envConfig.appSecret);
      await codePush.sync({
        deploymentKey: envConfig.appSecret,
        installMode: codePush.InstallMode.IMMEDIATE,
      })
    } catch (error) {
    }
    this.setState({
      isLoading: false
    })
  }
  async componentDidMount() {
    console.disableYellowBox = true;
    await this.checkUpdateCodePush();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }

  }

  renderStart = () => {
    const { translate } = rootStore.stores.languageStore
    const { isLoading } = this.state
    if (isLoading) {
      return (
        <View
          style={styles.view}
        >
          <ActivityIndicator animating={true} size={'large'} color={'blue'} />
          <View style={{ backgroundColor: EColor.white }}>
            <Text style={{ textAlign: 'center', color: EColor.black }}>{translate("Check version")} ...</Text>
          </View>
        </View>
      )
    }
    return (
      <>
        <RootNavigation />
        <Toast ref={(ref) => {
          toastService.setRefContainer(ref)
        }} />
      </>
    )
  }

  render() {

    return (
      <Provider  {...rootStore.stores}>
        <SafeAreaView style={{ flex: 1 }}>
          {this.renderStart()}
        </SafeAreaView>

      </Provider>

    )
  }
}
const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 999,
    width: '100%',
    height: '100%'
  }
})

export default codePush(codePushOptions)(App) 