
import React, { Component, Fragment } from 'react';

import { createAppContainer, createSwitchNavigator, NavigationState, NavigationAction } from 'react-navigation';
import SplashScreen from '~/screens/SplashScreen';
import AuthNavigation from '~/navigations/AuthNavigation';
import AppNavigation from '~/navigations/AppNavigation';
import navigationService from './services/navigation/navigation.service';
import Toast from './components/toast/Toast';
import { toastService } from './services/toast/toast.service';
import { View } from 'react-native';



const swithNavigation = createSwitchNavigator({
    SplashScreen: SplashScreen,
    AuthNavigation: AuthNavigation,
    AppNavigation: AppNavigation
}, {
    initialRouteName: 'SplashScreen'
})

const AppContainer = createAppContainer(swithNavigation);

interface IProps {

}
export default class RootNavigation extends Component<IProps> {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <AppContainer
                    ref={navigatorRef => {
                        navigationService.setTopLevelNavigator(navigatorRef!);
                    }}
                    screenProps={{}}
                >

                </AppContainer>
                <Toast ref={(ref) => {
                    toastService.setRefContainer(ref)
                }} />
            </View>

        )
    }
}