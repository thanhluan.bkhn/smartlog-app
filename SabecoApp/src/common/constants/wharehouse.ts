export const RECEIPT_STATUS = {
    New: {
        value: '16',
        text: 'New',
        color: '#343a40'
    },
    Arrived: {
        value: '0',
        text: 'Arrived',
        color: '#6610f2'
    },
    InReceiving: {
        value: '5',
        text: 'InReceiving',
        color: '#FDC02F'
    },
    Received: {
        value: '9',
        text: 'Received',
        color: '#28a745'
    }
}
export const ORDER_STATUS = {
    New: {
        value: '04',
        text: 'New',
        color: '#343a40'
    },
    PartAllocate: {
        value: '14',
        text: 'Part Allocate',
        color: '#17a2b8'
    },
    Allocated: {
        value: '17',
        text: 'Allocated',
        color: '#007bff'
    },
    PartPick: {
        value: '52',
        text: 'Part Pick',
        color: '#ffc107'
    },
    Picked: {
        value: '55',
        text: 'Picked',
        color: '#ff4500'
    },
    PartShipped: {
        value: '92',
        text: 'Part Ship',
        color: '#da70d6'
    },
    ShipCompleted: {
        value: '95',
        text: 'Shipped',
        color: '#28a745'
    }
}
export const PICKDETAIL_STATUS = {
    Allocated: {
        value: '0',
        text: 'Allocated',
        color: '#007bff'
    },
    PickCompleted: {
        value: '5',
        text: 'Picked',
        color: '#ff4500'
    },
    Shipped: {
        value: '9',
        text: 'Shipped',
        color: '#28a745'
    }
}

export const TABLE_KEY_IMAGES = {
    RECEIPT: 'RECEIPT'.trim(),
    ORDERS: 'ORDERS'.trim()
}

export const TYPE_HEADER = ['TP', 'BB']
export const TYPE_HEADER_INVENTORY = ['SKUXLOT', 'SKU']