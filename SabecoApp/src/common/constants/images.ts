
const pathRoot = '../../../assets/images/';
export const IMAGES = {
  qoobee: require( pathRoot + 'qoobee.png'),
  sabeco_logo: require(pathRoot + 'sabeco_logo.png'),
  inbound: require(pathRoot + 'inbound.png'),
  putaway: require(pathRoot + 'putaway.png'),
  pick: require(pathRoot + 'pick.png'),
  ship: require(pathRoot + 'ship.png'),
  stock_count: require(pathRoot + 'stock_count.png')
}
