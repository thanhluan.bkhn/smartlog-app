import { StyleSheet } from "react-native";
export const STYLES = StyleSheet.create({
    container: {
      flex: 1
    },
    center: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }
  }
)
