import React, { Component, Fragment } from 'react'
import { BinPropertyStore, bindCommon } from '~/stores/base/decorator';
import { withNavigationFocus } from 'react-navigation';
import FOptimized from '~/components/views/FOptimized';
import { IStore } from '~/stores';


export interface ICommonStore {
    translate?: (key: string) => string,
    selectedWharehouse?: {
        storerCodes?: any[],
        code?: string,
    },
    isSpecial?: boolean,
    userInfo?: {
        username?: string
    }
}



export function injectCommonStore(WrappedComponent: any) {

    @BinPropertyStore(bindCommon)
    class HigherOrderComponent extends Component {
        
        render() { 
            return (
                <FOptimized >
                    <WrappedComponent {...this.props} />
                </FOptimized>
                
            )
        }
    };
    return withNavigationFocus(HigherOrderComponent as any)
};

