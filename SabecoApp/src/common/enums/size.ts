import { Dimensions } from 'react-native'
export enum ESize  {
  WIDTH_WINDOW = Dimensions.get('window').width,
  HEIGHT_WINDOW = Dimensions.get('window').height,
  WIDTH_SCREEN = Dimensions.get('screen').width,
  HEIGHT_SCREEN = Dimensions.get('window').height,
}
