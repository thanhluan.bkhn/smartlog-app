const numbeVersion = '1.7.1';
export default {
    version: 'Version ' + numbeVersion,
    versionSortName: 'V ' + numbeVersion
}