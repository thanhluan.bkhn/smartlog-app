import { ajax, AjaxResponse, AjaxRequest } from 'rxjs/ajax'
import { Subscriber } from 'rxjs'


export interface IHeader {
    [key: string]: string | Function
}
export interface IRequestOptions {
    user?: string;
    async?: boolean;
    headers?: IHeader,
    timeout?: number;
    password?: string;
    hasContent?: boolean;
    crossDomain?: boolean;
    withCredentials?: boolean;
    createXHR?: () => XMLHttpRequest;
    progressSubscriber?: Subscriber<any>;
    responseType?: string;

}
export interface IRequest extends IRequestOptions {
    method?: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE',
    url?: string;
    body?: any;
}
export interface IResponse<T extends any> extends AjaxResponse {
    response: T
}
export class HttpClient {
    private baseUrl: string
    private options: IRequestOptions
    constructor(baseurl: string, options: IRequestOptions) {
        this.baseUrl = baseurl
        this.options = options
    }
    private parseHeader() {
        return new Promise(async (resolve, reject) => {
            const headersFix: any = {};
            try {
                if (!this.options || !this.options.headers) {
                    throw new Error('NotFound');
                }
                const { headers } = this.options
                for (const key in headers) {
                    if (headers[key] instanceof Function) {
                        const value = await (headers[key] as any)()
                        Object.assign(headersFix, {
                            [key]: value
                        })
                    } else {
                        Object.assign(headersFix, {
                            [key]: headers[key]
                        })
                    }
                }
            } catch (error) {
            }
            resolve(headersFix)
        })
    }
    async get<T>(url: string, body?: any): Promise<IResponse<T>> {
        const headers = await this.parseHeader();
        const newOptions = Object.assign({}, this.options, {
            headers
        })

        const options: IRequest = {
            ...newOptions,
            method: 'GET',
            url: (this.baseUrl + url),
            body: body
        }
        return ajax(options).toPromise()
    }
    async post<T>(url: string, body?: any): Promise<IResponse<T>> {
        const headers = await this.parseHeader();
        const newOptions = Object.assign({}, this.options, {
            headers:headers
        }) 
        const options: IRequest = {
            ...newOptions,
            method: 'POST',
            url: (this.baseUrl + url),
            body: body
        }
        return ajax(options).toPromise();
    }
    async put<T>(url: string, body?: any): Promise<IResponse<T>> {
        const headers = await this.parseHeader();
        const newOptions = Object.assign({}, this.options, {
            headers
        })
        const options: IRequest = {
            ...newOptions,
            method: 'PUT',
            url: (this.baseUrl + url),
            body: body
        }
        return ajax(options).toPromise()
    }
    async patch<T>(url: string, body?: any): Promise<IResponse<T>> {
        const headers = await this.parseHeader();
        const newOptions = Object.assign({}, this.options, {
            headers
        })
        const options: IRequest = {
            ...newOptions,
            method: 'PATCH',
            url: (this.baseUrl + url),
            body: body
        }
        return ajax(options).toPromise()
    }
    async delete<T>(url: string, body?: any): Promise<IResponse<T>> {
        const headers = await this.parseHeader();
        const newOptions = Object.assign({}, this.options, {
            headers
        })
        const options: IRequest = {
            ...newOptions,
            method: 'DELETE',
            url: (this.baseUrl + url),
            body: body
        }
        return ajax(options).toPromise()
    }
}

