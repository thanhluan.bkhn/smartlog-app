import Toast, { IToastOption } from "~/components/toast/Toast";
import { EColor } from "~/common/enums";
class ToastService {
    private ref: Toast;
    setRefContainer(ref:Toast) {
        this.ref = ref;
    }
    success = (message: string = 'DONE', duration: number = 4000) => {
        this.ref.showToast({
            message,duration,
            backgroundColor: EColor.success,
            colorText: 'white',
            iconName: 'checkcircleo',
            iconType: 'antdesign'
        })
    }
    error = (message: string = 'NOT FOUND', duration: number = 4000) => {
        this.ref.showToast({
            message,duration,
            backgroundColor: EColor.primary,
            colorText: 'white',
            iconName: 'exclamationcircleo',
            iconType: 'antdesign'
        })
    }
    info = (message: string = '', duration: number = 4000) => {
        this.ref.showToast({
            message,duration,
            backgroundColor: 'gray',
            colorText: 'white'
        })
    }
    show = (message: string = '', duration: number = 4000) => {
        this.ref.showToast({
            message,duration,
            backgroundColor: 'gray',
            colorText: 'white'
        })
    }
    warning = (message: string = '', duration: number = 4000) => {
        this.ref.showToast({
            message,duration,
            backgroundColor: 'yellow',
            colorText: 'white',
            iconName: 'infocirlceo',
            iconType: 'antdesign'
        })
    }

}

export const toastService  = new ToastService();