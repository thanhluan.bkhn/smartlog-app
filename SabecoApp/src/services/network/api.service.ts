import React from 'react';
import { HttpClient, IRequestOptions, IResponse, CustomError } from "~/common/modules";
import AsyncStorage from "@react-native-community/async-storage";
import { AjaxError } from "rxjs/ajax";
import { NavigationScreenProp } from "react-navigation";
import navigationService, { NavigationService } from "../navigation/navigation.service";
import { configEnv } from '~/@config';


// const timeout = 10000;
class ApiService {
    private httpClient: HttpClient
    private navigation: NavigationScreenProp<any> | NavigationService
    constructor(baseurl: string, options: IRequestOptions) {
        this.httpClient = new HttpClient(baseurl, options)
    }

    setNavigation(navigation: NavigationScreenProp<any> | NavigationService) {
        this.navigation = navigation
    }
    private parseError(err: AjaxError) {

        const navigation = this.navigation || navigationService
        const { status } = err;
        if (status === 401) {
            navigation.navigate('AuthNavigation')
        }
        if (!err.response) {
            return new CustomError("Network not connect", status)
        }
        const { response } = err
        if (response.error) {
            if (response.error.message) {
                return new CustomError(response.error.message, status)
            }
            return new CustomError(JSON.stringify(response.error), status)
        }
        if (response.ExceptionMessage || response.Message) {
            const message = response.ExceptionMessage || response.Message
            return new CustomError(message, status);
        }
        return new CustomError(JSON.stringify(response), status)
    }
    get<T = any>(url: string, body?: any): Promise<T> {
        return new Promise((resolve, reject) => {
            this.httpClient.get<T>(url, body).then((data: IResponse<T>) => {
                if (data.status > 300) {
                    reject(new CustomError('NotAcceptable', data.status))
                } else {
                    resolve(data.response)
                }
            }).catch((err) => {
                reject(this.parseError(err))
            })
        })
    }
    post<T = any>(url: string, body?: any): Promise<T> {

        return new Promise((resolve, reject) => {
            this.httpClient.post<T>(url, body).then((data: IResponse<T>) => {

                if (data.status > 300) {
                    reject(new CustomError('NotAcceptable', data.status))
                } else {
                    resolve(data.response)
                }
            }).catch((err) => {

                reject(this.parseError(err))
            })
        })
    }
    put<T = any>(url: string, body?: any): Promise<T> {
        return new Promise((resolve, reject) => {
            this.httpClient.put<T>(url, body).then((data: IResponse<T>) => {
                if (data.status > 300) {
                    reject(new CustomError('NotAcceptable', data.status))
                } else {
                    resolve(data.response)
                }
            }).catch((err) => {
                reject(this.parseError(err))
            })
        })
    }
    patch<T = any>(url: string, body?: any): Promise<T> {
        return new Promise((resolve, reject) => {
            this.httpClient.patch<T>(url, body).then((data: IResponse<T>) => {
                if (data.status > 300) {
                    reject(new CustomError('NotAcceptable', data.status))
                } else {
                    resolve(data.response)
                }
            }).catch((err) => {
                reject(this.parseError(err))
            })
        })
    }

    delete<T = any>(url: string, body?: any): Promise<T> {
        return new Promise((resolve, reject) => {
            this.httpClient.delete<T>(url, body).then((data: IResponse<T>) => {
                if (data.status > 300) {
                    reject(new CustomError['NotAcceptable'])
                } else {
                    resolve(data.response)
                }
            }).catch((err) => {
                reject(this.parseError(err))
            })
        })
    }

}

// http://sbctest-lb.gogoviet.com/

// https://swm-be-dev.sabeco.vn/api/


const { host, hostAuth, hostMedia } = configEnv();
export const apiService = new ApiService(host, {
    // timeout,
    crossDomain: true,
    withCredentials: false,
    async: true,
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': "*",
        Token: () => {
            return AsyncStorage.getItem('Token')
        },
        Whseid: () => {
            return AsyncStorage.getItem('Whseid')
        },
        platform: 'MOBILE'.trim(),
    }
})

export const apiMediaService = new ApiService(hostMedia, {
    // timeout,
    crossDomain: false,
    withCredentials: false,
    headers: {
        token: () => {
            return AsyncStorage.getItem('Token')
        },
        'Content-Type': 'multipart/form-data'
    }
})

export const apiAuthService = new ApiService(hostAuth, {
    // timeout,
    crossDomain: true,
    withCredentials: false,
    async: true,
    headers: {
        Token: () => {
            return AsyncStorage.getItem('Token')
        },
        'Content-Type': 'application/json',
    }
})