export const AUTH_ENPOINT = {
    LOGIN: 'user/login'.trim(),
    AUTHENTICATE: 'user/authenticate'.trim()
}