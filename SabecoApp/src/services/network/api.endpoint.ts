export const API = {
    RECEIPTS: {
        LIST: 'Receipts/listSBC'.trim(),
        UPDATE: 'Receipts/updateSBC'.trim(),
        COUNTITEMPALLET: 'Receipts/countItemPallet'.trim(),
        ADDORMOVE: 'Receipts/addOrMove'.trim(),
        PALLETZATION: 'Receipts/palletzation'.trim(),
        CHANGEKEEPERANDPALLETIONSBC: 'Receipts/changeKeeperAndPalletionSBC'.trim() // queue
    },
    RECEIPTDETAILS: {
        SUGGESTLOCATIONSBC: 'Receiptdetails/suggestLocationSBC'.trim(), // queue
        LISTSBC: 'Receiptdetails/listSBC'.trim(),
        CONFIRMRECEIVE: 'Receiptdetails/confirmReceive'.trim(), // queue
        UPDATEPRODUCTIONDATESBC: 'Receiptdetails/updateProductionDateSBC'.trim(),
        UPDATELOTTABLE01SBC: 'Receiptdetails/updateLottable01SBC'.trim(),
        LISTBYSUGGESTLOCATIONSBC: 'Receiptdetails/listBySuggestlocationSBC'.trim(),
        CONFIRMSUGGESTLOCATION: 'Receiptdetails/confirmSuggestLocation'.trim() // queue
    },
    ORDERS: {
        COUNTITEMPALLET: 'Orders/countItemPallet'.trim(),
        ADDORMOVE: 'Orders/addOrMove'.trim(),
        LISTSBC: 'Orders/listSBC'.trim(),
        UPDATE: 'Orders/updateSBC'.trim(),
    },
    PICKDETAIL: {
        LISTSBC: 'Pickdetails/listSBC'.trim(),
        PICK: 'Pickdetails/pickSBC'.trim(), // queue
        SHIP: 'Pickdetails/shipSBC'.trim() // queue
    },
    ORDERDETAIL: {
        LISTSBC: 'Orderdetails/listSBC'.trim(),
    },
    OUTBOUNDS: {
        ALLOCATE: 'Outbounds/allocateByOrders'.trim() // 
    },
    INVENTORY: {
        LIST: 'Inventories/listSBC'.trim(),
        LISTBYSKU: 'Inventories/listBySkuSBC'.trim(),
    },
    MEDIA: {
        SAVE: 'ImagexTables/save'.trim(),
        FINDPAGINATION: 'ImagexTables/findPagination'.trim(),
        UPLOAD: 'Images/upload'.trim()
    }
}