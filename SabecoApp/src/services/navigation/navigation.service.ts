import {  NavigationAction, NavigationActions, NavigationContainerComponent, StackActions } from 'react-navigation';
import {DrawerActions} from 'react-navigation-drawer'
export class NavigationService {
  private navigatorRef?: NavigationContainerComponent;
  setTopLevelNavigator(navigatorRef: NavigationContainerComponent) {
    this.navigatorRef = navigatorRef;
  }

  dispatch(action: NavigationAction) {
    this.navigatorRef!.dispatch(action);
  }
  navigate(routeName: string, params?: object) {
    this.dispatch(
      NavigationActions.navigate({
        routeName,
        params
      })
    );
  }
  goBack() {
    
   this.dispatch(NavigationActions.back())
  }
  openDrawer() {
    this.dispatch(
      DrawerActions.openDrawer()
    );
  }
  toggleDrawer() {
    this.dispatch(
      DrawerActions.toggleDrawer()
    );
  }


  // stack navigtion

  push(routeName: string, params?: object) {
    this.dispatch(StackActions.push({
      routeName,
      params
    }));
  }
  pop(numberOfscreensBack: number = 1, immediate = false) {
    this.dispatch(StackActions.pop({
        n: numberOfscreensBack,
        immediate
    }));
  }
}
export default new NavigationService();
